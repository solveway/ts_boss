<?php
	Route::get('login','LoginController@index');
	Route::post('cek_login','LoginController@cek_login');
	Route::post('forgot_password','LoginController@forgot_password');
	Route::get('logout','LoginController@logout');
	
	Route::get('profile', 'LoginController@profile');
	Route::post('update_profile', 'LoginController@update_profile');
	Route::get('change-language/{id}','LoginController@change_language');

	//Main route
	Route::get('/', 'IndexController@index');

	Route::group(['prefix' => 'users'], function(){
		Route::resource('user', 'UserController');
		Route::any('user/ext/{action}','UserController@ext');
		Route::get('user/get_store/{id}','UserController@get_store');

		Route::resource('access-right', 'UseraccessController');
		Route::any('access-right/ext/{action}','UseraccessController@ext');

		Route::resource('members', 'MembersController');
		Route::any('members/ext/{action}','MembersController@ext');
	});

	Route::group(['prefix' => 'page'], function(){
		Route::resource('our-company', 'OurCompanyController');
		Route::any('our-company/ext/{action}','OurCompanyController@ext');

		Route::resource('our-service', 'OurServiceController');
		Route::any('our-service/ext/{action}','OurServiceController@ext');

		Route::get('home', 'HomeController@index');
		Route::post('home/update','HomeController@update');
	});

	Route::group(['prefix' => 'accounts'], function(){
		Route::resource('apply', 'ApplyController');
		Route::any('apply/ext/{action}','ApplyController@ext');

		Route::resource('customer', 'CustomerController');
		Route::any('customer/ext/{action}','CustomerController@ext');
		Route::any('customer/get-district/{par}', 'CustomerController@get_district');
		Route::any('customer/get-subdistrict/{id}', 'CustomerController@get_subdistrict');
		Route::any('customer/get-postcode/{id}', 'CustomerController@get_postcode');
	});

	Route::group(['prefix' => 'merchants'], function(){
		Route::resource('manage-merchant', 'MerchantController');
		Route::any('manage-merchant/ext/{action}','MerchantController@ext');

		Route::resource('merchant-category', 'MerchantCategoryController');
		Route::any('merchant-category/ext/{action}','MerchantCategoryController@ext');

		Route::get('manage-merchant/get_merchant_category/{par}', 'MerchantController@get_merchant_category');
		Route::get('manage-merchant/get_merchant_location/{par}', 'MerchantController@get_merchant_location');

		Route::get('store/get_kecamatan/{id}','MerchantController@get_kecamatan');


	});

	Route::group(['prefix' => 'our-clients'], function(){
		Route::resource('card-category', 'CardCategoryController');
		Route::any('card-category/ext/{action}','CardCategoryController@ext');

		Route::resource('manage-our-client', 'ClientController');
		Route::any('manage-our-client/ext/{action}','ClientController@ext');
	});

	Route::group(['prefix' => 'news'], function(){
		Route::resource('manage-news', 'NewsController');
		Route::any('manage-news/ext/{action}','NewsController@ext');
	});

	Route::group(['prefix' => 'notification'], function(){
		Route::resource('preorder', 'PreorderController');
		Route::any('preorder/ext/{action}','PreorderController@ext');

		Route::resource('inbox', 'InboxController');
		Route::any('inbox/ext/{action}','InboxController@ext');
		Route::any('inbox/show2/{id}/{id2}','InboxController@show2');

		Route::resource('broadcast', 'BroadcastController');
		Route::any('broadcast/ext/{action}','BroadcastController@ext');
 
	});

	Route::group(['prefix' => 'administration'], function(){
		Route::resource('user-access', 'UseraccessController');
		Route::any('user-access/ext/{action}','UseraccessController@ext');

		Route::resource('user', 'UserController');
		Route::any('user/ext/{action}','UserController@ext');
	});

	Route::group(['prefix' => 'preferences'], function(){
		Route::get('general-settings', 'ConfigController@index');
		Route::post('general-settings/update', 'ConfigController@update');
	});


	// Route::group(['prefix' => 'v1'], function(){
	// 	Route::any('get-district', 'CustomerController@get_district');
	// 	Route::any('get-subdistrict', 'CustomerController@get_subdistrict');
	// 	Route::any('get-postcode', 'CustomerController@get_postcode');
	// });

	//Main services for ajax outside builder
	Route::any('services','ServicesController@index');