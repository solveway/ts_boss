<?php
//Main route
Route::get('/', 'IndexController@index')->name('cs_home');

Route::group(['prefix' => 'about-us'], function(){
	Route::get('vision-mission', 'PagesController@aboutvm');
	Route::get('company-strength', 'PagesController@aboutcs');
	Route::get('milestones', 'PagesController@aboutml');
});

Route::group(['prefix' => 'corporate-governance'], function(){
	Route::get('the-boards', 'PagesController@boardcm');
});

Route::group(['prefix' => 'mining-assets'], function(){
	Route::get('pt-bangun-olah-sarana', 'PagesController@olah_sarana');
	Route::get('pt-pratama-bersama', 'PagesController@pratama_bersama');
	Route::get('pt-energi-amzal-bersama', 'PagesController@energi_amzal');
	Route::get('pt-pratama-buana-sentosa', 'PagesController@buana_sentosa');
});

Route::group(['prefix' => 'project-data'], function(){
	Route::get('production-hauling', 'PagesController@prj_pro');
	Route::get('port-facility', 'PagesController@prj_port');
	Route::get('guaranteed-specifications', 'PagesController@prj_guaran');
	Route::get('coal-exports', 'PagesController@prj_coal');
	Route::get('documents', 'PagesController@prj_documents');
});

Route::get('investor-relation', 'PagesController@relation');
Route::get('health-safety', 'PagesController@health_safe');
Route::match(['get', 'post'], 'news-release', 'PagesController@news');

Route::get('contact-us', 'PagesController@contact_us');
Route::post('contact-us', 'PagesController@contact_message');

Route::post('search', 'PagesController@search');

Route::group(['prefix' => 'v1'], function(){
	Route::post('live_search', 'PagesController@sc_live');
});
