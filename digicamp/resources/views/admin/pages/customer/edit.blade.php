@extends($view_path.'.layouts.master')
@section('content')
<form role="form" method="post" action="{{url($path)}}/{{$data1->id}}" enctype="multipart/form-data">
{{ method_field('PUT') }}
	<div class="portlet light bordered">
    	<div class="portlet-title">
			<div class="caption font-green">
				<i class="icon-layers font-green title-icon"></i>
				<span class="caption-subject bold uppercase"> {{$title}}</span>
			</div>
			<div class="actions">
				<a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
			</div>
    	</div>
    	<div class="portlet-body form">
      		@include('admin.includes.errors')
  			<div class="row">
  				{!!view($view_path.'.builder.text',['type' => 'text','name' => 'name','label' => 'Name','value' => (old('name') ? old('name') : $data1->name),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => 'name', 'required' => 'y'])!!}

  				{!!view($view_path.'.builder.text',['type' => 'text','name' => 'email','label' => 'Email','value' => (old('email') ? old('email') : $data1->email),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => 'email', 'required' => 'y'])!!}

  				<div class="col-md-12">
					<div class="form-group form-md-line-input" hre="">
						<label for="form_floating_hre">Password <span class="required" aria-required="true">*</span></label>
				 		<div class="input-group">
				      		<input type="password" class="form-control password" id="password" name="password" placeholder="Password" value="">
				      		<span class="input-group-addon">
	                            <a id="show-password" class="text-default"><i class="fa fa-eye"></i></a>
	                        </span>

	                        <span class="input-group-btn">
	                            <a id="generate-password" class="btn btn-primary">Generate</a>
	                        </span>
				    	</div>
					
						<small></small>

					</div>
				</div>

				@php
					$st_rel_count = 1;
					$st_rel = array('Belum Menikah', 'Menikah');

					if(in_array($data1->status_menikah, $st_rel)){
						$st_rel_count = 1;
					} else{
						$st_rel_count = 0;
					}
				@endphp
				<div class="col-md-12">
					<div class="col-md-6">
			            <div class="form-group">
							<label for="tag">Status Menikah <span class="required" aria-required="true">*</span></label>
							<select class="select2" name="status_menikah">
								<option value="Belum Menikah" {{ $data1->status_menikah == "Belum Menikah" ? 'selected' : '' }}>Belum Menikah</option>
								<option value="Menikah" {{ $data1->status_menikah == "Menikah" ? 'selected' : '' }}>Menikah</option>
								<option value="other" {{ $st_rel_count == 0 ? 'selected' : '' }}>Lainnya</option>
							</select>
			            </div>
			        </div>

			        <div class="col-md-6">
			            <div class="form-group form-md-line-input">
			            	<input type="text" class="form-control" name="status_menikah_lainnya" value="{{ $st_rel_count == 0 ?  $data1->status_menikah : '' }}"/>
			            	<label for="form_floating_IcO">Lainnya <span class="required" aria-required="true"></span></label>
							<small>Jika Status tidak ada, pilih Lainnya dan isi kotak diatas.</small>
			            </div>
			        </div>
			    </div>

				<div class="col-md-6">
		            <div class="form-group">
						<label for="tag">Jenis Kelamin <span class="required" aria-required="true">*</span></label>
						<select class="select2" name="jenis_kelamin">
							<option value="Pria" {{ $data1->jenis_kelamin == "Pria" ? 'selected' : '' }}>Pria</option>
							<option value=">Wanita" {{ $data1->jenis_kelamin == "Menikah" ? 'selected' : '' }}>Wanita</option>
							<option value="other" {{ $st_rel_count == 0 ? 'selected' : '' }}>Lainnya</option>
						</select>
		            </div>
		        </div>


			    {!!view($view_path.'.builder.text',['type' => 'text','name' => 'telephone','label' => 'Telephone','value' => (old('telephone') ? old('telephone') : $data1->telephone),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => '', 'placeholder' => '', 'required' => 'y'])!!}

			    {!!view($view_path.'.builder.text',['type' => 'text','name' => 'handphone','label' => 'Handphone','value' => (old('handphone') ? old('handphone') : $data1->handphone),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => '', 'placeholder' => '', 'required' => 'y'])!!}

				@php
					$get_ttl = $data1->ttl;
					$ct_count = 0;
					if($get_ttl != NULL){
						$split = explode(",",$get_ttl);
						$tl = $split[0];
						$tgl = date_create($split[1]);
						$get_tgl = date_format($tgl,"Y-m-d");
					}else{
						$tl = "";
						$get_tgl = "";
					}
				@endphp

				<div class="col-md-6">
		          <div class="form-group form-md-line-input">
		            <input type="date" class="form-control" name="date_birth" value="{{ $get_tgl }}" />
		            <label for="form_floating_Hqd">Date Birth<span class="" aria-required="true">*</span></label>
		            <small></small>
		          </div>
		        </div>

				<div class="col-md-12">
	                <div class="col-md-6">
	                	<div class="form-group">
							<label>Place Birth<span class="required" aria-required="true">*</span></label>
							<select class="form-control select2 select2-hidden-accessible" name="ep_tp_born" required="" onchange="" tabindex="-1" aria-hidden="true">
								<option value="">--Kota Lahir--</option>
								<option value=""></option>
								@foreach($ttl as $q => $ctl)
									<option value="{{ ucfirst(strtolower($ctl)) }}" {{ $ctl == $tl ? 'selected' : '' }}>{{ ucfirst(strtolower($ctl)) }}</option>

									@php
										if($ctl == $tl) {
											$ct_count = $ct_count+1;
										}
									@endphp
								@endforeach
								<option value="other" {{ $ct_count == 0 ? 'selected' : '' }}>Lainnya</option>
							</select>
							<div class="form-control-focus"> </div>
							<small></small>
						</div>
					</div>

					<div class="col-md-6">
			            <div class="form-group form-md-line-input">
			            	<input type="text" class="form-control" name="place_birth_lainnya" value="{{ $ct_count == 0 ?  $tl : '' }}"/>
			            	<label for="form_floating_IcO">Lainnya <span class="required" aria-required="true"></span></label>
							<small>Jika kota tidak ada, pilih lainnya dan isi kotak diatas.</small>
			            </div>
			        </div>
				</div>

			</div>

			<div class="row">
		            <div class="form-group col-md-12">
		              <label for="tag" class="sub-title">Address</label>
		            </div>
					{!!view($view_path.'.builder.textarea',['type' => 'text','name' => 'address','label' => 'Address','value' => (old('address') ? old('address') : $data1->address),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => 'address'])!!}

					{!!view($view_path.'.builder.text',['type' => 'text','name' => 'rt','label' => 'RT','value' => (old('rt') ? old('rt') : $data1->rt),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => ''])!!}

					{!!view($view_path.'.builder.text',['type' => 'text','name' => 'rw','label' => 'RW','value' => (old('rw') ? old('rw') : $data1->rw),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => ''])!!}

					<div class="col-md-6">
						<div class="form-group form-md-line-input">
							<label>Kota<span class="required" aria-required="true">*</span></label>
								<select class="form-control edp_city get-district select2" name="ep_kota" data-target="district">
									<option value=""></option>
							    	@foreach($city as $q => $ctc)
							    		<option value="{{ $q }}" {{ $q == $profile->kota ? 'selected' : '' }}>{{ ucfirst(strtolower($ctc)) }}</option>
							    	@endforeach
								</select>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group form-md-line-input">
							<label>Kecamatan<span class="required" aria-required="true">*</span></label>
								<select class="form-control district" name="ep_kecamatan" data-target="subdistrict">
									<option value="">-- Pilih Kecamatan --</option>
									@if($kecamatan != "")
									<option value="{{ $kecamatan->id }}" selected>{{ $kecamatan->name }}</option>
									@endif
								</select>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group form-md-line-input">
							<label>Kelurahan<span class="required" aria-required="true">*</span></label>
								<select class="form-control subdistrict" name="ep_kelurahan" data-target="postcode">
									<option value="">-- Pilih Kelurahan --</option>
									@if($kelurahan != "")
									<option value="{{ $kelurahan->id }}" selected>{{ $kelurahan->name }}</option>
									@endif
								</select>
						</div>
					</div>

					@php
						$kd_count = 0;
					@endphp

					<!-- <div class="col-md-12"> -->
						<div class="col-md-6">
							<div class="form-group">
								<label>Kodepos<span class="required" aria-required="true">*</span></label>
								<select class="form-control postcode select2" name="ep_kodepos">
									<option value="">-- Pilih Kodepos --</option>
									@if($kodepos != "")
									<option value="{{ $kodepos->id }}" selected>{{ $kodepos->postcode }}</option>
									@endif
								</select>
							</div>
						</div>

						<!-- <div class="col-md-6">
				            <div class="form-group form-md-line-input">
				            	<input type="text" class="form-control" name="kodepos_lainnya" value="{{ $kodepos != '' ?  $kodepos->postcode : '' }}"/>
				            	<label for="form_floating_IcO">Lainnya <span class="required" aria-required="true"></span></label>
								<small>Jika kota tidak ada, pilih lainnya dan isi kotak diatas.</small>
				            </div>
				        </div> -->
				    <!-- </div> -->
			</div>
			<div class="row">
	            <div class="form-group col-md-12">
	              <label for="tag" class="sub-title">Other</label>
	            </div>
				@php
					$pr_rel_count = 1;
					$pr_rel = array('Pegawai Negeri', 'Pegawai Swasta', 'Ojek', 'Wiraswasta/Pedagang', 'Mahasiswa/Pelajar', 'Guru/Dosen', 'TNI/POLRI', 'Ibu Rumah Tangga', 'Petani/Nelayan','Proffessional(Dokter/Pengacara dll');

					if(in_array($profile->pekerjaan, $pr_rel)){
						$pr_rel_count = 1;
					} else{
						$pr_rel_count = 0;
					}
				@endphp

				<div class="col-md-12">
					<div class="col-md-6">
			            <div class="form-group">
							<label>Pekerjaan <span class="required" aria-required="true">*</span></label>
							<select class="select2" name="ep_pekerjaan">
								<option value="">-- Pilih Pekerjaan --</option>
								<option value="Pegawai Negeri" {{ $profile->pekerjaan == "Pegawai Negeri" ? 'selected' : '' }}>Peg. Negeri</option>
								<option value="Pegawai Swasta" {{ $profile->pekerjaan == "Pegawai Swasta" ? 'selected' : '' }}>Peg. Swasta</option>
								<option value="Ojek" {{ $profile->pekerjaan == "Ojek" ? 'selected' : '' }}>Ojek</option>
								<option value="Wiraswasta/Pedagang" {{ $profile->pekerjaan == "Wiraswasta/Pedagang" ? 'selected' : '' }}>Wiraswasta/Pedagang</option>
								<option value="Mahasiswa/Pelajar" {{ $profile->pekerjaan == "Mahasiswa/Pelajar" ? 'selected' : '' }}>Mahasiswa/Pelajar</option>
								<option value="Guru/Dosen" {{ $profile->pekerjaan == "Guru/Dosen" ? 'selected' : '' }}>Guru/Dosen</option>
								<option value="TNI/POLRI" {{ $profile->pekerjaan == "TNI/POLRI" ? 'selected' : '' }}>TNI/POLRI</option>
								<option value="Ibu Rumah Tangga" {{ $profile->pekerjaan == "Ibu Rumah Tangga" ? 'selected' : '' }}>Ibu Rumah Tangga</option>
								<option value="Petani/Nelayan" {{ $profile->pekerjaan == "Petani/Nelayan" ? 'selected' : '' }}>Petani/Nelayan</option>
								<option value="Professional(Dokter/Pengacara dll)" {{ $profile->pekerjaan == "Professional(Dokter/Pengacara dll)" ? 'selected' : '' }}>Professional(Dokter/Pengacara dll)</option>
								<option value="other" {{ $pr_rel_count == 0 ? 'selected' : ''}}>Lainnya</option>
							</select>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group form-md-line-input">
							@if($pr_rel_count == 0)
								<input type="text" class="form-control" name="ep_pekerjaan_other" placeholder="Pekerjaan Lainnya" value="{{ $profile->pekerjaan }}" />
							@else
								<input type="text" class="form-control" name="ep_pekerjaan_other" placeholder="Pekerjaan Lainnya" />
							@endif
							<label for="form_floating_IcO">Lainnya <span class="required" aria-required="true"></span></label>
							<small>Jika Pekerjaan tidak ada, pilih Lainnya dan isi kotak diatas.</small>
						</div>
					</div>
				</div>

				@php
					$ag_rel_count = 1;
					$ag_rel = array('Islam', 'Kristen', 'Katolik', 'Hindu', 'Buddha');

					if(in_array($profile->agama, $ag_rel)){
						$ag_rel_count = 1;
					} else{
						$ag_rel_count = 0;
					}
				@endphp

				<div class="col-md-12">
					<div class="col-md-6">
			            <div class="form-group">
							<label>Agama <span class="tw_dot">*</span></label>
							<select class="select2 form-control" name="ep_agama">
								<option value="">-- Pilih Agama --</option>
								<option value="Islam" {{ $profile->agama == "Islam" ? 'selected' : '' }}>Islam</option>
								<option value="Kristen" {{ $profile->agama == "Kristen" ? 'selected' : '' }}>Kristen</option>
								<option value="Katolik" {{ $profile->agama == "Katolik" ? 'selected' : '' }}>Katolik</option>
								<option value="Hindu" {{ $profile->agama == "Hindu" ? 'selected' : '' }}>Hindu</option>
								<option value="Buddha" {{ $profile->agama == "Buddha" ? 'selected' : '' }}>Buddha</option>
								<option value="other" {{ $ag_rel_count == 0 ? 'selected' : '' }}>Lainnya</option>
							</select>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group form-md-line-input">
							@if($ag_rel_count == 0)
								<input type="text" class="form-control" name="ep_agama_other" placeholder="Agama Lainnya" value="{{ $profile->agama }}" />
							@else
								<input type="text" class="form-control" name="ep_agama_other" placeholder="Agama Lainnya" />
							@endif
								<label for="form_floating_IcO">Lainnya <span class="required" aria-required="true"></span></label>
								<small>Jika Agama tidak ada, pilih Lainnya dan isi kotak diatas.</small>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group form-md-line-input">
						<label>Pendidikan <span class="tw_dot">*</span></label>
							<select class="select2" name="ep_pendidikan">
								<option value="">-- Pilih Pendidikan --</option>
								<option value="Tidak Tamat SD" {{ $profile->pendidikan == "Tidak Tamat SD" ? 'selected' : '' }}>Tidak Tamat SD</option>
								<option value="SD" {{ $profile->pendidikan == "SD" ? 'selected' : '' }}>SD</option>
								<option value="SLTP/SMP" {{ $profile->pendidikan == "SLTP/SMP" ? 'selected' : '' }}>SLTP/SMP</option>
								<option value="SLTP/SMA" {{ $profile->pendidikan == "SLTP/SMA" ? 'selected' : '' }}>SLTP/SMA</option>
								<option value="Akademi/D1/D2/D3" {{ $profile->pendidikan == "Akademi/D1/D2/D3" ? 'selected' : '' }}>Akademi/D1/D2/D3</option>
								<option value="Sarjana/S1" {{ $profile->pendidikan == "Sarjana/S1" ? 'selected' : '' }}>Sarjana/S1</option>
								<option value="Pasca Sarjana/S2/S3" {{ $profile->pendidikan == "Pasca Sarjana/S2/S3" ? 'selected' : '' }}>Pasca Sarjana/S2/S3</option>
							</select>
					</div>
				</div>

				@php
					$tj_rel_count = 1;
					$tj_rel = array('Berdagang', 'Jarak Dekat', 'Ke Sekolah/Kampus', 'Rekreasi/Olahraga', 'Kebutuhan Keluarga', 'Bekerja');

					if(in_array($profile->tujuan, $tj_rel)){
						$tj_rel_count = 1;
					} else{
						$tj_rel_count = 0;
					}
				@endphp

				<div class="col-md-12">
					<div class="col-md-6">
						<div class="form-group">
							<label>Tujuan Pakai <span class="tw_dot">*</span></label>
								<select class="select2" name="ep_tujuan">
									<option value="">-- Pilih Tujuan Pakai --</option>
									<option value="Berdagang" {{ $profile->tujuan == "Berdagang" ? 'selected' : '' }}>Berdagang</option>
									<option value="Jarak Dekat" {{ $profile->tujuan == "Jarak Dekat" ? 'selected' : '' }}>Jarak Dekat</option>
									<option value="Ke Sekolah/Kampus" {{ $profile->tujuan == "Ke Sekolah/Kampus" ? 'selected' : '' }}>Ke Sekolah/Kampus</option>
									<option value="Rekreasi/Olahraga" {{ $profile->tujuan == "Rekreasi/Olahraga" ? 'selected' : '' }}>Rekreasi/Olahraga</option>
									<option value="Kebutuhan Keluarga" {{ $profile->tujuan == "Kebutuhan Keluarga" ? 'selected' : '' }}>Kebutuhan Keluarga</option>
									<option value="Bekerja" {{ $profile->tujuan == "Bekerja" ? 'selected' : '' }}>Bekerja</option>
									<option value="other" {{ $tj_rel_count == 0 ? 'Selected' : '' }}>Lainnya</option>
								</select>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group form-md-line-input">
							@if($tj_rel_count == 0)
								<input type="text" class="form-control" name="ep_tujuan_other" placeholder="Tujuan Pakai Lainnya" value="{{ $profile->tujuan }}" />
							@else
								<input type="text" class="form-control" name="ep_tujuan_other" placeholder="Tujuan Pakai Lainnya" />
							@endif
							<label for="form_floating_IcO">Lainnya <span class="required" aria-required="true"></span></label>
							<small>Jika Pilihan tidak ada, pilih Lainnya dan isi kotak diatas.</small>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group form-md-line-input">
						<label>Pengeluaran Perbulan <span class="tw_dot">*</span></label>
						<select class="form-control select2" name="ep_pengeluaran">
							<option value="">-- Pengeluaran Perbulan --</option>
							@foreach($pengeluaran as $q => $pg)
								<option value="{{ $q }}" {{ $profile->pengeluaran == $q ? 'selected' : '' }}> {{ $pg }}</option>
							@endforeach
						</select>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group form-md-line-input">
						<label>Hobby <span class="tw_dot">*</span></label>
							<input type="text" class="form-control" name="ep_hobby" value="{{ $profile->hobby ? $profile->hobby : '' }}" placeholder="Hobby" />
					</div>
				</div>

				<div class="form-group form-md-line-input col-md-12">
		            <label>Photo</label><br>
		            <label class="btn green input-file-label-photo">
		              <input type="file" class="form-control col-md-12 single-image" name="photo"> Pilih File
		            </label>
		                <button type="button" class="btn red-mint remove-single-image" data-id="single-image" data-name="photo">Hapus</button>
		              <input type="hidden" name="remove-single-image-photo" value="n">
		              <br>
		            <small>Note: File Must jpeg,png,jpg,gif | Max file size: 2Mb | Best Resolution: 500 x 500 px</small>	

		            <div class="form-group single-image-photo col-md-12">
		              <img src="{{isset($data1->photo) ? asset($image_path.'/'.$data1->photo) : asset($image_path.'/none.png')}}" class="img-responsive thumbnail single-image-thumbnail">
		            </div>
		        </div>

  				<input type="hidden" id="root-url" value="{{$path}}" />
			</div>	

			<div class="row">
  				{!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
  			</div>
	</div>
</form>
@push('custom_scripts')
	<script>
		$(document).ready(function(){
			var password = $(".password");
			// var username = $(".username");
			// var name = $(".name");
 			// $(document).on('change','.select-merchant',function(res){
				// getStore('');
 			// });

			// function getStore(id){	
			// 	var merchant_id = $('.select-merchant option:selected').val();
			// 	$(".select-store").children('option:not(:first)').remove();	
			// 	$("#select2-store_id-eb-container").text("");		
			//  	var url    = $.root() + 'users/user/get_store/' + merchant_id;
			//  	console.log(url);
			//     $.ajax({
			// 		url: url,
			// 		type: "GET",
			// 		dataType: 'json',
			// 		// data: par
	  //         	}).done(function(msg) {
	  //         		var option='';
			// 		$.each(msg, function( index, value ) {
			// 			if(id){
			// 				var selected = (id == index) ? 'selected' : '';
			// 			}
			// 			option += "<option value="+index+" "+selected+">"+value+"</option>";
			// 		});
			// 		$(".select-store").append(option);
			// 		var selected_id = $(".select-store option:selected").val();
			// 		// if(selected_id){
			// 		// 	var selected_text = $(".select-store option:selected").text();
			// 		// 	// $(".select2-selection__rendered").text(selected_text);
			// 		// }
	  //         	});
			
			//    	// return data;
			// }

			
			$('#generate-password').on('click', function(e){
	        	var randomstring = Math.random().toString(36).slice(-6);
		        password.val(randomstring);
		    });

		    $('#show-password').on('click', function(e){
		        if(password.attr("type") == "password"){
		            password.attr("type", "text");
		            $("#show-password").addClass("text-primary");
		            $("#show-password").removeClass("text-default");
		        }
		        else{
		            password.attr("type", "password");
		            $("#show-password").addClass("text-default");
		            $("#show-password").removeClass("text-primary");
		        }
		    });

		    
		    function getKecamatan(text){
		        // var merchant_id = $('.select-merchant option:selected').val();
		        // $(".select-store").children('option:not(:first)').remove(); 
		        // $("#select2-store_id-eb-container").text("");   
		        var url    = $.root() + 'merchants/store/get_kecamatan/' + text;
		        console.log(url);
		          $.ajax({
		          url: url,
		          type: "GET",
		          dataType: 'json',
		          // data: par
		          }).done(function(msg) {
		          		console.log(msg);
		              var data = [];
		              $.each(msg, function( index, value ) {
		                var arr={
		                  'value': index,
		                  'label': value
		                }

		                data.push(arr);
		              });
		              console.log(data);

		              $( ".region_kecamatan_id" ).autocomplete({
		                  minLength: 0,
		                  source: data,
		                  focus: function( event, ui ) {
		                    console.log(ui);
		                    $( ".region_kecamatan_id" ).val( ui.item.label );
		                    return false;
		                  },
		                  select: function( event, ui ) {
		                    console.log(ui.item);
		                    $( ".region_kecamatan_id" ).val( ui.item.label );
		                    $( "#kecamatan_id" ).val( ui.item.value );

		                    return false;
		                  }
		              })
		          });
		    }

			$(document).on('change','.get-district',function(e,ret){
		        var target  = $(this).data('target');
		        var id      = $(this).val();
		        var temp    = '<option value="">-- Pilih Kecamatan --</option>'
		        var data    = {id:id};

		        $('#loader-wrapper').fadeIn();

		        $.postdata($.root()+'accounts/customer/get-district/'+id).success(function(res){

		          var district  = res.district;
		          district.forEach(function(v,i){
		            temp      += '<option value="'+v.id+'">'+v.name+'</option>';
		          });

		          $('.'+target).html(temp);
		          $('#loader-wrapper').fadeOut();

		        }).error(function(res){
		          $.growl_alert("Sorry, there a problem with your request");
		        });

		    });

		    $(document).on('change','.district',function(e,ret){
		        var target  = $(this).data('target');
		        var id      = $(this).val();
		        var temp    = '<option value="">-- Pilih Kelurahan --</option>'
		        var data    = {id:id};

		        $('#loader-wrapper').fadeIn();

		        $.postdata($.root()+'accounts/customer/get-subdistrict/'+id).success(function(res){
		          var subdistrict  = res.subdistrict;
		          subdistrict.forEach(function(v,i){
		            temp      += '<option value="'+v.id+'">'+v.name+'</option>';
		          });

		          $('.'+target).html(temp);
		          $('#loader-wrapper').fadeOut();
		        }).error(function(res){
		          $.growl_alert("Sorry, there a problem with your request");
		        });

		    });

		    $(document).on('change','.subdistrict',function(e,ret){
		        var target  = $(this).data('target');
		        var id      = $(this).val();
		        var temp    = '<option value="">-- Pilih Kodepos --</option>'
		        var data    = {id:id};

		        $('#loader-wrapper').fadeIn();

		        $.postdata($.root()+'accounts/customer/get-postcode/'+id).success(function(res){
		          var postcode  = res.postcode;
		          postcode.forEach(function(v,i){
		            temp      += '<option value="'+v.id+'">'+v.postcode+'</option>';
		          });

		          $('.'+target).html(temp);
		          $('#loader-wrapper').fadeOut();

		        }).error(function(res){
		          $.growl_alert("Sorry, there a problem with your request");
		        });

		    });
		  
		});
	</script>
@endpush
@endsection