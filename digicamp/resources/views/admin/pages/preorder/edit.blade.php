@extends($view_path.'.layouts.master')
@section('content')
<form role="form" method="post" action="{{url($path)}}/{{$preorderhd->id}}">
  {{ method_field('PUT') }}
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green portlet-container">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
        <div class="head-button">
          <a href="{{url($path)}}"><button type="button" class="btn red-mint"><i class="fa fa-arrow-left"></i> {{trans('general.back')}}</button></a>
          {!!view($view_path.'.builder.button',['type' => 'submit','label' => 'Simpan'])!!}
        </div>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      <div class="row"> 
        {!!view($view_path.'.builder.text',['name' => 'store_id','label' => 'Store Name','form_class' => 'col-md-6','value' => (old('store_id') ? old('store_id') : $preorderhd->stores->store_name),'attribute' => 'required disabled'])!!}
        {!!view($view_path.'.builder.text',['name' => 'user_id','label' => 'Requester','form_class' => 'col-md-6','value' => (old('user_id') ? old('user_id') : $preorderhd->user->name),'attribute' => 'required disabled'])!!}
      </div>
      {!!view($view_path.'.builder.select',['name' => 'status','label' => 'Response','class' => 'select2','value' => 'y','data' => ['y' => 'Approve' , 'n' => 'Reject']])!!}
      <h4>List Request Item</h4>
      <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <th>No</th>
            <th>Product</th>
            <th>Total</th>
          </thead>
          <tbody>
            @foreach($preorderhd->preorder_dt as $pr)
              <tr>
                <td>{{$no++}}</td>
                <td>{{$pr->product}}</td>
                <td>{{$pr->total}}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div> 
  </div>
</form>
@endsection
