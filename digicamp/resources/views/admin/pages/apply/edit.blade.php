@extends($view_path.'.layouts.master')
@section('content')
<form role="form" method="post" action="{{url($path)}}/{{$data1->id}}" enctype="multipart/form-data">
  {{ method_field('PUT') }}
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <div class="actions">
          <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
        </div>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      <div class="row">       
        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'name','label' => 'Name','value' => (old('name') ? old('name') : $data1->name),'attribute' => 'required autofocus','form_class' => 'col-md-6'])!!}

        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'phone','label' => 'Phone','value' => (old('phone') ? old('phone') : $data1->phone),'attribute' => 'required autofocus','form_class' => 'col-md-12'])!!}

        {!!view($view_path.'.builder.textarea',['type' => 'text','name' => 'address','label' => 'Address','value' => (old('address') ? old('address') : $data1->address),'attribute' => 'required autofocus','form_class' => 'col-md-12'])!!} 

        <div class="form-group col-md-6">
          <label for="tag">Client</label>
          <input type="hidden" name="">
          <select name="client_card_card_category" class="form-control client-card-card-category" id="client-card-card-category">
            @foreach($data2 as $d2)
              <option value="{{$d2->id}}" {{$d2->id == $data1->client_id ? 'selected' : ''}}>{{$d2->client_name}}</option>
            @endforeach
          </select>
        </div>

        <div class="col-md-12 actions">
          {!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
        </div>
      </div>
    </div>
</form>

@push('scripts')

@endpush
@push('custom_scripts')
  <script>
  </script>
@endpush
@endsection
