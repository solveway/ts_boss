@extends($view_path.'.layouts.master')
@section('content')
<form role="form" method="post" action="{{url($path)}}/update" enctype="multipart/form-data">
  <div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green">
          <i class="icon-layers font-green title-icon"></i>
          <span class="caption-subject bold uppercase"> {{$title}}</span>
        </div>

        <div class="actions">
        <div class="actions">
          <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
        </div>
      </div>
    </div>
    <datalist></datalist>
      @include('admin.includes.errors')
      <div class="tabbable-line">
        <div class="row">
          <div class="form-group col-md-12 form-group-promo">
            <label for="tag" class="sub-title">Slideshow</label>
          </div>
          @foreach($slideshow as $s)
          <div class="form-group form-md-line-input col-md-12">
              <label>{{$s->name}}</label><br>
              <label class="btn green input-file-label-logo">
                <input type="file" class="form-control col-md-12 single-image" name="slideshow_{{$s->id}}"> Pilih File
              </label>
                  <button type="button" class="btn red-mint remove-single-image" data-id="single-image" data-name="slideshow_{{$s->id}}">Hapus</button>
                <input type="hidden" name="remove-single-image-slideshow_{{$s->id}}" value="n">
                <br>
              <small>Note: File Must jpeg,png,jpg,gif | Max file size: 2Mb | Best Resolution: 1601 x 521 px</small>

              <div class="form-group single-image-slideshow_{{$s->id}} col-md-12">
                <img src="{{$s->image != null ? asset($image_path3.'/'.$s->image) : asset($image_path2).'/none.png'}}" class="img-responsive thumbnail single-image-thumbnail">
              </div>

              <div class="form-group col-md-12">
                <input type="text" class="editor" name="slideshow_description_{{$s->id}}" value="{{$s->description}}" />
              </div>
          </div>
          @endforeach
          </hr>

          <div class="form-group form-md-line-input col-md-12">
            <label>Our Activity</label><br>
            <label class="btn green input-file-label-logo">
              <input type="file" class="form-control col-md-12 single-image" name="slideshow_{{$s->id}}"> Pilih File
            </label>
          </div>
      </div>
    </div>
  </div>
</form>
@push('custom_scripts')
  @if ($role->view == 'n')
    <script>
      $(document).ready(function(){
        // $('input,select,textarea').prop('disabled',true);
      });
    </script>
  @endif
@endpush
@endsection
