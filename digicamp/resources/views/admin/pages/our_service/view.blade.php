@extends($view_path.'.layouts.master')
@section('content')
<form role="form" method="post" action="{{url($path)}}/{{$data1->id}}" enctype="multipart/form-data">
  {{ method_field('PUT') }}
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <div class="actions">
          <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
        </div>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      <div class="row">

       
        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'our_service_name','label' => 'Name','value' => (old('our_service_name') ? old('our_service_name') : $data1->our_service_name),'attribute' => 'required autofocus','form_class' => 'col-md-12'])!!}

        {!!view($view_path.'.builder.textarea',['type' => 'text','name' => 'description','label' => 'Description','value' => (old('description') ? old('description') : $data1->description),'attribute' => 'required autofocus','form_class' => 'col-md-12', 'class' => 'editor'])!!}

        <div class="form-group form-md-line-input col-md-12">
            <label>Icon</label><br>
            <label class="btn green input-file-label-icon">
              <input type="file" class="form-control col-md-12 single-image" name="icon"> Pilih File
            </label>
                <button type="button" class="btn red-mint remove-single-image" data-id="single-image" data-name="icon">Hapus</button>
              <input type="hidden" name="remove-single-image-icon" value="n">
              <br>
            <small>Note: File Must jpeg,png,jpg,gif | Max file size: 2Mb | Best Resolution: x px</small>

            <div class="form-group single-image-icon col-md-12">
              <img src="{{$data1->icon != null ? asset($image_path.$data1->icon) : asset($image_path2).'/none.png'}}" class="img-responsive thumbnail single-image-thumbnail">
            </div>
        </div>

        
        <div class="form-group form-md-line-input col-md-12">
            <label>Icon Active</label><br>
            <label class="btn green input-file-label-icon">
              <input type="file" class="form-control col-md-12 single-image" name="icon_active"> Pilih File
            </label>
                <button type="button" class="btn red-mint remove-single-image" data-id="single-image" data-name="icon_active">Hapus</button>
              <input type="hidden" name="remove-single-image-icon_active" value="n">
              <br>
            <small>Note: File Must jpeg,png,jpg,gif | Max file size: 2Mb | Best Resolution: x px</small>

            <div class="form-group single-image-icon_active col-md-12">
              <img src="{{$data1->image_active != null ? asset($image_path.$data1->image_active) : asset($image_path2).'/none.png'}}" class="img-responsive thumbnail single-image-thumbnail">
            </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="form-group form-md-line-input" style="border-bottom: 1px solid #eef1f5;">
          <h4 >Meta Data</h4>
          </div>
           <br>
            <small>Note: Leave blank to use default value</small>
        </div>
        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'meta_title','label' => 'Meta Title','value' => (old('meta_title') ? old('meta_title') : $data1->meta_title),'attribute' => 'autofocus','form_class' => 'col-md-6'])!!}

        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'meta_keyword','label' => 'Meta Keyword','value' => (old('meta_keyword') ? old('meta_keyword') : $data1->meta_keyword),'attribute' => 'autofocus','form_class' => 'col-md-6'])!!}

        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'meta_description','label' => 'Meta Description','value' => (old('meta_description') ? old('meta_description') : $data1->meta_description),'attribute' => 'autofocus','form_class' => 'col-md-12'])!!}

         <div class="col-md-12 actions">
          <!-- {!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!} -->
        </div>
    </div>
    </div>
  </div>
</form>
@push('scripts')

@endpush
@push('custom_scripts')
  <script>
    $(document).ready(function(){
      $('input,select,checkbox,button.remove-single-image').attr('disabled',true);
    });
  </script>
@endpush
@endsection
