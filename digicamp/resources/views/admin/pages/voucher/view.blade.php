@extends($view_path.'.layouts.master')
@push('css')
  <link href="{{asset('components/back/css/pages/profile-2.min.css')}}" rel="stylesheet" type="text/css" />
@endpush
@section('content')
<div class="profile">
    <div class="tabbable-line tabbable-full-width">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#tab_1_1" data-toggle="tab"> Overview </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1_1">
                <div class="row">
                    <div class="col-md-3">
                        <ul class="list-unstyled profile-nav">
                            <li>
                                 @if($voucher->image != null)
                                    <img src="{{asset('components/admin/image/voucher/'.$voucher->image)}}" class="img-responsive pic-bordered" alt="" />
                                @else
                                    <img src="{{asset('components/admin/image/default.jpg')}}" class="img-responsive pic-bordered" alt="" />
                                @endif
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-8 profile-info">
                                <h1 class="font-green sbold uppercase">{{$voucher->voucher_name}}</h1>
                                <p>
                                    {{$voucher->voucher_detail}}
                                </p>
                                <ul class="list-inline">
                                    <li>
                                        <i class="fa fa-calendar"></i> {{date( 'd M Y', strtotime($voucher->start_date) )}} - {{date( 'd M Y', strtotime($voucher->end_date) )}} </li>
                                    <li>
                                        <i class="fa fa-flag"></i> {{$voucher->redeem_flag == 'y' ? "For redeem (".$voucher->redeem_point." pp)"  : "Not for redeem"}} </li>
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <div class="portlet sale-summary">
                                    <div class="portlet-title">
                                        <div class="caption font-red sbold"> Summary </div>
                                    </div>
                                    <div class="portlet-body">
                                        <ul class="list-unstyled">
                                            <li>
                                                <span class="sale-info">
                                                    Discount
                                                </span>
                                                <span class="sale-num">
                                                    @if($voucher->discount_type == "p")
                                                        {{number_format($voucher->discount_value)}}%
                                                    @else
                                                        IDR {{number_format($voucher->discount_value)}}
                                                    @endif
                                                </span>
                                            </li>
                                            <li>
                                                <span class="sale-info">
                                                    Status
                                                </span>
                                                <span class="sale-num">
                                                    @if($voucher->status == "y")
                                                        <label class='label label-success'>Active</label>
                                                    @else
                                                        <label class='label label-danger'>Not Active</label>
                                                    @endif
                                                </span>
                                            </li>
                                            <li>
                                                <span class="sale-info"> Publish
                                                    <i class="fa fa-img-up"></i>
                                                </span>
                                                <span class="sale-num">
                                                    @if($voucher->publish == "y")
                                                        <label class='label label-success'>Active</label>
                                                    @else
                                                        <label class='label label-danger'>Not Active</label>
                                                    @endif
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end row-->
                        <div class="tabbable-line tabbable-custom-profile">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_1_11" data-toggle="tab"> Voucher code </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1_11">
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-advance table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        Voucher Code </th>
                                                    <th>
                                                        Member </th>
                                                    <th>
                                                        Status </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if($voucher->voucher_code != null)
                                                    @foreach(json_decode($voucher->voucher_code) as $vm)
                                                        <tr>
                                                            <td>
                                                                {{$vm}} </td>
                                                            <td>
                                                                <label class="label label-info">Not yet redeemed</label></td>
                                                            <td>
                                                                <label class="label label-warning">Not yet used</label> </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                                {{$voucher->voucher_member}}
                                                @if($voucher->voucher_member)
                                                    @foreach($voucher->voucher_member as $key => $vm)
                                                        <tr>
                                                            <td>
                                                                {{$vm->voucher_code}} </td>
                                                            <td>
                                                                {{$vm->member->member_user->username}} </td>
                                                            <td>
                                                                @if($vm->voucher_use()->where('voucher_id', $vm->voucher_id)->where('member_id', $vm->member_id)->count() > 0)
                                                                    <label class='label label-danger'>Used</label>
                                                                @else
                                                                    <label class='label label-success'>Not yet used</label>
                                                                @endif
                                                             </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end tab-pane-->
        </div>
    </div>
</div>
@endsection