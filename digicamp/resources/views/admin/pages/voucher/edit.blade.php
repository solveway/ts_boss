@extends($view_path.'.layouts.master')
@section('content')
<form role="form" method="post" action="{{url($path)}}/{{$voucher->id}}" enctype="multipart/form-data">
  {{ method_field('PUT') }}
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green portlet-container">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
        <div class="head-button">
          <a href="{{url($path)}}"><button type="button" class="btn red-mint"><i class="fa fa-arrow-left"></i> {{trans('general.back')}}</button></a>
          {!!view($view_path.'.builder.button',['type' => 'submit','label' => 'Lanjutkan'])!!}
        </div>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      <div class="tabbable-line">
        <ul class="nav nav-tabs ">
          <li class="active">
            <a href="#summary" data-toggle="tab" aria-expanded="true">Information</a>
          </li>
          <li>
            <a href="#condition" data-toggle="tab" aria-expanded="false">Condition</a>
          </li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="summary">
            {!!view($view_path.'.builder.text',['name' => 'voucher_name','label' => 'Voucher Name','value' => (old('voucher_name') ? old('voucher_name') : $voucher->voucher_name),'attribute' => 'required'])!!}
            {!!view($view_path.'.builder.textarea',['name' => 'voucher_detail','label' => 'Voucher Detail','value' => (old('voucher_detail') ? old('voucher_detail') : $voucher->voucher_detail),'attribute' => 'required'])!!}
            <div class="row">   
              {!!view($view_path.'.builder.file',['name' => 'image','label' => 'Image','value' => $voucher->image,'file_opt' => ['path' => $image_path],'type' => 'file','upload_type' => 'single-image','class' => 'col-md-6','note' => 'Note: File Must jpeg,png,jpg,gif | Best Resolution: 800 x 240 px','form_class' => 'col-md-6'])!!}
              {!!view($view_path.'.builder.text',['name' => 'start_date','label' => 'Valid From','value' => (old('start_date') ? old('start_date') : date_format(date_create($voucher->start_date),'Y-m-d')),'class' => 'datepicker','form_class' => 'col-md-6','attribute' => 'required readonly'])!!}
              {!!view($view_path.'.builder.text',['name' => 'end_date','label' => 'Valid Until','value' => (old('end_date') ? old('end_date') : date_format(date_create($voucher->end_date),'Y-m-d')),'class' => 'datepicker','form_class' => 'col-md-6','attribute' => 'required readonly'])!!}
            </div>
            <div class="row"> 
              {!!view($view_path.'.builder.radio',['type' => 'radio','data' => ['y' => 'Active','n' => 'Not Active'],'name' => 'status','label' => 'Status','form_class' => 'col-md-6','value' => (old('status') ? old('status') : $voucher->status)])!!}
              {!!view($view_path.'.builder.radio',['type' => 'radio','data' => ['y' => 'Publish','n' => 'Not Publish'],'name' => 'publish','label' => 'Publish','attribute' => $voucher->publish == 'y' ? 'disabled' : '','form_class' => 'col-md-6','value' => (old('publish') ? old('publish') : $voucher->publish)])!!}
            </div>
          </div>
          <div class="tab-pane" id="condition">
            {!!view($view_path.'.builder.radio',['type' => 'radio','data' => ['y' => 'Yes','n' => 'No'],'name' => 'redeem_flag','label' => 'Redeem Before','value' => (old('redeem_flag') ? old('redeem_flag') : $voucher->redeem_flag == 'y' ? 'y' :  'n'), 'onclick' => 'redeem()'])!!}
            {!!view($view_path.'.builder.text',['type' => 'number', 'form_class' => 'redeem-point display-none', 'name' => 'redeem_point','label' => 'Redeem Point','value' => (old('redeem_point') ? old('redeem_point') : $voucher->redeem_point)])!!}
            {!!view($view_path.'.builder.radio',['type' => 'radio','data' => ['y' => 'Automatic','n' => 'Manual'],'name' => 'generate_voucher_code','label' => 'Generate Voucher Code','value' => (old('generate_voucher_code') ? old('generate_voucher_code') : $voucher->generate_voucher_code == 'y' ? 'y' :  'n'), 'onclick' => 'generateVoucher()'])!!}
            {!!view($view_path.'.builder.text',['form_class' => 'voucher-code auto-off', 'name' => 'voucher_code','label' => 'Voucher Code','value' => (old('voucher_code') ? old('voucher_code') : ''), 'placeholder' => 'e.g. XBR38239; XBR38238'])!!}
            {!!view($view_path.'.builder.select',['name' => 'discount_type','label' => 'Discount Type','value' => (old('discount_type') ? old('discount_type') : $voucher->discount_type),'data' => ['p' => 'Percentage','v' => 'Value'],'class' => 'select2 discount-type','attribute' => 'required'])!!}
            {!!view($view_path.'.builder.text',['type' => 'number','name' => 'discount_value','label' => 'Discount value','value' => (old('discount_value') ? old('discount_value') : $voucher->discount_value),'attribute' => 'required','class' => 'discount-value'])!!}
            <div class="form-group">
              <label for="tag">Store <span class="required" aria-required="true">*</span></label>
              <select class="select2" name="store_allowed[]" multiple="multiple" {{$voucher->publish == 'y' ? 'disabled' : ''}}>
                <!-- @if(!is_array(json_decode(auth()->guard($guard)->user()->store_id)) && in_array(auth()->guard($guard)->user()->store_id,[0,1]))
                  <option value="0" {{old('store_allowed') ? (in_array(0,old('store_allowed')) ? 'selected' : '') : $voucher->voucher_store_type == 'all' ? 'selected' : ''}}>-- All Store --</option>
                @endif -->
                @foreach($store as $s)
                  <option value="{{$s->id}}" {{old('store_allowed') ? (in_array($s->store_id,old('store_allowed')) ? 'selected' : '') : $voucher->voucher_store_type == 'custom' ? (in_array($s->id,$voucher_store) ? 'selected' : '  ') : ''}}>{{$s->store_name}}</option>
                @endforeach
              </select>
            </div>
            <hr/>
            <div class="form-group redeem-off">
              <label for="tag">Generate voucher for member?</label>
              <select name="voucher_member_type" class="form-control voucher_member_type" {{$voucher->publish == 'y' ? 'disabled' : ''}}>
                <option value="1" {{old('voucher_member_type') ? (old('voucher_member_type') == 1 ? 'selected' : '') : $voucher->voucher_member_type == 1 ? 'selected' : ''}}>All Member</option>
                <option value="2" {{old('voucher_member_type') ? (old('voucher_member_type') == 2 ? 'selected' : '') : $voucher->voucher_member_type == 2 ? 'selected' : ''}}>Random</option>
                <option value="3" {{old('voucher_member_type') ? (old('voucher_member_type') == 3 ? 'selected' : '') : $voucher->voucher_member_type == 3 ? 'selected' : ''}}>Choose Member</option>
              </select>
            </div>
            {!!view($view_path.'.builder.text',['name' => 'total_voucher','label' => 'Total Voucher','value' => (old('total_voucher') ? old('total_voucher') : $voucher->voucher_total),'form_class' => 'auto-on total-voucher redeem-auto'])!!}
            <!-- {!!view($view_path.'.builder.text',['type' => 'number','name' => 'max_use','label' => 'Maximum use per user','value' => (old('max_use') ? old('max_use') : $voucher->max_use),'attribute' => 'required'])!!} -->
            <div class="choose-member redeem-auto">
              @if($voucher->publish == 'n')
                <div class="row">
                  <div class="form-group col-md-6">
                    <label for="tag">Member</label>
                    <select class="select2 user_code_member" name="member">
                      @foreach($member as $m)
                        @if($m->member_user)
                            <option value="{{$m->id}}">{{$m->member_user->username}}</option>
                        @endif
                      @endforeach
                    </select>
                  </div>
                  <!-- {!!view($view_path.'.builder.text',['type' => 'text','name' => 'voucher_code_member','class' => 'voucher_code_member','label' => 'Voucher Code','value' => '','note' => 'Leave blank if you want to generate random voucher','form_class' => 'col-md-6'])!!} -->
                </div>
                {!!view($view_path.'.builder.button',['type' => 'button','label' => 'Add','class' => 'add-member-voucher'])!!}
                <hr/>
              @endif
              <div class="table-responsive redeem-auto">
                <table class="table table-bordered">
                  <thead>
                    <th>Username</th>
                    <th>Voucher Code</th>
                    @if($voucher->publish == 'n')
                      <th></th>
                    @endif
                  </thead>
                  <tbody class="member-data">
                    @foreach($voucher_member as $vm)  
                      @if($vm->member)                                      
                        <tr>
                          <td>{{$vm->member->member_user->username}}</td>
                          <td>
                            {{$vm->voucher_code}}
                            <input type="hidden" name="choose_member[]" value="{{$vm->member_id}}">
                            <input type="hidden" name="choose_code[]" value="{{$vm->voucher_code}}">
                          </td>
                          @if($voucher->publish == 'n')
                            <td><button type="button" class="btn btn-danger delete-member-voucher"><i class="fa fa-trash"></i></button></td>
                          @endif
                        </tr>
                      @endif
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
@push('custom_scripts')
  <script type="text/javascript">
    function redeem(){
      var redeemFlag = $('input[name=redeem_flag]:checked').val();
      
      if(redeemFlag == 'y'){
        $(".redeem-point").show();
        $(".redeem-off").hide();
        $(".redeem-auto").hide();
        $('.voucher_member_type').val("1");
      }
      else if(redeemFlag == 'n'){
        $(".redeem-point").hide();
        $(".redeem-off").show();
        $('.voucher_member_type').trigger('change');
      }
    }

    function generateVoucher(){
      var generateVoucher = $('input[name=generate_voucher_code]:checked').val();
      var redeemFlag = $('input[name=redeem_flag]:checked').val();

      if(generateVoucher == 'y'){
        $(".auto-off").hide();

        if(redeemFlag == 'y'){
          $(".auto-on").show();
        }
      }
      else if(generateVoucher == 'n'){
        $(".auto-off").show();

        if(redeemFlag == 'y'){
          $(".auto-on").hide();
        }
      }
    }

    $(document).ready(function(){
       //return old value
      @if(old('redeem_flag') && old('redeem_flag') == 'y')
        $('.redeem-point').show();
        $('.redeem-off').hide();
        $('.redeem-auto').hide();
      @endif

      @if(old('generate_voucher_code') && old('generate_voucher_code') == 'y')
        $('.voucher-code').hide();
      @endif

      $('.total-voucher').slideUp();
      $('.choose-member').slideUp();

      var temp2 = "";
      @if(old('choose_member'))
      @foreach(old('choose_member') as $key => $cm)
        temp2 += '<tr><td>{{old("choose_member_name")[$key]}}</td><td>random<input type="hidden" name="choose_member[]" value="{{$cm}}"><input type="hidden" name="choose_code[]" value="random"></td><td><button type="button" class="btn btn-danger delete-member-voucher"><i class="fa fa-trash"></i></button></td></tr>';
      @endforeach
      @endif
      
      $('.member-data').append(temp2);

      $(document).on('change','.voucher_member_type',function(){
        var val   = $(this).val();
        if(val == 1){
          $('input[name=total_voucher]').removeAttr('required');
          $('.total-voucher,.choose-member').slideUp();
        }else if(val == 2){
          $('input[name=total_voucher]').attr('required',true);
          $('.total-voucher').slideDown();
          $('.choose-member').slideUp();
        }else{
          $('.total-voucher').slideUp();
          $('.choose-member').slideDown();
        }
      });
      $('.voucher_member_type').trigger('change');

      setTimeout(function(){ redeem() }, 1000);
      setTimeout(function(){ generateVoucher() }, 1000);

      @if($voucher->publish == 'y')
        $('input[name=total_voucher],.discount-type,.discount-value,input[name=max_use],input[name=redeem_flag],input[name=redeem_point],input[name=generate_voucher_code],input[name=voucher_code]').attr('disabled',true);
      @endif

      $(document).on('click','.add-member-voucher',function(){
        var member      = $('.user_code_member').val();
        var membername  = $('.user_code_member').select2('data')[0].text;
        var code        = $('.voucher_code_member').val();
        if(!code){
          code          = 'random';
        }
        var temp        = '<tr><td>'+membername+'</td><td>'+code+'<input type="hidden" name="choose_member[]" value="'+member+'"><input type="hidden" name="choose_member_name[]" value="'+membername+'"><input type="hidden" name="choose_code[]" value="'+code+'"></td><td><button type="button" class="btn btn-danger delete-member-voucher"><i class="fa fa-trash"></i></button></td></tr>';
        $('.member-data').append(temp);
      })

      $(document).on('click','.delete-member-voucher',function(){
        $(this).closest('tr').remove();
      })
    });
  </script>
@endpush
@endsection
