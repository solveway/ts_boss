@extends($view_path.'.layouts.master')
@section('content')
<form role="form" method="post" action="{{url($path)}}/" enctype="multipart/form-data">
  {{ method_field('PUT') }}
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <div class="actions">
          <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
        </div>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      <div class="row">
        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'member_username','label' => 'Member Username','value' => (old('member_user_id') ? old('member_user_id') : $inbox->username),'attribute' => 'required','form_class' => 'col-md-12'])!!} 

        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'user_username','label' => 'User Username','value' => (old('title') ? old('title') : $inbox->store_name),'attribute' => 'required autofocus','form_class' => 'col-md-12'])!!}

        {!!view($view_path.'.builder.textarea',['type' => 'textarea','name' => 'text','label' => 'text','value' => (old('title') ? old('title') : $inbox->text),'attribute' => 'required autofocus','form_class' => 'col-md-12'])!!}

        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'sender','label' => 'sender','value' => (old('title') ? old('title') : $inbox->sender),'attribute' => 'required autofocus','form_class' => 'col-md-12'])!!}

        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'date','label' => 'Date','value' => (old('title') ? old('title') : $inbox->date),'attribute' => 'required autofocus','form_class' => 'col-md-12'])!!}
      </div>
    </div>
  </div>
</form>
@push('scripts')

@endpush
@push('custom_scripts')
  <script>
    $(document).ready(function(){
      $('input,select,checkbox,button.remove-single-image').attr('disabled',true);
    });
  </script>
@endpush
@endsection
