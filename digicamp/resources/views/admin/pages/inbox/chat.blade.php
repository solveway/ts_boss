@extends($view_path.'.layouts.master')    
@section('content')
<style>
    .chat-bubble {
      border-radius: 5px;
      display: inline-block;
      padding: 10px 18px;
      position: relative;
      margin: 10px;
      max-width: 80%;
    }

    .chat-bubble:before {
      content: "\00a0";
      display: block;
      height: 16px;
      width: 9px;
      position: absolute;
      bottom: -7.5px;
    }

    .chat-bubble.left {
      background-color: #f2f2f2;
      color: #bfc0c2;
      float: left;
      margin-left: 15px;
    }

    .chat-bubble.left:before {
      background-color: #f2f2f2;
      left: 10px;
      -webkit-transform: rotate(70deg) skew(5deg);
    }

    .chat-bubble.right {
      background-color: #ffe1ed;
      color: #bfc0c2;
      float: right;
      margin-right: 15px;
    }

    .chat-bubble.right:before {
      background-color: #ffe1ed;
      right: 10px;
      -webkit-transform: rotate(118deg) skew(-5deg);
    }

    .chat-bubble.right a.autolinker {
      color: #bfc0c2;
      font-weight: bold;
    }

    .inbox{
        background-color: white;
        overflow-y: scroll; 
    }

    .msg-content{
        color: black;
        font-size: 17px;
    }

    .msg-datetime{
        font-size: 15px;
        color:rgba(0,0,0,0.45);
        text-align: right;      
    }

    .totop {
    position: fixed;
    bottom: 10px;
    right: 20px;
}
.totop a {
    display: none;
}
a#loadMore, a:visited {
    color: #33739E;
    text-decoration: none;
    display: block;
    margin: 10px 0;
}
a:hover {
    text-decoration: none;
}
a#loadMore {
    padding: 10px;
    text-align: center;
    background-color: #33739E;
    color: #fff;
    border-width: 0 1px 1px 0;
    border-style: solid;
    border-color: #fff;
    box-shadow: 0 1px 1px #ccc;
    transition: all 600ms ease-in-out;
    -webkit-transition: all 600ms ease-in-out;
    -moz-transition: all 600ms ease-in-out;
    -o-transition: all 600ms ease-in-out;
}
#loadMore:hover {
    background-color: #fff;
    color: #33739E;
}
.chat-bubble{
   display:none;   
}
</style>
  <div class='row'>
    <div class='col-md-12'>
         <div class="head-button">
          <a href="{{url($path)}}"><button type="button" class="btn red-mint"><i class="fa fa-arrow-left"></i> {{trans('general.back')}}</button></a>
        </div>
    </div>
  </div>
        <div class='row inbox'>
            @for($i = 0;$i < count($inbox); $i++)
                @if($inbox[$i]->sender == 'App User')
                    <div class='row col-md-12'>
                        <div class='chat-bubble left'>
                            <div class='msg-content left'>{{$inbox[$i]->text}}</div>
                            <div class='msg-datetime'>{{$inbox[$i]->created_at}}</div>
                        </div>
                   </div>
                @else
                   <div class='row col-md-12'>
                        <div class='chat-bubble right'>
                            <div class='msg-content right'>{{$inbox[$i]->text}}</div>
                            <div class='msg-datetime'>{{$inbox[$i]->created_at}}</div>
                        </div>
                   </div>
                @endif
            @endfor

           
        </div>
        <!-- <div class='row'> -->
         <div class='col-md-12'>
              <a href="#" id="loadMore">Load More</a>
          </div>
            
          <div class='col-md-12'>
              <p class="totop"> 
                <a href="#top">Back to top</a> 
              </p>
          </div>


@push('custom_scripts')
    <script>
        $(function () {
          console.log($(".chat-bubble:hidden"));
            $(".chat-bubble").slice(0, 8).show();
            $("#loadMore").on('click', function (e) {
                e.preventDefault();
                $(".chat-bubble:hidden").slice(0, 8).slideDown();
                if ($(".chat-bubble:hidden").length == 0) {
                    $("#load").fadeOut('slow');
                }
                $('html,body').animate({
                    scrollTop: $(this).offset().top
                }, 1500);
            });
        });

        $('a[href=#top]').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 600);
            return false;
        });

        $('#inbox').scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('.totop a').fadeIn();
            } else {
                $('.totop a').fadeOut();
            }
        });
    </script>
@endpush  
@endsection