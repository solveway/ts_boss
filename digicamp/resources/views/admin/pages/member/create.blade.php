@extends($view_path.'.layouts.master')
@section('content')
<form role="form" method="post" action="{{url($path)}}" enctype="multipart/form-data">
	<div class="portlet light bordered">
    	<div class="portlet-title">
			<div class="caption font-green">
				<i class="icon-layers font-green title-icon"></i>
				<span class="caption-subject bold uppercase"> {{$title}}</span>
			</div>
			<div class="actions">
				<a><button type="button" class="btn btn-success" onclick="add_member()">{{trans('general.create')}} {{trans('general.member')}}</button></a>
				<a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
			</div>
    	</div>
    	<div class="portlet-body form">
      		@include('admin.includes.errors')
  			<div class="row">
  				<input type="hidden" id="root-url" value="{{$path}}" />

  				{!!view($view_path.'.builder.text',['type' => 'text','name' => 'email','label' => 'Email', 'placeholder' => 'e.g. john@dash.co.id; glend@dash.co.id', 'attribute' => 'required autofocus', 'form_class' => 'col-md-6', 'value' => old('email')])!!}

  				<div class="col-md-12">
						  	<div class="form-group" style={{in_array(auth()->guard($guard)->user()->user_access_id,[1,12]) ? '' : "display:none;"}}>
				              <label for="tag">Store <span class="required" aria-required="true">*</span></label>
				              <select class="select2" name="store_allowed[]" multiple="multiple">
				                @if(!is_array(json_decode(auth()->guard($guard)->user()->store_id)) && in_array(auth()->guard($guard)->user()->store_id,[0,1]))
				                  <option value="0" {{old('store_allowed') ? (in_array(0,old('store_allowed')) ? 'selected' : '') : ''}}>-- All Store --</option>
				                @endif

				                @foreach($store as $s)
				                  <option value="{{$s->id}}" {{old('store_allowed') ? (in_array($s->id,old('store_allowed')) ? 'selected' : '') : 'selected'}}>{{$s->store_name}}</option>
				                @endforeach
				              </select>
				            </div>
				        </div>	
			</div>	
			{!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => 'Invite','ask' => 'y'])!!}
		</div>
	</div>
</form>
	<div class="modal fade bs-modal-lg" id="modal-member" tabindex="-1" role="dialog" aria-hidden="true">
		  <div class="modal-dialog modal-lg">
		    <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		          <h4 class="modal-title">Add Member</h4>
		        </div>
		        <div class="modal-body">
		        	<form role="form" method="post" action="{{url($path)}}" enctype="multipart/form-data">
			            <div class="row">
			           		 <div class ="col-md-6">
					            <div class="form-group form-md-line-input">
					              <input type="text" id="name" class="form-control" name="point" value="{{(old('name') ? old('name') : '')}}" placeholder="Name" required="required">
					              <label for="" id="point_label">Name <span class="required" aria-required="true">*</span></label>
					            </div>
					          </div>


					          <div class ="col-md-6">
					            <div class="form-group form-md-line-input">
					              <input type="text" id="price" class="form-control" name="price" value="{{(old('phone') ? old('phone') : '')}}" placeholder="Phone" required="required">
					              <label for="">Phone <span class="required" aria-required="true">*</span></label>
					            </div>
					          </div>

					          <div class ="col-md-6">
					            <div class="form-group form-md-line-input">
					              <input type="text" id="name" class="form-control" name="point" value="{{(old('email') ? old('email') : '')}}" placeholder="Email" required="required">
					              <label for="" id="point_label">Email <span class="required" aria-required="true">*</span></label>
					            </div>
					          </div>

					          <div class ="col-md-6">
					            <div class="form-group form-md-line-input">
					              <input type="text" id="name" class="form-control" name="point" value="{{(old('username') ? old('username') : '')}}" placeholder="Username" required="required">
					              <label for="" id="point_label">Username <span class="required" aria-required="true">*</span></label>
					            </div>
					          </div>

					          <div class ="col-md-6">
					            <div class="form-group form-md-line-input">
					              <input type="text" id="name" class="form-control" name="point" value="{{(old('password') ? old('password') : '')}}" placeholder="Password" required="required">
					              <label for="" id="point_label">Password <span class="required" aria-required="true">*</span></label>
					            </div>
					          </div>

					           <div class ="col-md-6">
					            <div class="form-group form-md-line-input">
					              <input type="text" id="name" class="form-control" name="point" value="{{(old('member_no') ? old('member_no') : '')}}" placeholder="Member No." required="required">
					              <label for="" id="point_label">Member No. <span class="required" aria-required="true">*</span></label>
					            </div>
					          </div>

					        <div class="col-md-12">
							  	<div class="form-group" style={{in_array(auth()->guard($guard)->user()->user_access_id,[1,12]) ? '' : "display:none;"}}>
					              <label for="tag">Store <span class="required" aria-required="true">*</span></label>
					              <select class="select2" name="store_allowed[]" multiple="multiple">
					                @if(!is_array(json_decode(auth()->guard($guard)->user()->store_id)) && in_array(auth()->guard($guard)->user()->store_id,[0,1]))
					                  <option value="0" {{old('store_allowed') ? (in_array(0,old('store_allowed')) ? 'selected' : '') : ''}}>-- All Store --</option>
					                @endif

					                 @foreach($store as $s)
					                  <option value="{{$s->id}}" {{old('store_allowed') ? (in_array($s->id,old('store_allowed')) ? 'selected' : '') : 'selected'}}>{{$s}}</option>
					                @endforeach
					              </select>
					            </div>
					        </div>		
			            </div>
			        </form>
		        </div>
		        <div class="modal-footer">
		          <button type="button" class="btn dark btn-outline cancel-crop" data-dismiss="modal" data-name="">cancel</button>
		          <button type="button" class="btn green save-crop" data-name="">Save changes</button>
		        </div>
		    </div>
		  </div>
	</div>
@push('custom_scripts')
	<script>
		function add_member(){
			$("#modal-member").modal({show:true,backdrop: 'static', keyboard: false});
		}
	</script>
@endpush
@endsection
