@extends($view_path.'.layouts.master')
@section('content')
@stack('scripts')
<script src="{{asset('components/plugins/amcharts/amcharts/amcharts.js')}}"></script>
<script src="{{asset('components/plugins/amcharts/amcharts/serial.js')}}"></script>
<!-- Load the JavaScript API client and Sign-in library. -->
<script src="https://apis.google.com/js/client:platform.js"></script>
@push('styles')

@endpush
<!-- <div class="row">
	<div class="col-md-12 dashboard coming-soon">
		<img class="img-responsive" src="{{asset('components/back/images/admin/dashboard.jpg')}}">
	</div>
</div> -->

	<div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat blue">
                <div class="visual">
                    <i class="fa fa-flag"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{$merchant}}">{{$merchant}}</span>
                    </div>
                    <div class="desc"> Jumlah Merchant </div>
                </div>
                <a class="more" href="{{url($root_path.'/merchants/manage-merchant')}}"> View more
                    <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat red">
                <div class="visual">
                    <i class="fa fa-newspaper-o"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{$news}}">{{$news}}</span> 
                    </div>
                    <div class="desc"> Jumlah News </div>
                </div>
                <a class="more" href="{{url($root_path.'/news/manage-news')}}"> View more
                    <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat green">
                <div class="visual">
                    <i class="fa fa-user"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{$customer}}">{{$customer}}</span>
                    </div>
                    <div class="desc"> Jumlah Customer </div>
                </div>
                <a class="more" href="{{url($root_path.'/accounts/customer')}}"> View more
                    <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat purple">
                <div class="visual">
                    <i class="fa fa-credit-card"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{$client}}">{{$client}}</span> </div>
                    <div class="desc"> Jumlah Client </div>
                </div>
                <a class="more" href="{{url($root_path.'/our-clients/manage-our-client')}}"> View more
                    <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-bar-chart font-green-haze"></i>
                                <span class="caption-subject bold uppercase font-green-haze">Top 5 Most Viewed Merchant</span>
                            </div>
                        </div>

                        <div class="portlet-body form">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Merchant</th>
                                        </tr>
                                        @foreach($merchant_log as $q => $mc_log)
                                        <tr>
                                            <td class="vcenter">{{ $q+1 }}</td>
                                            <td class="vcenter">{{ $mc_log->merchant_name }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

     <!-- BEGIN ROW -->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN CHART PORTLET-->
            <input type="hidden" id="merchant-category" value="{{$merchant_category}}">
            <input type="hidden" id="merchant-location" value="{{$merchant_location}}">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-bar-chart font-green-haze"></i>
                        <span class="caption-subject bold uppercase font-green-haze"> Merchant Charts</span>
                        <span class="caption-helper">column and line mix</span>
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                  <!--       <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                        <a href="javascript:;" class="reload"> </a> -->
                        <a href="javascript:;" class="fullscreen"> </a>
                        <!-- <a href="javascript:;" class="remove"> </a> -->
                    </div>
                    <div class="actions">
                        <div class="btn-group">
                            <a href="" class="btn dark btn-outline btn-circle btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> Filter Range
                                <span class="fa fa-angle-down"> </span>
                            </a>
                            <ul class="dropdown-menu pull-right merchant-category-ul">
                                <li class="active">
                                    <a href="javascript:;" onclick="getdata('merchants/manage-merchant/get_merchant_category', '7');">1 {{trans('general.week')}}
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;" onclick="getdata('merchants/manage-merchant/get_merchant_category', '30');">1 {{trans('general.month')}}
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;" onclick="getdata('merchants/manage-merchant/get_merchant_category', '90');">3 {{trans('general.month')}}
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- <div class="actions">
                        <div class="btn-group">
                            <a href="" class="btn dark btn-outline btn-circle btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> Filter Chart
                                <span class="fa fa-angle-down"> </span>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="javascript:;">1 {{trans('general.week')}}
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;">1 {{trans('general.month')}}
                                    </a>
                                </li>
                                <li class="active">
                                    <a href="javascript:;">3 {{trans('general.month')}}
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div> -->
                </div>
                <div class="portlet-body">
                    <!-- <div id="chart_1" class="chart_1" style="height: 500px;"> </div> -->
                    <div id="chart-merchant-category" class="chart" style="height: 500px;"> </div>
                    <!-- <div id="charts">
                      <div id="chartdiv1" style="height: 500px;"></div>
                      <div id="chartdiv2" style="height: 500px;"></div>
                    </div>
                    <div id="filter">
                      <label style="color: #67b7dc;"><input type="checkbox" value="Executive" checked="checked" onclick="applyFilters()" class="filter-position" /> Executive</label><br />
                      <label style="color: #fdd400;"><input type="checkbox" value="Research" checked="checked" onclick="applyFilters()" class="filter-position" /> Research</label><br />
                      <label style="color: #84b761;"><input type="checkbox" value="Marketing" checked="checked" onclick="applyFilters()" class="filter-position" /> Marketing</label><br />
                      <label style="color: #cc4748;"><input type="checkbox" value="Sales" checked="checked" onclick="applyFilters()" class="filter-position" /> Sales</label><br />

                      <label>
                        Name:
                        <input id="filter-name" type="text" onchange="applyFilters()" />
                      </label>
                    </div> -->
            </div>
            <!-- END CHART PORTLET-->
        </div>

          <div class="col-md-12">
            <!-- BEGIN CHART PORTLET-->
            <input type="hidden" id="merchant-category" value="{{$merchant_category}}">
            <input type="hidden" id="merchant-location" value="{{$merchant_location}}">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-bar-chart font-green-haze"></i>
                        <span class="caption-subject bold uppercase font-green-haze"> Merchant Charts</span>
                        <span class="caption-helper">column and line mix</span>
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                  <!--       <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                        <a href="javascript:;" class="reload"> </a> -->
                        <a href="javascript:;" class="fullscreen"> </a>
                        <!-- <a href="javascript:;" class="remove"> </a> -->
                    </div>
                    <div class="actions">
                        <div class="btn-group">
                            <a href="" class="btn dark btn-outline btn-circle btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> Filter Range
                                <span class="fa fa-angle-down"> </span>
                            </a>
                            <ul class="dropdown-menu pull-right merchant-location-ul">
                                <li class="active">
                                    <a href="javascript:;" onclick="getdata('merchants/manage-merchant/get_merchant_location', '7');">1 {{trans('general.week')}}
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;" onclick="getdata('merchants/manage-merchant/get_merchant_location', '30');">1 {{trans('general.month')}}
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;" onclick="getdata('merchants/manage-merchant/get_merchant_location', '90');">3 {{trans('general.month')}}
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="chart-merchant-location" class="chart" style="height: 500px;"> </div>
            </div>
            <!-- END CHART PORTLET-->
        </div>

        <div class="col-md-12">
            <!-- <h1>Hello Analytics Reporting API V4</h1> -->

            <!-- The Sign-in button. This will run `queryReports()` on success. -->
            <p class="g-signin2" data-onsuccess="queryReports"></p>

            <!-- The API response will be printed here. -->
            <!-- <textarea cols="80" rows="20" id="query-output"></textarea> -->

            <div id="chart-session" class="chart" style="height: 500px;"> </div>
        </div>
    </div>
    <!-- END ROW -->

@push('scripts')
<script type="text/javascript">
	$(document).ready(function(){
        $('ul.merchant-category-ul li').click(function(e) { 
           $('ul.merchant-category-ul li').removeClass('active');
            $(this).addClass('active');
        });

        $('ul.merchant-location-ul li').click(function(e) { 
           $('ul.merchant-location-ul li').removeClass('active');
            $(this).addClass('active');
        });
    });
        function getdata(url2, par){
            var url    = $.root() + url2 +'/' + par;
            // console.log(url);
            $.ajax({
                url: url,
                type: "GET",
                dataType: 'json',
                // data: par
            }).done(function(msg) {
                console.log(msg);
                chartData = msg;
                // var chartData = JSON.parse(msg);
                if(url2 == 'merchants/manage-merchant/get_merchant_category'){
                    chart.dataProvider = chartData;
                    chart.validateData();
                    checkEmptyData(chart);
                }else{
                    chart2.dataProvider = chartData;
                    chart2.validateData();
                    checkEmptyData(chart2);
                }
            }).fail(function(data){
              console.log(data.responseText);
            });
        }

		// var chartData = [{
		//     "country": "Otomotif",
		//     "visits": 3025,
		//     "color": "#FF0F00",
		//      subdata: [
		//         { country: "New York", visits: 1000  },
		//         { country: "California", visits: 785    },
		//         { country: "Florida", visits: 501    },
		//         { country: "Illinois", visits: 321   },
		//         { country: "Washington", visits: 101  }
		//     ] ,"color":"#FF0000"
		//   }, {
		//     "country": "Entertainment",
		//     "visits": 1882,
		//     "color": "#FF6600"
		//   }, {
		//     "country": "Food & Beverage",
		//     "visits": 1809,
		//     "color": "#FF9E01"
		//   }, {
		//     "country": "Health",
		//     "visits": 1322,
		//     "color": "#FCD202"
		//   },];

		// console.log(chartData);	
			// var chart1 = AmCharts.makeChart( "chart_1", {
			//   "type": "serial",
			// 	  "theme": "light",
			// 	  "marginRight": 70,
			// 	  "dataProvider": chartData,
			// 	  "valueAxes": [{
			// 	    "axisAlpha": 0,
			// 	    "position": "left",
			// 	    "title": "Visitors from Merchant Category"
			// 	  }],
			// 	  "startDuration": 1,
			// 	  "graphs": [{
			// 	    "balloonText": "<b>[[category]]: [[value]]</b>",
			// 	    "fillColorsField": "color",
			// 	    "fillAlphas": 0.9,
			// 	    "lineAlpha": 0.2,
			// 	    "type": "column",
			// 	    "valueField": "visits"
			// 	  }],
			// 	  "chartCursor": {
			// 	    "categoryBalloonEnabled": false,
			// 	    "cursorAlpha": 0,
			// 	    "zoomable": false
			// 	  },
			// 	  "categoryField": "country",
			// 	  "categoryAxis": {
			// 	    "gridPosition": "start",
			// 	    "labelRotation": 45
			// 	  },
			// 	  "export": {
			// 	    "enabled": true
			// 	  }
			// } );


		// var chartData = [
		// {
		//     category_name: "USA",
		//     visits: 4025,
		//     subdata: [
		//         { category_name: "New York", visits: 1000  },
		//         { category_name: "California", visits: 785    },
		//         { category_name: "Florida", visits: 501    },
		//         { category_name: "Illinois", visits: 321   },
		//         { category_name: "Washington", visits: 101  }
		//     ] ,"color":"#FF0000"
		// },
		// {
		//     category_name: "China",
		//     visits: 1882
		// ,"color":"#FF0000"},
		// {
		//     category_name: "Japan",
		//     visits: 1809
		// ,"color":"#FF0000"},
		// {
		//     category_name: "Germany",
		//     visits: 1322
		// ,"color":"#FF0000"}];
        var merchant_category = $('#merchant-category').val();
        // console.log(merchant_category);
        var chartData = JSON.parse(merchant_category);
        // console.log(chartData);
        var chart;
		AmCharts.ready(function() {
		    // SERIAL CHART
		    chart = new AmCharts.AmSerialChart();
		    chart.dataProvider = chartData;
		    chart.categoryField = "category_name";
		    chart.startDuration = 1;

		    // AXES
		    // category
		    var categoryAxis = chart.categoryAxis;
		    categoryAxis.labelRotation = 45;
		    categoryAxis.gridPosition = "start";

            var valueAxis = new AmCharts.ValueAxis();
            valueAxis.title = "Merchant Category";
            valueAxis.axisAlpha = 0;
            valueAxis.position = "left";
            chart.addValueAxis(valueAxis);
            // valueAxes.axisAlpha = 0;
            // valueAxes.position = "left";
            // valueAxes.title = "Visitors from Merchant Category";
		    // value
		    // in case you don't want to change default settings of value axis,
		    // you don't need to create it, as one value axis is created automatically.
		    // GRAPH
		    var graph = new AmCharts.AmGraph();
		    graph.valueField = "visits";
		    graph.colorField = "color"
		    graph.balloonText = "[[category]]: [[value]]";
		    graph.type = "column";
		    graph.lineAlpha = 0;
		    graph.fillAlphas = 0.8;
		    chart.addGraph(graph);
		    
		    chart.addListener("clickGraphItem", function (event) {
		        // let's look if the clicked graph item had any subdata to drill-down into
		        if (event.item.dataContext.subdata != undefined) {
		            // wow it has!
		            // let's set that as chart's dataProvider
		            event.chart.dataProvider = event.item.dataContext.subdata;
                    event.chart.categoryField = 'merchant_name';
		            event.chart.validateData();
		        }
		    });
            // console.log(chart);
            checkEmptyData(chart);
		    chart.write("chart-merchant-category");
		});

        var merchant_location = $('#merchant-location').val();
        // console.log(merchant_location);
        var chartData2 = JSON.parse(merchant_location);
        // console.log(chartData2);
        var chart2;
        AmCharts.ready(function() {
            // SERIAL CHART
            chart2 = new AmCharts.AmSerialChart();
            chart2.dataProvider = chartData2;
            chart2.categoryField = "city_name";
            chart2.startDuration = 1;

            // AXES
            // category
            var categoryAxis = chart2.categoryAxis;
            categoryAxis.labelRotation = 45;
            categoryAxis.gridPosition = "start";

            var valueAxis = new AmCharts.ValueAxis();
            valueAxis.title = "Merchant Location";
            valueAxis.axisAlpha = 0;
            valueAxis.position = "left";
            chart2.addValueAxis(valueAxis);
            // valueAxes.axisAlpha = 0;
            // valueAxes.position = "left";
            // valueAxes.title = "Visitors from Merchant Category";
            // value
            // in case you don't want to change default settings of value axis,
            // you don't need to create it, as one value axis is created automatically.
            // GRAPH
            var graph = new AmCharts.AmGraph();
            graph.valueField = "visits";
            graph.colorField = "color"
            graph.balloonText = "[[category]]: [[value]]";
            graph.type = "column";
            graph.lineAlpha = 0;
            graph.fillAlphas = 0.8;
            chart2.addGraph(graph);
            
            chart2.addListener("clickGraphItem", function (event) {
                // let's look if the clicked graph item had any subdata to drill-down into
                if (event.item.dataContext.subdata != undefined) {
                    // wow it has!
                    // let's set that as chart's dataProvider
                    event.chart2.dataProvider = event.item.dataContext.subdata;
                    event.chart2.categoryField = 'merchant_name';
                    event.chart2.validateData();
                }
            });
            checkEmptyData(chart2);
            chart2.write("chart-merchant-location");
        });

        var arr2 = [];
        var chart3 = AmCharts.makeChart( "chart-session", {
          "type": "serial",
          "dataProvider": arr2,
          "creditsPosition": "top-right",
          "categoryField": "country",
          "categoryAxis": {
            "gridAlpha": 0.07,
            "gridPosition": "start",
            "tickPosition": "start",
            "title": "Country"
          },
          "valueAxes": [ {
            "id": "v1",
            "gridAlpha": 0.07,
            "title": "Users/Sessions"
          }, {
            "id": "v2",
            "gridAlpha": 0,
            "position": "right",
            "title": "Page views"
          } ],
          "graphs": [ {
            "type": "column",
            "title": "Sessions",
            "valueField": "sessions",
            "lineAlpha": 0,
            "fillAlphas": 0.6
          }, {
            "type": "column",
            "title": "Users",
            "valueField": "users",
            "lineAlpha": 0,
            "fillAlphas": 0.6
          }, {
            "type": "line",
            "valueAxis": "v2",
            "title": "Page views",
            "valueField": "pageviews",
            "lineThickness": 2,
            "bullet": "round"
          } ],
          "legend": {}
        } );



    // Replace with your view ID.
          var VIEW_ID = '155600948';
          // Query the API and print the results to the page.
          function queryReports() {
            gapi.client.request({
              path: '/v4/reports:batchGet',
              root: 'https://analyticsreporting.googleapis.com/',
              method: 'POST',
              body: {
                reportRequests: [
                    {
                        viewId: VIEW_ID,
                        dateRanges: [
                          {
                            startDate: '7daysAgo',
                            endDate: 'today'
                          }
                        ],
                        metrics: [
                            {expression: "ga:pageviews"},
                            {expression: 'ga:sessions'},
                            {expression: 'ga:users'}
                        ],
                        "dimensions": [{"name": "ga:country"}],
                        "orderBys": [
                            {"fieldName": "ga:country"}
                          ]
                    }
                ]
              }
            }).then(displayResults, console.error.bind(console));
            $('.g-signin2').hide();
          }

            function checkEmptyData(charts) {
                // console.log(charts);
              if (0 == charts.dataProvider.length) {
                // add label
                charts.addLabel(0, '50%', 'The chart contains no data', 'center');
                // chart.validateNow();
              }else{
                console.log(charts.allLabels);
                charts.allLabels = [];
                // if(charts.allLabels[0]){
                //     charts.allLabels[0]['text'] = '';
                // }
              }
              charts.validateNow();
            }

          function displayResults(response) {
            var array = response.result.reports[0].data.rows;
            // console.log(array);
            array.forEach(function(item, index, arr) {
                var tmp = {
                    'country'   : item.dimensions[0],
                    'browser'   : item.dimensions[1],
                    'pageviews' : item.metrics[0].values[0],
                    'sessions'  : item.metrics[0].values[1],
                    'users'     : item.metrics[0].values[2],
                }
                // console.log(tmp);
                arr2.push(tmp);
                 // console.log(arr);
                // console.log(arr[index]);
                // console.log(item);
            });
            // console.log(arr2);
            var formattedJson = JSON.stringify(response.result, null, 2);

            // document.getElementById('query-output').value = formattedJson;
            
            chart3.dataProvider = arr2;
            chart3.validateData();
          }
</script>
@endpush
@endsection
