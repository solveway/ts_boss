@extends($view_path.'.layouts.master')
@section('content')
@section('content')
<style>
 /* .single-image-card{
    max-width: 1024px !important;
    max-height: 714px !important;
  }*/
</style>
<!-- croppie -->
<link rel = "stylesheet" href="{{asset('components/back/css/croppie.css')}}" type="text/css">
<!-- <link rel = "stylesheet" href="{{asset('components/back/css/demo.css')}}" type="text/css"> -->
<!-- croppie -->
@push('styles')
<style>
  .modal-dialog {
    width: 1300px;
    height: 100%;
    margin: 0;
    padding: 0;
  }

  .modal-content {
    height: auto;
    min-height: 100%;
    border-radius: 0;
  }
  
  .canvas-image_card{
    width: 1100px;
    height: 800px;
  }
</style>

<form role="form" method="post" action="{{url($path)}}" enctype="multipart/form-data">
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      <div class="row">

        {!!view($view_path.'.builder.select',['name' => 'membership_type','label' => 'Member Type','value' => (old('membership_type') ? old('membership_type') : '1'),'attribute' => 'required','form_class' => 'col-md-12', 'data' => $membership_type, 'class' => 'select2 select_membership'])!!}

        {!!view($view_path.'.builder.select',['name' => 'store_type','label' => 'Store Type','value' => (old('store_type') ? old('store_type') : '1'),'attribute' => 'required','form_class' => 'col-md-6', 'data' => $store_type, 'class' => 'select2'])!!}

        {!!view($view_path.'.builder.select',['name' => 'category_store','label' => 'Category Store','value' => (old('category_store') ? old('category_store') : '1'),'attribute' => 'required','form_class' => 'col-md-6', 'data' => $category_store, 'class' => 'select2'])!!}

        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'name','label' => 'Name','value' => (old('name') ? old('name') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6'])!!}

        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'address','label' => 'Address','value' => (old('address') ? old('address') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6'])!!}

        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'phone','label' => 'Phone','value' => (old('phone') ? old('phone') : ''),'attribute' => 'required','form_class' => 'col-md-6'])!!}

        {!!view($view_path.'.builder.text',['type' => 'email','name' => 'email','label' => 'Email','value' => (old('email') ? old('email') : ''),'attribute' => 'required','form_class' => 'col-md-6'])!!}

        {!!view($view_path.'.builder.textarea',['type' => 'textarea','name' => 'description','label' => 'Description','value' => (old('description') ? old('description') : ''),'attribute' => '','form_class' => 'col-md-12'])!!}

         <div class ="col-md-6">
            <div class="form-group form-md-line-input" pattern="[0-9]*">
              <input type="number" id="point" class="form-control" name="point" value="{{(old('point') ? old('point') : '')}}" placeholder="Point" required="required">
              <label for="" id="point_label">Point / Stamp <span class="required" aria-required="true">*</span></label>
            </div>
          </div>


          <div class ="col-md-6">
            <div class="form-group form-md-line-input">
              <input type="text" id="price" class="form-control" name="price" value="{{(old('price') ? old('price') : '')}}" placeholder="Point" required="required">
              <label for="">Price (Rp.) <span class="required" aria-required="true">*</span></label>
            </div>
          </div>


         {!!view($view_path.'.builder.file',['name' => 'logo','label' => 'Store Logo','value' => '','type' => 'file','file_opt' => ['path' => $image_path],'upload_type' => 'single-image','class' => 'col-md-12','note' => 'Note: File Must jpeg,png,jpg | Best Resolution: 500 x 500 px','form_class' => 'logo col-md-12', 'attribute' => 'required autofocus'])!!}  

          {!!view($view_path.'.builder.file',['name' => 'image_store','label' => 'Image Store','value' => '','type' => 'file','file_opt' => ['path' => $image_path],'upload_type' => 'single-image','class' => 'col-md-12','note' => 'Note: File Must jpeg,png,jpg | Best Resolution: 1000 x 500 px','form_class' => 'image_store col-md-12', 'attribute' => 'required autofocus'])!!}  

        <div id="image-card">
          <div class="form-group form-md-line-input col-md-12">
            <label>Image Card</label><br>
            <label class="btn green input-file-label-image_card">
              <input type="file" class="form-control col-md-12 image_card single-image file-image-card" name="image_card"> Pilih File
            </label>
                <button type="button" class="btn red-mint remove-single-image" data-id="single-image" data-name="image_card">Hapus</button>
              <input type="hidden" name="remove-single-image-image_card" value="n">
              <br>
            <small>Note: File Must jpeg,png,jpg,gif | Best Resolution: 1024 x 714 px</small>
          </div>

          <div class="form-group single-image-image_card col-md-12">
            <img src="{{url($image_path2)}}/none.png" class="img-responsive preview-image_card thumbnail single-image-thumbnail">
          </div>
        </div>

       <!--  <div id="image-card">
          <div class="form-group form-md-line-input col-md-12">
            <label>Image Card</label><br>
            <label class="btn green input-file-label-image_card">
              <input type="file" class="form-control col-md-12 image_card single-image-2 file-image-card" name="image_card" data-crop="true"> Pilih File
            </label>
                <button type="button" class="btn red-mint remove-single-image" data-id="single-image" data-name="image_card">Hapus</button>
              <input type="hidden" name="remove-single-image-image_card" value="n">
              <br>
            <small>Note: File Must jpeg,png,jpg,gif | Best Resolution: 1024 x 714 px</small>
          </div>

          <div class="form-group single-image-image_card col-md-12">
            <img src="{{url($image_path2)}}/none.png" class="img-responsive preview-image_card thumbnail single-image-thumbnail">
          </div>
        </div> -->

        <!-- <div id="image-stamp">
           <div class="form-group form-md-line-input col-md-12">
              <label>Image Stamp</label><br>
              <label class="btn green input-file-label-image_stamp">
                <input type="file" class="form-control col-md-12 image_stamp single-image-2" name="image_stamp" data-crop="true"> Pilih File
                  </label>
                  <button type="button" class="btn red-mint remove-single-image" data-id="single-image" data-name="image_stamp">Hapus</button>
                <input type="hidden" name="remove-single-image-image_stamp" value="n">
                <br>
              <small>Note: File Must jpeg,png,jpg,gif | Best Resolution: 500 x 500 px</small>
            </div>
            <div class="form-group single-image-image_stamp col-md-12">
              <img src="{{url($image_path2)}}/none.png" class="img-responsive thumbnail single-image-thumbnail">
            </div>
        </div> -->

        <div id="image-stamp">
           <div class="form-group form-md-line-input col-md-12">
              <label>Image Stamp</label><br>
              <label class="btn green input-file-label-image_stamp">
                <input type="file" class="form-control col-md-12 image_stamp single-image" name="image_stamp"> Pilih File
                  </label>
                  <button type="button" class="btn red-mint remove-single-image" data-id="single-image" data-name="image_stamp">Hapus</button>
                <input type="hidden" name="remove-single-image-image_stamp" value="n">
                <br>
              <small>Note: File Must jpeg,png,jpg,gif | Best Resolution: 500 x 500 px</small>
            </div>
            <div class="form-group single-image-image_stamp col-md-12">
              <img src="{{url($image_path2)}}/none.png" class="img-responsive thumbnail single-image-thumbnail">
            </div>
        </div>

        <div class="col-md-12">
            <input id="pac-input" class="controls" type="text" placeholder="Search Box">
            <div id="map" class="gmaps_outlet"> </div>
            <input type="hidden" name="latitude" id="latbox" value="{{(old('latitude') ? old('latitude') : $default_lat)}}"></button>
            <input type="hidden" name="longitude" id="lngbox" value="{{(old('longitude') ? old('longitude') : $default_lng)}}"></button>
        </div>
        <div class="clearfix"></div>
        <hr/>
       <!--  {!!view($view_path.'.builder.text',['type' => 'text','name' => 'lat','label' => 'Latitude','value' => (old('email') ? old('email') : ''),'attribute' => 'required','form_class' => 'col-md-12'])!!} 

        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'long','label' => 'Longitude','value' => (old('email') ? old('email') : ''),'attribute' => 'required','form_class' => 'col-md-12'])!!} --> 

        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'region_kecamatan_id','label' => 'Kecamatan','value' => (old('region_kecamatan_id') ? old('region_kecamatan_id') : ''),'attribute' => 'required','form_class' => 'col-md-12', 'class' => 'select2, region_kecamatan_id'])!!}

        <input type="hidden" id="kecamatan_id" name="kecamatan_id" value="{{(old('kecamatan_id') ? old('kecamatan_id') : '')}}"/>  
        <input type="hidden" id="image_card_result" name="image_card_result"/>  
        <input type="hidden" id="image_stamp_result" name="image_stamp_result"/>  

        <div class="col-md-12 actions">
          {!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
        </div>
    </div>
  </div>
</form>

    <!-- Cropping modal -->
    <div class="modal fade" id="crop-modal-image_card" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title" id="avatar-modal-label">Change Image</h4>
            </div>
            <div class="modal-body">
              <div class="avatar-body">

                <!-- Crop and preview -->
                <div class="row">
                  <div class="canvas-image_card">
                    <div class="image-crop-data-image_card" style="height:714px;"></div>
                  </div>
                  <div class="col-md-3">
                    <div class="avatar-preview preview-lg"></div>
                    <div class="avatar-preview preview-md"></div>
                    <div class="avatar-preview preview-sm"></div>
                  </div>
                </div>

                <div class="row avatar-btns">
                  <div class="col-md-9">
                    <div class="btn-group">
                      <button type="button" class="btn btn-primary" id="crop-image_card" data-method="rotate" data-option="" title="">Crop</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div><!-- /.modal -->

    <!-- Cropping modal -->
    <div class="modal fade" id="crop-modal-image_stamp" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title" id="avatar-modal-label">Change Image</h4>
            </div>
            <div class="modal-body">
              <div class="avatar-body">

                <!-- Crop and preview -->
                <div class="row">
                  <div class="canvas-image_card">
                    <div class="image-crop-data-image_stamp" style="height:500px;"></div>
                  </div>
                  <div class="col-md-3">
                    <div class="avatar-preview preview-lg"></div>
                    <div class="avatar-preview preview-md"></div>
                    <div class="avatar-preview preview-sm"></div>
                  </div>
                </div>

                <div class="row avatar-btns">
                  <div class="col-md-9">
                    <div class="btn-group">
                      <button type="button" class="btn btn-primary" id="crop-image_stamp" data-method="rotate" data-option="" title="">Crop</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div><!-- /.modal -->
@push('scripts')

@endpush
@push('custom_scripts')
  <script>
    var map;
    $(document).ready(function(){
      $("#image-card").hide();
      $("#image-stamp").hide();

      $(document).on('change','.select_membership',function(res){
          var res2 = $(".select_membership").val();
          console.log(res2);
          $("#image-card").hide();
          $("#image-stamp").hide();
          showHideImage(res2)
      });

      showHideImage($(".select_membership").val())

      $(document).on('keyup','.region_kecamatan_id',function(res){
        console.log(res);
        var text = $(".region_kecamatan_id").val();
        if(text.length != undefined && text.length >= 5){
          getKecamatan(text);
        } 
      });
        // var data = [];
       // console.log(data);
      
      function getKecamatan(text){
        // var merchant_id = $('.select-merchant option:selected').val();
        // $(".select-store").children('option:not(:first)').remove(); 
        // $("#select2-store_id-eb-container").text("");   
        var url    = $.root() + 'merchants/store/get_kecamatan/' + text;
        console.log(url);
          $.ajax({
          url: url,
          type: "GET",
          dataType: 'json',
          // data: par
          }).done(function(msg) {
              var data = [];
              $.each(msg, function( index, value ) {
                var arr={
                  'value': index,
                  'label': value
                }

                data.push(arr);
              });
              console.log(data);

              $( ".region_kecamatan_id" ).autocomplete({
                  minLength: 0,
                  source: data,
                  focus: function( event, ui ) {
                    console.log(ui);
                    $( ".region_kecamatan_id" ).val( ui.item.label );
                    return false;
                  },
                  select: function( event, ui ) {
                    console.log(ui.item);
                    $( ".region_kecamatan_id" ).val( ui.item.label );
                    $( "#kecamatan_id" ).val( ui.item.value );

                    return false;
                  }
              })
          });
      }

      function showHideImage(res2){
        if(res2 == '3'){
            $("#image-card").show();
            $("#image-stamp").show();
            $("#point_label").text('Stamp');
        }else if(res2 == '1' || res2 == '2'){
            $("#image-card").show();
             $("#point_label").text('Point');
        }
      }

      setTimeout(function(){ 
          var lat = $('#latbox').val();
          var lng = $('#lngbox').val();

          google.maps.event.trigger(map, 'resize'); 
        
          Markerlatlng = new google.maps.LatLng(lat, lng);

          map.setCenter(Markerlatlng); // setCenter takes a LatLng object
        }, 2500);

      document.getElementById("price").onblur =function (){    

        //number-format the user input
        this.value = parseFloat(this.value.replace(/,/g, ""))
                    .toFixed(2)
                    .toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      }
     
      function popupResult(result) {
        var html;
        if (result.html) {
          html = result.html;
        }
        if (result.src) {
          html = '<img src="' + result.src + '" />';
        }
        swal({
          title: '',
          html: true,
          text: html,
          allowOutsideClick: true
        });
        setTimeout(function(){
          $('.sweet-alert').css('margin', function() {
            var top = -1 * ($(this).height() / 2),
              left = -1 * ($(this).width() / 2);

            return top + 'px 0 0 ' + left + 'px';
          });
        }, 1);
      }

      $(document).on('change','.single-image-2',function(res){
        console.log('.single-image-2');
        if(res){
          name_method = $(this).attr('name');
          crop        = $(this).data('crop');
          target_crop = $('<img class="image-crop-data-'+name_method+'">');
          console.log(this);
          files       = this.files;
          console.log(this.files);
          ext         = files[0].name.split('.')[1].toLowerCase();
          size        = files[0].size;
          allow_ext   = ['jpg','gif','png','jpeg'];
          var width;
          var height;
          if($.inArray(ext,allow_ext) > -1){
            if(size <= 2000000){
              if(crop == undefined){
                $.base64image(files).done(function(res){
                  file = "<img src="+res+" class='preview-"+name_method+" single-image-card upload-image_card'><button type='button' class='btn green btn greenask upload-result'>Simpan</button>";
                  $('input[name="remove-single-image-'+name_method+'"]').val('n');
                  $('.single-image-'+name_method).html(file);
                  
                  if(name_method == 'image_card'){
                    width = 1024;
                    height = 714;
                  }
                    var uploadCrop = $(".preview-"+name_method).croppie({
                      // enableExif: true,
                      // enforceBoundary: false,
                      viewport: {
                          'width': 1024,
                          'height': 714,
                      },
                      // boundary: {
                      //     width: width,
                      //     height: height,
                      // }
                   
                  });

                    // uploadCrop.croppie('bind', {
                    //     url: res,
                    //     point: [0,0,0,0],
                    // });

                    $('.upload-result').on('click', function (ev) {
                        console.log(ev);
                        uploadCrop.croppie('result', {
                            type: 'canvas',
                            size: 'viewport',
                            format: 'jpeg'
                        }).then(function (resp) {
                          console.log(resp);
                            console.log('name_method: '+name_method);
                            // console.log(resp);
                            $('#'+name_method+'_result').val(resp);
                            file = "<img src='"+resp+"' class='img-responsive thumbnail preview-"+name_method+" single-image-card upload-image_card'><button type='button' class='btn green btn greenask upload-result'> Simpan</button><button type='button' class='btn blue btn result'> Result</button>";
                            $('input[name="remove-single-image-'+name_method+'"]').val('n');
                            $('.single-image-'+name_method).html(file);
                            // $.base64image(resp).done(function(res){
                            //   console.log(res);
                            // });
                            // popupResult({
                            //   src: resp
                            // });
                        });
                    });

                });
              }else{
                // $('.container-crop-'+name_method).addClass('hidden');
                // $('.waiting-crop-'+name_method).removeClass('hidden');
                // $('.image-crop-data-image_card').addClass('hidden');
                // $('.image-crop-data-image_stamp').addClass('hidden');
                // $('#crop-image_card').addClass('hidden');
                // $('#crop-image_stamp').addClass('hidden');
                var minCropBoxWidth;
                var minCropBoxHeight;
                $.base64image(files).done(function(res){
                  $('#crop-modal-'+name_method).modal('show');      
                  setTimeout(function(){
                    console.log(name_method);
                    // $('.image-crop-data-'+name_method).removeClass('hidden');
                    // $('#crop-'+name_method).removeClass('hidden');
                    if(name_method == 'image_card'){
                      minCropBoxWidth = 1024;
                      minCropBoxHeight = 714;
                    }
                    // else if(name_method == 'image_stamp'){
                    //   minCropBoxWidth = 500;
                    //   minCropBoxHeight = 500;
                    // }
                    console.log(minCropBoxWidth+'-'+minCropBoxHeight);
                    $('.image-crop-data-'+name_method).cropper({
                        aspectRatio: 0,
                        // preview: this.$avatarPreview.selector,
                        minCropBoxWidth: minCropBoxWidth,
                        minCropBoxHeight: minCropBoxHeight,
                        // viewMode: 1,
                        dragMode: 'move',
                        autoCropArea: 0,
                        restore: false,
                        guides: false,
                        highlight: false,
                        cropBoxMovable: false,
                        cropBoxResizable: false,
                    }).cropper('replace', res);
                    // $('.waiting-crop').addClass('hidden');
                    // $('.container-crop').removeClass('hidden');
                  }, 500);

                  $('#crop-'+name_method).on('click', function(e){
                    console.log(e);
                    var resp = $('.image-crop-data-'+name_method).cropper('getCroppedCanvas', {
                                  width: minCropBoxWidth,
                                  height: minCropBoxHeight
                                }).toDataURL('image/jpeg');
                    $('#'+name_method+'_result').val(resp);
                    file = "<img src='"+resp+"' class='img-responsive thumbnail preview-"+name_method+" single-image-thumbnail single-image-card upload-image_card'>";
                    $('input[name="remove-single-image-'+name_method+'"]').val('n');
                    $('.single-image-'+name_method).html(file);
                     $('#crop-modal-'+name_method).modal('hide');
                  });
                });
              }
            }else{
              alert("File size is to large");
            }
          }else{
            file = "<img src='"+$.root()+"../components/both/images/web/none.png' class='img-responsive thumbnail single-image-thumbnail'>";
            $(this).val(null);
            $('.single-image-'+name_method).html(file);
            $('input[name="remove-single-image-'+name_method+'"]').val('y');
            alert ("File must image");
          }
        }
      });

      // var $uploadCrop;

      // function readFile(input) {
      //   name_method = $(input).attr('name');
      //   if (input.files && input.files[0]) {
      //           var reader = new FileReader();
                
      //           reader.onload = function (e) {
      //       // $('.upload-demo').addClass('ready');
      //             $uploadCrop.croppie('bind', {
      //               url: e.target.result
      //             }).then(function(){
      //               console.log('jQuery bind complete');
      //             });
                  
      //           }
                
      //           reader.readAsDataURL(input.files[0]);
      //       }
      //       else {
      //         swal("Sorry - you're browser doesn't support the FileReader API");
      //     }
      // }

      
      // $('.single-image-2').on('change', function () {
      //     var name_method = $(this).attr('name');
      //     console.log(name_method);
      //     $uploadCrop = $('.preview-'+name_method).croppie({
      //       viewport: {
      //         width: 512,
      //         height: 357,
      //         // type: 'circle'
      //       },
      //       enableExif: true
      //     });

      //    readFile(this); 
      // });

    });

     function initAutocomplete() {
        var lat = parseFloat($('#latbox').val());
        var lng = parseFloat($('#lngbox').val());
        
        markers = [];
        map = new google.maps.Map(document.getElementById('map'), {
          zoom: 12,
        }); 

        var marker = new google.maps.Marker({
            position: {lat: lat, lng: lng},
            map: map,
            draggable:true,
            zoom:12
        });

        markers.push(marker);

        google.maps.event.addListener(marker, 'dragstart', function(event){
            document.getElementById("latbox").value = event.latLng.lat();
            document.getElementById("lngbox").value = event.latLng.lng();
        });

        google.maps.event.addListener(marker, 'dragend', function(event){
            document.getElementById("latbox").value = event.latLng.lat();
            document.getElementById("lngbox").value = event.latLng.lng();
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          for(var i = 0;i < 1; i++){
            place = places[i];
            var marker = new google.maps.Marker({
              map: map,
              title: place.name,
              position: place.geometry.location,
              draggable:true,
              zoom:1
            });
            markers.push(marker);
            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }

            document.getElementById("latbox").value = place.geometry.location.lat();
            document.getElementById("lngbox").value = place.geometry.location.lng();

            google.maps.event.addListener(marker, 'dragstart', function(event){
                document.getElementById("latbox").value = event.latLng.lat();
                document.getElementById("lngbox").value = event.latLng.lng();
            });
            google.maps.event.addListener(marker, 'dragend', function(event){
                document.getElementById("latbox").value = event.latLng.lat();
                document.getElementById("lngbox").value = event.latLng.lng();
            });
          }
          map.fitBounds(bounds);
        });
    }
  </script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBpMppLOBQyYyKg11P20E9pe4Hs-cwlH9U&libraries=places&callback=initAutocomplete" async defer></script>
  <!-- croppie -->
  <script src="{{asset('components/back/js/croppie.js')}}"></script>
  <!-- <script src="{{asset('components/back/js/demo.js')}}"></script> -->
   <!-- <script>
            Demo.init();
        </script> -->
  <!-- croppie -->
@endpush
@endsection
