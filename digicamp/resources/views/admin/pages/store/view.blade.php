@extends($view_path.'.layouts.master')
@section('content')
<form role="form" method="post" action="{{url($path)}}/{{$store->id}}" enctype="multipart/form-data">
  {{ method_field('PUT') }}
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <div class="actions">
          <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
        </div>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      <div class="row">
        {!!view($view_path.'.builder.select',['name' => 'membership_type','label' => 'Member Type','value' => (old('membership_type') ? old('membership_type') : $store->membership_type),'attribute' => 'required','form_class' => 'col-md-12', 'data' => $membership_type, 'class' => 'select2'])!!}

        {!!view($view_path.'.builder.select',['name' => 'store_type','label' => 'Store Type','value' => (old('store_type') ? old('store_type') : $store->store_type),'attribute' => 'required','form_class' => 'col-md-6', 'data' => $store_type, 'class' => 'select2'])!!}

        {!!view($view_path.'.builder.select',['name' => 'category_store','label' => 'Category Store','value' => (old('category_store') ? old('category_store') : $store->category_id),'attribute' => 'required','form_class' => 'col-md-6', 'data' => $category_store, 'class' => 'select2'])!!}

        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'name','label' => 'Name','value' => (old('name') ? old('name') : $store->store_name),'attribute' => 'required autofocus','form_class' => 'col-md-6'])!!}

        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'address','label' => 'Address','value' => (old('address') ? old('address') : $store->address),'attribute' => 'required autofocus','form_class' => 'col-md-6'])!!}

        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'phone','label' => 'Phone','value' => (old('phone') ? old('phone') : $store->phone),'attribute' => 'required','form_class' => 'col-md-6'])!!}

        {!!view($view_path.'.builder.text',['type' => 'email','name' => 'email','label' => 'Email','value' => (old('email') ? old('email') : $store->email),'attribute' => 'required','form_class' => 'col-md-6'])!!}

        {!!view($view_path.'.builder.textarea',['type' => 'textarea','name' => 'description','label' => 'Description','value' => (old('description') ? old('description') : $store->description),'attribute' => 'required','form_class' => 'col-md-12'])!!}

         <div class ="col-md-6">
            <div class="form-group form-md-line-input" pattern="[0-9]*">
              <input type="number" id="point" class="form-control" name="point" value="{{$store->point}}" placeholder="Point">
              <label for="">Point / Stamp</label>
            </div>
          </div>


          <div class ="col-md-6">
            <div class="form-group form-md-line-input">
              <input type="text" id="price" class="form-control" name="price" value="{{$store->price}}" placeholder="Price">
              <label for="">Price (Rp.)</label>
            </div>
          </div>

         {!!view($view_path.'.builder.file',['name' => 'logo','label' => 'Store Logo','value' => $store->logo,'type' => 'file','file_opt' => ['path' => $image_path],'upload_type' => 'single-image','class' => 'col-md-12','note' => 'Note: File Must jpeg,png,jpg,gif | Best Resolution: 138 x 44 px','form_class' => 'col-md-12'])!!} 

          {!!view($view_path.'.builder.file',['name' => 'image_store','label' => 'Image Store','value' => $store->image_store,'type' => 'file','file_opt' => ['path' => $image_path],'upload_type' => 'single-image','class' => 'col-md-12','note' => 'Note: File Must jpeg,png,jpg | Best Resolution: 1000 x 500 px','form_class' => 'col-md-12', 'attribute' => 'required'])!!}

         <div id="image-card">
          <div class="form-group form-md-line-input col-md-12">
            <label>Image Card</label><br>
            <label class="btn green input-file-label-image_card">
              <input type="file" class="form-control col-md-12, image_card single-image" name="image_card"> Pilih File
            </label>
                <button type="button" class="btn red-mint remove-single-image" data-id="single-image" data-name="image_card">Hapus</button>
              <input type="hidden" name="remove-single-image-image_card" value="n">
              <br>
            <small>Note: File Must jpeg,png,jpg,gif | Best Resolution: 138 x 44 px</small>
          </div>

          <div class="form-group single-image-image_card col-md-12">
            <img src="{{asset($image_path.$store->image_card)}}" class="img-responsive thumbnail single-image-thumbnail">
          </div>
        </div>

        <div id="image-stamp">
           <div class="form-group form-md-line-input col-md-12">
              <label>Image Stamp</label><br>
              <label class="btn green input-file-label-image_stamp">
                <input type="file" class="form-control col-md-12, image_card single-image" name="image_stamp" value="$image_path {{$store->image_card}}"> Pilih File
                  </label>
                  <button type="button" class="btn red-mint remove-single-image" data-id="single-image" data-name="image_stamp">Hapus</button>
                <input type="hidden" name="remove-single-image-image_stamp" value="n">
                <br>
              <small>Note: File Must jpeg,png,jpg,gif | Best Resolution: 138 x 44 px</small>
            </div>
            <div class="form-group single-image-image_stamp col-md-12">
              <img src="{{asset($image_path.$store->image_stamp)}}" class="img-responsive thumbnail single-image-thumbnail">
            </div>
        </div>

        <div class="col-md-12">
            <input id="pac-input" class="controls" type="text" placeholder="Search Box">
            <div id="map" class="gmaps_outlet"> </div>
            <input type="hidden" name="latitude" id="latbox" value="{{(old('latbox') ? old('latbox') : $store->lat)}}"></button>
            <input type="hidden" name="longitude" id="lngbox" value="{{(old('lngbox') ? old('lngbox') : $store->long)}}"></button>
        </div>
        <div class="clearfix"></div>
        <hr/>
         {!!view($view_path.'.builder.select',['name' => 'region_kecamatan_id','label' => 'Kecamatan','value' => (old('category_store') ? old('category_store') : $store->region_kecamatan_id),'attribute' => 'required','form_class' => 'col-md-12', 'data' => $kecamatan, 'class' => 'select2'])!!}
      </div>
    </div>
  </div>
</form>
@push('scripts')

@endpush
@push('custom_scripts')
  <script>
    var map;
    $(document).ready(function(){
      $('input,select,checkbox,button.remove-single-image').attr('disabled',true);
      $("#image-card").hide();
      $("#image-stamp").hide();

      setTimeout(function(){ 
        var lat = $("#latbox").val();
        var lng = $("#lngbox").val();

        google.maps.event.trigger(map, 'resize'); 
      
        Markerlatlng = new google.maps.LatLng(lat, lng);
        map.setCenter(Markerlatlng); // setCenter takes a LatLng object
      }, 2500);

      var member_type_id = {{$store->membership_type}};
      console.log(member_type_id);
      showHideImage(member_type_id);
      function showHideImage(res2){
        if(res2 == '3'){
            $("#image-card").show();
            $("#image-stamp").show();
        }else if(res2 == '1' || res2 == '2'){
            $("#image-card").show();
        }
      }
    });

     function initAutocomplete() {
        var lat = parseFloat($("#latbox").val());
        var lng = parseFloat($("#lngbox").val());

        markers = [];
        map = new google.maps.Map(document.getElementById('map'), {
          zoom: 12,
        }); 

        var marker = new google.maps.Marker({
            position: {lat: lat, lng: lng},
            map: map,
            draggable:true,
            zoom:12
        });

        markers.push(marker);

        google.maps.event.addListener(marker, 'dragstart', function(event){
            document.getElementById("latbox").value = event.latLng.lat();
            document.getElementById("lngbox").value = event.latLng.lng();
        });

        google.maps.event.addListener(marker, 'dragend', function(event){
            document.getElementById("latbox").value = event.latLng.lat();
            document.getElementById("lngbox").value = event.latLng.lng();
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          for(var i = 0;i < 1; i++){
            place = places[i];
            var marker = new google.maps.Marker({
              map: map,
              title: place.name,
              position: place.geometry.location,
              draggable:true,
              zoom:1
            });
            markers.push(marker);
            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }

            document.getElementById("latbox").value = place.geometry.location.lat();
            document.getElementById("lngbox").value = place.geometry.location.lng();

            google.maps.event.addListener(marker, 'dragstart', function(event){
                document.getElementById("latbox").value = event.latLng.lat();
                document.getElementById("lngbox").value = event.latLng.lng();
            });
            google.maps.event.addListener(marker, 'dragend', function(event){
                document.getElementById("latbox").value = event.latLng.lat();
                document.getElementById("lngbox").value = event.latLng.lng();
            });
          }
          map.fitBounds(bounds);
        });
    }
  </script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBpMppLOBQyYyKg11P20E9pe4Hs-cwlH9U&libraries=places&callback=initAutocomplete" async defer></script>
  </script>
@endpush
@endsection
