<div class="row">
  <div class="cus_container3">
    <div class="row flex_table">
      <div class="col-md-4 col-sm-4 cus_offset"></div>

      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-8">
        <a href="{{ url('/') }}">
          <img src="{{ asset('components/front/images/mockup/logo.jpg') }}" class="img-responsive img_width img_center" />
        </a>
      </div>

      <div class="col-md-offset-2 col-md-2 col-sm-offset-2 col-sm-2 col-xs-4 navbar-header">
        <button type="button" class="navbar-toggle align_center" data-toggle="collapse" data-target="#menus">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                     
        </button>
      </div>

      <div class="col-lg-10 col-md-9 col-sm-9 col-xs-12 collapse navbar-collapse align_right head_menu" id="menus">
        <ul class="nav navbar-nav">
          <li class="active"><a href="{{ url('/') }}">HOME</a></li>
          <li class="dropdown hov_dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">ABOUT US</a>
            <ul class="dropdown-menu dropdown-content cus_drop">
              <li><a href="{{ url('about-us/vision-mission') }}">Vision Mission</a></li>
              <li><a href="{{ url('about-us/company-strength') }}">Company Strength</a></li>
              <li><a href="{{ url('about-us/milestones') }}">Milestones</a></li>
            </ul>
          </li>
          <li class="dropdown hov_dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">CORPORATE GOVERNANCE</a>
            <ul class="dropdown-menu dropdown-content cus_drop">
              <li><a href="{{ url('corporate-governance/the-boards') }}">The Boards</a></li>
              <!-- <li><a href="{{ url('corporate-governance/board-directors') }}">Board Of Directors</a></li> -->
            </ul>
          </li>
          <li class="dropdown hov_dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">MINING ASSETS</a>
            <ul class="dropdown-menu dropdown-content cus_drop">
              <li><a href="{{ url('mining-assets/pt-bangun-olah-sarana') }}">PT Bangun Olah Sarana</a></li>
              <li><a href="{{ url('mining-assets/pt-pratama-bersama') }}">PT Pratama Bersama</a></li>
              <li><a href="{{ url('mining-assets/pt-energi-amzal-bersama') }}">PT Energi Amzal Bersama</a></li>
              <li><a href="{{ url('mining-assets/pt-pratama-buana-sentosa') }}">PT Pratama Buana Sentosa</a></li>
            </ul>
          </li>
          <li class="dropdown hov_dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">PROJECT DATA</a>
            <ul class="dropdown-menu dropdown-content cus_drop">
              <li><a href="{{ url('project-data/production-hauling') }}">Production & Hauling</a></li>
              <li><a href="{{ url('project-data/port-facility') }}">Port Facility</a></li>
              <li><a href="{{ url('project-data/guaranteed-specifications') }}">Guaranteed Specifications</a></li>
              <li><a href="{{ url('project-data/coal-exports') }}">Coal Exports</a></li>
              <li><a href="{{ url('project-data/documents') }}">Documents</a></li>
            </ul>
          </li>
          <!-- <li><a href="{{ url('investor-relation') }}">INVESTOR RELATION</a></li> -->
          <li><a href="{{ url('health-safety') }}">CSR</a></li>
          <li><a href="{{ url('news-release') }}">NEWS RELEASE</a></li>
          <li><a href="{{ url('contact-us') }}">CONTACT US</a></li>
          <li id="search_in"><a><i class="glyphicon glyphicon-search"></i></a></li>
            <div id="custom-search-input">
              <form action="{{ url('/search') }}" method="POST">
              {{ csrf_field() }}

              <div class="input-group col-md-12">
                  <input type="text" id="search_input" class="form-control" placeholder="Search" name="search" />
                  <span class="input-group-btn">
                      <button class="btn btn-danger" type="submit">
                          <span class=" glyphicon glyphicon-search"></span>
                      </button>
                  </span>
              </div>
              </form>
            </div>
        </ul>
      </div>
    </div>
  </div>
</div>