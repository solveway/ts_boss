<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12 foot_copy">
		<p>&copy; Copyright 2017 PT Borneo Olah Sarana Sukses</p>
	</div>
</div>

<script type="text/javascript" src="{{asset('components/plugins/jquery.min.js')}}"></script>
<script src="{{asset('components/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('components/plugins/swiper-slider/swiper.jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('components/front/js/global.js')}}"></script>
<script type="text/javascript" src="{{asset('components/front/js/jquery.mousewheel.js')}}"></script>
<script type="text/javascript" src="{{asset('components/front/js/jquery.jscrollpane.min.js')}}"></script>
<script src="{{asset('components/plugins/select2/js/select2.full.min.js')}}"></script>
<script src="{{asset('components/plugins/bootstrap-growl/jquery.bootstrap-growl.min.js')}}"></script>
<script type="text/javascript" src="{{asset('components/plugins/owl-carousel/owl.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('components/front/js/kyubi-head.js')}}"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyCsNthU_4z1icAgE19GuSQDIbwwqRRUE_A"></script>

<script>
$(document).ready(function() {
	$(document).on('click', '#search_in', function(){
		var windowWidth = $(window).width();
		if(windowWidth <= 991){
			$(this).css('display', 'none');
		}
		$('#custom-search-input').css('display', 'block');
		$('#search_input').focus();
	});

	$(document).mouseup(function(e) {
		var windowWidth = $(window).width();
	    var container = $("#custom-search-input");

	    if (!container.is(e.target) && container.has(e.target).length === 0) 
	    {
	        container.hide();
	        if(windowWidth <= 991){
				$('#search_in').css('display', 'block');
			}
	    }
	});
});
</script>
@stack('custom_scripts')
