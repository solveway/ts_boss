@extends($view_path.'.layouts.master')
@section('content')
<div class="row chp_con">
  <div class="row cus_con">
  	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  	  <div class="row">
  	  	<div class="col-lg-offset-3 col-lg-6 col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8 col-xs-12 chp_cons">
  	  	  <div class="row">
     		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 chp_text">
     		  <p>Change Password</p>
     		</div>

     		@if (count($errors) > 0)
			<div class="col-md-12 col-sm-12 col-xs-12">
			    <div class="alert alert-danger alert-dismissable">
			    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			</div>
			@endif

			@if(session()->has('message'))
		    <div class="col-md-12 col-sm-12 col-xs-12">
			    <div class="alert alert-success alert-dismissable">
			     	<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
			        {{ session()->get('message') }}
			    </div>
		    </div>
		  	@endif
			
			<div class="col-md-12 col-sm-12 col-xs-12">
				<form class="form-horizontal" role="form" method="POST" action="change-password">
				{{csrf_field()}}

				<div class="form-group">
					<label class="col-md-4 col-sm-5 control-label chp_input">Old Password :</label>
					<div class="col-md-6 col-sm-6 chp_input">
						<input type="password" class="form-control" name="old_password">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 col-sm-5 control-label chp_input">Password :</label>
					<div class="col-md-6 col-sm-6 chp_input">
						<input type="password" class="form-control" name="password">
					</div>
				</div>

				<div class="form-group chp_input">
					<label class="col-md-4 col-sm-5 control-label chp_input">Confirm Password :</label>
					<div class="col-md-6 col-sm-6 chp_input">
						<input type="password" class="form-control" name="password_confirmation">
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-6 col-md-offset-4 col-sm-6 col-sm-offset-5">
						<button type="submit" class="btn chp_submit">
							Submit
						</button>
					</div>
				</div>
				</form>
			</div>
     	  </div>
  	  	</div>
  	  </div>
  	</div>
  </div>
</div>
@endsection