@extends($view_path.'.layouts.master')
@section('content')
<div class="loader-custom">
	<span class="cssload-loader"><span class="cssload-loader-inner"></span></span>
</div>

<div class="row edp_con">
  <div class="row cus_con">
  	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  	  <div class="row">
  	  	<div class="col-lg-offset-3 col-lg-6 col-md-offset-2 col-md-8 col-sm-offset-1 col-sm-10 col-xs-12 edp_cons">
  	  	  <div class="row">
     		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 edp_text">
     		  <p>Edit Profile</p>
     		</div>

     		@if (count($errors) > 0)
			<div class="col-md-12 col-sm-12 col-xs-12">
			    <div class="alert alert-danger alert-dismissable">
			    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			</div>
			@endif

			@if(session()->has('message'))
		    <div class="col-md-12 col-sm-12 col-xs-12">
			    <div class="alert alert-success alert-dismissable">
			     	<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
			        {{ session()->get('message') }}
			    </div>
		    </div>
		  @endif

			<div class="col-md-12 col-sm-12 col-xs-12">
				<form class="form-horizontal" role="form" method="POST" action="edit-profile" enctype="multipart/form-data">
				{{csrf_field()}}

				<div class="form-group">
					<label class="col-lg-5 col-md-5 col-sm-5 edp_input">Nama <span class="tw_dot">:</span></label>
					<div class="col-lg-7 col-md-7 col-sm-7 edp_input">
						<input type="text" class="form-control" name="ep_name" value="{{ $profile->name ? $profile->name : '' }}"  placeholder="Nama Anda" />
					</div>
				</div>

				<div class="form-group">
					<label class="col-lg-5 col-md-5 col-sm-5 edp_input">Email <span class="tw_dot">:</span></label>
					<div class="col-lg-7 col-md-7 col-sm-7 edp_input">
						<input type="email" class="form-control" name="ep_email" value="{{ $profile->email ? $profile->email : '' }}"  placeholder="Email Anda" />
					</div>
				</div>

				@php
					$st_rel_count = 1;
					$st_rel = array('Belum Menikah', 'Menikah');

					if(in_array($profile->status_menikah, $st_rel)){
						$st_rel_count = 1;
					} else{
						$st_rel_count = 0;
					}
				@endphp

				<div class="form-group">
					<label class="col-lg-5 col-md-5 col-sm-5 edp_input">Status Menikah <span class="tw_dot">:</span></label>
					<div class="col-lg-7 col-md-7 col-sm-7 edp_input">
						<select class="form-control" name="ep_status" placeholder="Status Anda">
							<option value="">-- Select Status --</option>
							<option value="Belum Menikah" {{ $profile->status_menikah == "Belum Menikah" ? 'selected' : '' }}>Belum Menikah</option>
							<option value="Menikah" {{ $profile->status_menikah == "Menikah" ? 'selected' : '' }}>Menikah</option>
							<option value="other" {{ $st_rel_count == '0' ? 'selected' : '' }}>Lainnya</option>
						</select>
					</div>

					<div class="col-lg-offset-5 col-lg-7 col-md-offset-5 col-md-7 col-sm-offset-5 col-sm-7 col-xs-12 edp_other">
					@if($st_rel_count == 0)
						<input type="text" class="form-control" name="ep_status_other" placeholder="Status Lainnya" value="{{ $profile->status_menikah }}" />
					@else
						<input type="text" class="form-control" name="ep_status_other" placeholder="Status Lainnya" />
					@endif
						<label>Jika Status tidak ada, pilih Lainnya dan isi kotak diatas.</label>
					</div>
				</div>

				<div class="form-group">
					<label class="col-lg-5 col-md-5 col-sm-5 edp_input">Jenis Kelamin <span class="tw_dot">:</span></label>
					<div class="col-lg-7 col-md-7 col-sm-7 edp_input">
						<select class="form-control" name="ep_jenis_kelamin">
							<option value="">-- Pilih Jenis Kelamin --</option>
							<option value="Pria" {{ $profile->jenis_kelamin == "Pria" ? 'selected' : '' }}>Pria</option>
							<option value="Wanita" {{ $profile->jenis_kelamin == "Wanita" ? 'selected' : '' }}>Wanita</option>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="col-lg-5 col-md-5 col-sm-5 edp_input">Nomor Telepon <span class="tw_dot">:</span></label>
					<div class="col-lg-7 col-md-7 col-sm-7 edp_input">
						<input type="text" class="form-control" name="ep_phone" value="{{ $profile->telephone ? $profile->telephone : '' }}" placeholder="Nomor Telepon" />
					</div>
				</div>

				<div class="form-group">
					<label class="col-lg-5 col-md-5 col-sm-5 edp_input">Handphone <span class="tw_dot">:</span></label>
					<div class="col-lg-7 col-md-7 col-sm-7 edp_input">
						<input type="text" class="form-control" name="ep_handphone" value="{{ $profile->handphone ? $profile->handphone : '' }}" placeholder="Handphone" />
					</div>
				</div>

				@php
					$get_ttl = $profile->ttl;
					$ct_count = 0;
					if($get_ttl != NULL){
						$split = explode(",",$get_ttl);
						$tl = $split[0];
						$tgl = date_create($split[1]);
						$get_tgl = date_format($tgl,"Y-m-d");
					}else{
						$tl = "";
						$get_tgl = "";
					}
				@endphp

				<div class="form-group">
					<label class="col-lg-5 col-md-5 col-sm-5 edp_input">Tanggal Lahir <span class="tw_dot">:</span></label>
					<div class="col-lg-7 col-md-7 col-sm-7 edp_input">
						<input type="date" class="form-control" name="ep_date" value="{{ $get_tgl }}" />
					</div>
				</div>

				<div class="form-group">
					<label class="col-lg-5 col-md-5 col-sm-5 edp_input">Tempat Lahir <span class="tw_dot">:</span></label>
					<div class="col-lg-7 col-md-7 col-sm-5 col-xs-12">
						<select class="form-control edp_city" name="ep_tp_born">
							<option value=""></option>
					    	@foreach($ttl as $q => $ctl)
					    		<option value="{{ ucfirst(strtolower($ctl)) }}" {{ $ctl == $tl ? 'selected' : '' }}>{{ ucfirst(strtolower($ctl)) }}</option>

					    		@php
					    			if($ctl == $tl) {
					    				$ct_count = $ct_count+1;
					    			}
					    		@endphp
					    	@endforeach
							<option value="other" {{ $ct_count == '0' ? 'selected' : '' }}>Lainnya</option>
						</select>
					</div>

					<div class="col-lg-offset-5 col-lg-7 col-md-offset-5 col-md-7 col-sm-offset-5 col-sm-7 col-xs-12 edp_other">
					@if($ct_count == 0)
						<input type="text" class="form-control" name="ep_tp_other" placeholder="Kota Lainnya" value="{{ $tl }}" />
					@else
						<input type="text" class="form-control" name="ep_tp_other" placeholder="Kota Lainnya" />
					@endif
						<label>Jika kota tidak ada, pilih lainnya dan isi kotak diatas.</label>
					</div>
				</div>

				<div class="form-group">
					<label class="col-lg-5 col-md-5 col-sm-5 edp_input">Address <span class="tw_dot">:</span></label>
					<div class="col-lg-7 col-md-7 col-sm-7 edp_input">
						<textarea class="form-control" name="ep_address" rows="5">{{ $profile->address ? $profile->address : '' }}</textarea>
					</div>
				</div>

				<div class="form-group">
					<label class="col-lg-5 col-md-5 col-sm-5 edp_input">RT <span class="tw_dot">:</span></label>
					<div class="col-lg-7 col-md-7 col-sm-7 edp_input">
						<input type="text" class="form-control" name="ep_rt" value="{{ $profile->rt ? $profile->rt : '' }}" placeholder="Rt Anda" />
					</div>
				</div>

				<div class="form-group">
					<label class="col-lg-5 col-md-5 col-sm-5 edp_input">RW <span class="tw_dot">:</span></label>
					<div class="col-lg-7 col-md-7 col-sm-7 edp_input">
						<input type="text" class="form-control" name="ep_rw" value="{{ $profile->rw ? $profile->rw : '' }}" placeholder="Rw Anda" />
					</div>
				</div>

				<div class="form-group">
					<label class="col-lg-5 col-md-5 col-sm-5 edp_input">Kota <span class="tw_dot">:</span></label>
					<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
						<select class="form-control edp_city get-district" name="ep_kota" data-target="district">
							<option value=""></option>
					    	@foreach($city as $q => $ctc)
					    		<option value="{{ $q }}" {{ $q == $profile->kota ? 'selected' : '' }}>{{ ucfirst(strtolower($ctc)) }}</option>
					    	@endforeach
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="col-lg-5 col-md-5 col-sm-5 edp_input">Kecamatan <span class="tw_dot">:</span></label>
					<div class="col-lg-7 col-md-7 col-sm-7 edp_input">
						<select class="form-control district" name="ep_kecamatan" data-target="subdistrict">
							<option value="">-- Pilih Kecamatan --</option>
							@if($kecamatan != "")
							<option value="{{ $kecamatan->id }}" selected>{{ $kecamatan->name }}</option>
							@endif
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="col-lg-5 col-md-5 col-sm-5 edp_input">Kelurahan <span class="tw_dot">:</span></label>
					<div class="col-lg-7 col-md-7 col-sm-7 edp_input">
						<select class="form-control subdistrict" name="ep_kelurahan" data-target="postcode">
							<option value="">-- Pilih Kelurahan --</option>
							@if($kelurahan != "")
							<option value="{{ $kelurahan->id }}" selected>{{ $kelurahan->name }}</option>
							@endif
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="col-lg-5 col-md-5 col-sm-5 edp_input">Kodepos <span class="tw_dot">:</span></label>
					<div class="col-lg-7 col-md-7 col-sm-7 edp_input">
						<select class="form-control postcode" name="ep_kodepos">
							<option value="">-- Pilih Kodepos --</option>
							@if($kodepos != "")
							<option value="{{ $kodepos->id }}" selected>{{ $kodepos->postcode }}</option>
							@endif
						</select>
					</div>
				</div>

				@php
					$pr_rel_count = 1;
					$pr_rel = array('Pegawai Negeri', 'Pegawai Swasta', 'Ojek', 'Wiraswasta/Pedagang', 'Mahasiswa/Pelajar', 'Guru/Dosen', 'TNI/POLRI', 'Ibu Rumah Tangga', 'Petani/Nelayan','Proffessional(Dokter/Pengacara dll');

					if(in_array($profile->pekerjaan, $pr_rel)){
						$pr_rel_count = 1;
					} else{
						$pr_rel_count = 0;
					}
				@endphp

				<div class="form-group">
					<label class="col-lg-5 col-md-5 col-sm-5 edp_input">Pekerjaan <span class="tw_dot">:</span></label>
					<div class="col-lg-7 col-md-7 col-sm-7 edp_input">
						<select class="form-control" name="ep_pekerjaan">
							<option value="">-- Pilih Pekerjaan --</option>
							<option value="Pegawai Negeri" {{ $profile->pekerjaan == "Pegawai Negeri" ? 'selected' : '' }}>Peg. Negeri</option>
							<option value="Pegawai Swasta" {{ $profile->pekerjaan == "Pegawai Swasta" ? 'selected' : '' }}>Peg. Swasta</option>
							<option value="Ojek" {{ $profile->pekerjaan == "Ojek" ? 'selected' : '' }}>Ojek</option>
							<option value="Wiraswasta/Pedagang" {{ $profile->pekerjaan == "Wiraswasta/Pedagang" ? 'selected' : '' }}>Wiraswasta/Pedagang</option>
							<option value="Mahasiswa/Pelajar" {{ $profile->pekerjaan == "Mahasiswa/Pelajar" ? 'selected' : '' }}>Mahasiswa/Pelajar</option>
							<option value="Guru/Dosen" {{ $profile->pekerjaan == "Guru/Dosen" ? 'selected' : '' }}>Guru/Dosen</option>
							<option value="TNI/POLRI" {{ $profile->pekerjaan == "TNI/POLRI" ? 'selected' : '' }}>TNI/POLRI</option>
							<option value="Ibu Rumah Tangga" {{ $profile->pekerjaan == "Ibu Rumah Tangga" ? 'selected' : '' }}>Ibu Rumah Tangga</option>
							<option value="Petani/Nelayan" {{ $profile->pekerjaan == "Petani/Nelayan" ? 'selected' : '' }}>Petani/Nelayan</option>
							<option value="Professional(Dokter/Pengacara dll)" {{ $profile->pekerjaan == "Professional(Dokter/Pengacara dll)" ? 'selected' : '' }}>Professional(Dokter/Pengacara dll)</option>
							<option value="other" {{ $pr_rel_count == 0 ? 'selected' : ''}}>Lainnya</option>
						</select>
					</div>

					<div class="col-lg-offset-5 col-lg-7 col-md-offset-5 col-md-7 col-sm-offset-5 col-sm-7 col-xs-12 edp_other">
					@if($pr_rel_count == 0)
						<input type="text" class="form-control" name="ep_pekerjaan_other" placeholder="Pekerjaan Lainnya" value="{{ $profile->pekerjaan }}" />
					@else
						<input type="text" class="form-control" name="ep_pekerjaan_other" placeholder="Pekerjaan Lainnya" />
					@endif
						<label>Jika Pekerjaan tidak ada, pilih Lainnya dan isi kotak diatas.</label>
					</div>
				</div>

				@php
					$ag_rel_count = 1;
					$ag_rel = array('Islam', 'Kristen', 'Katolik', 'Hindu', 'Buddha');

					if(in_array($profile->agama, $ag_rel)){
						$ag_rel_count = 1;
					} else{
						$ag_rel_count = 0;
					}
				@endphp

				<div class="form-group">
					<label class="col-lg-5 col-md-5 col-sm-5 edp_input">Agama <span class="tw_dot">:</span></label>
					<div class="col-lg-7 col-md-7 col-sm-7 edp_input">
						<select class="form-control" name="ep_agama">
							<option value="">-- Pilih Agama --</option>
							<option value="Islam" {{ $profile->agama == "Islam" ? 'selected' : '' }}>Islam</option>
							<option value="Kristen" {{ $profile->agama == "Kristen" ? 'selected' : '' }}>Kristen</option>
							<option value="Katolik" {{ $profile->agama == "Katolik" ? 'selected' : '' }}>Katolik</option>
							<option value="Hindu" {{ $profile->agama == "Hindu" ? 'selected' : '' }}>Hindu</option>
							<option value="Buddha" {{ $profile->agama == "Buddha" ? 'selected' : '' }}>Buddha</option>
							<option value="other" {{ $ag_rel_count == 0 ? 'selected' : '' }}>Lainnya</option>
						</select>
					</div>

					<div class="col-lg-offset-5 col-lg-7 col-md-offset-5 col-md-7 col-sm-offset-5 col-sm-7 col-xs-12 edp_other">
					@if($ag_rel_count == 0)
						<input type="text" class="form-control" name="ep_agama_other" placeholder="Agama Lainnya" value="{{ $profile->agama }}" />
					@else
						<input type="text" class="form-control" name="ep_agama_other" placeholder="Agama Lainnya" />
					@endif
						<label>Jika Agama tidak ada, pilih Lainnya dan isi kotak diatas.</label>
					</div>
				</div>

				<div class="form-group">
					<label class="col-lg-5 col-md-5 col-sm-5 edp_input">Pendidikan <span class="tw_dot">:</span></label>
					<div class="col-lg-7 col-md-7 col-sm-7 edp_input">
						<select class="form-control" name="ep_pendidikan">
							<option value="">-- Pilih Pendidikan --</option>
							<option value="Tidak Tamat SD" {{ $profile->pendidikan == "Tidak Tamat SD" ? 'selected' : '' }}>Tidak Tamat SD</option>
							<option value="SD" {{ $profile->pendidikan == "SD" ? 'selected' : '' }}>SD</option>
							<option value="SLTP/SMP" {{ $profile->pendidikan == "SLTP/SMP" ? 'selected' : '' }}>SLTP/SMP</option>
							<option value="SLTP/SMA" {{ $profile->pendidikan == "SLTP/SMA" ? 'selected' : '' }}>SLTP/SMA</option>
							<option value="Akademi/D1/D2/D3" {{ $profile->pendidikan == "Akademi/D1/D2/D3" ? 'selected' : '' }}>Akademi/D1/D2/D3</option>
							<option value="Sarjana/S1" {{ $profile->pendidikan == "Sarjana/S1" ? 'selected' : '' }}>Sarjana/S1</option>
							<option value="Pasca Sarjana/S2/S3" {{ $profile->pendidikan == "Pasca Sarjana/S2/S3" ? 'selected' : '' }}>Pasca Sarjana/S2/S3</option>
						</select>
					</div>
				</div>

				@php
					$tj_rel_count = 1;
					$tj_rel = array('Berdagang', 'Jarak Dekat', 'Ke Sekolah/Kampus', 'Rekreasi/Olahraga', 'Kebutuhan Keluarga', 'Bekerja');

					if(in_array($profile->tujuan, $tj_rel)){
						$tj_rel_count = 1;
					} else{
						$tj_rel_count = 0;
					}
				@endphp

				<div class="form-group">
					<label class="col-lg-5 col-md-5 col-sm-5 edp_input">Tujuan Pakai <span class="tw_dot">:</span></label>
					<div class="col-lg-7 col-md-7 col-sm-7 edp_input">
						<select class="form-control" name="ep_tujuan">
							<option value="">-- Pilih Tujuan Pakai --</option>
							<option value="Berdagang" {{ $profile->tujuan == "Berdagang" ? 'selected' : '' }}>Berdagang</option>
							<option value="Jarak Dekat" {{ $profile->tujuan == "Jarak Dekat" ? 'selected' : '' }}>Jarak Dekat</option>
							<option value="Ke Sekolah/Kampus" {{ $profile->tujuan == "Ke Sekolah/Kampus" ? 'selected' : '' }}>Ke Sekolah/Kampus</option>
							<option value="Rekreasi/Olahraga" {{ $profile->tujuan == "Rekreasi/Olahraga" ? 'selected' : '' }}>Rekreasi/Olahraga</option>
							<option value="Kebutuhan Keluarga" {{ $profile->tujuan == "Kebutuhan Keluarga" ? 'selected' : '' }}>Kebutuhan Keluarga</option>
							<option value="Bekerja" {{ $profile->tujuan == "Bekerja" ? 'selected' : '' }}>Bekerja</option>
							<option value="other" {{ $tj_rel_count == 0 ? 'Selected' : '' }}>Lainnya</option>
						</select>
					</div>

					<div class="col-lg-offset-5 col-lg-7 col-md-offset-5 col-md-7 col-sm-offset-5 col-sm-7 col-xs-12 edp_other">
					@if($tj_rel_count == 0)
						<input type="text" class="form-control" name="ep_tujuan_other" placeholder="Tujuan Pakai Lainnya" value="{{ $profile->tujuan }}" />
					@else
						<input type="text" class="form-control" name="ep_tujuan_other" placeholder="Tujuan Pakai Lainnya" />
					@endif
						<label>Jika Pekerjaan tidak ada, pilih Lainnya dan isi kotak diatas.</label>
					</div>
				</div>

				<div class="form-group">
					<label class="col-lg-5 col-md-5 col-sm-5 edp_input">Pengeluaran Perbulan <span class="tw_dot">:</span></label>
					<div class="col-lg-7 col-md-7 col-sm-7 edp_input">
						<select class="form-control" name="ep_pengeluaran">
							<option value="">-- Pengeluaran Perbulan --</option>
							@foreach($pengeluaran as $q => $pg)
								<option value="{{ $q }}" {{ $profile->pengeluaran == $q ? 'selected' : '' }}> {{ $pg }}</option>
							@endforeach
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="col-lg-5 col-md-5 col-sm-5 edp_input">Hobby <span class="tw_dot">:</span></label>
					<div class="col-lg-7 col-md-7 col-sm-7 edp_input">
						<input type="text" class="form-control" name="ep_hobby" value="{{ $profile->hobby ? $profile->hobby : '' }}" placeholder="Hobby" />
					</div>
				</div>

				<div class="form-group">
					<label class="col-lg-5 col-md-5 col-sm-5 edp_input">Upload Picture <span class="tw_dot">:</span></label>

					<div class="col-lg-6 col-md-6 col-sm-5">
					  <div class="row">
					    <div class="col-md-12 col-sm-12 col-xs-12 single-image-photo">
					    	@if($profile->photo != NULL)
				              <img src="{{ asset('components/admin/image/customer') }}/{{ $profile->photo }}" class="img-responsive img_center" width="100px" height="100px" />
				            @else
				              <img src="{{ asset('components/both/images/web/none.png') }}" class="img-responsive img_center" width="100px" height="100px" />
				            @endif
              			</div>

              			<div class="col-md-12 col-sm-12 col-xs-12 pf_cus_up">
              			  <div class="row flex_table">
              			    <div class="col-md-6 col-sm-7 col-xs-offset-2 col-xs-8 pf_cus_upl">
			                  <input type="file" class="form-control col-md-12 single-image" name="photo"> Upload Picture
			              	</div>

			              	<div class="col-md-4 col-sm-4 col-xs-12">
			              	  <button type="button" class="pf_btn btn remove-single-image" data-id="single-image" data-name="photo">
			                  	<i class="fa fa-trash-o fa-3x pf_fa" aria-hidden="true"></i>
			                  </button>
			              	</div>

		              		<input type="hidden" name="remove-single-image-photo" value="n">
              			  </div>
              			</div>
              		  </div>
              		</div>
				</div>

				<div class="form-group">
					<div class="col-lg-offset-5 col-lg-7 col-md-offset-5 col-md-7 col-sm-offset-5 col-sm-7">
						<button type="submit" class="btn edp_submit">
							Submit
						</button>
					</div>
				</div>
				</form>

				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 edp_text pf_card">
	     		  <p>Tambah Nomor Kartu</p>
	     		</div>

				<div class="form-group">
					<div class="col-md-12 col-sm-12 col-xs-12"> <!-- Success Message -->
		                <div class="st_success"></div>
		            </div> <!-- Success Message -->
					
					<label class="col-lg-5 col-md-5 col-sm-5 edp_input">Nomor Kartu <span class="tw_dot">:</span></label>
					<div class="col-lg-7 col-md-7 col-sm-7 edp_input">
						<input type="text" class="form-control" id="ep_card" placeholder="Nomor Kartu" />
					</div>
				</div>

				<div class="form-group">
					<div class="col-lg-offset-5 col-lg-7 col-md-offset-5 col-md-7 col-sm-offset-5 col-sm-7">
						<button type="button" class="btn edp_submit ep_crd_bt">
							Add Card
						</button>
					</div>
				</div>

				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 crd_data"></div>
			</div>
     	  </div>
  	  	</div>
  	  </div>
  	</div>
  </div>
</div>
@endsection


@push('custom_scripts')
<script>
	$('#ep_card').keyup(function() {
	    var ids = this.value.split(" ").join("");
	    if (ids.length > 0) {
	        ids = ids.match(new RegExp('.{1,4}', 'g')).join(" ");
	    }
	    this.value = ids;
	});
</script>
@endpush