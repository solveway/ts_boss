@extends($view_path.'.layouts.master')
@section('content')
<div class="row rg_con" style="background-image:url('{{ asset('components/admin/image/banner') }}/{{ $banner->image }}');">
  <div class="row cus_con flex_table rg">
  	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
  	  <div class="row">
  	    <div class="col-md-11 col-sm-11 col-xs-12 rg_l_1">
  	      {!! $banner->description !!}
  	    </div>
  	  </div>
  	</div>

  	<div class="col-lg-offset-1 col-lg-5 col-md-6 col-sm-12 col-xs-12">
  	  <div class="row">
  	  	<div class="col-md-offset-2 col-md-10 col-sm-offset-2 col-sm-8 col-xs-12 ">
  	  	  <div class="row">
            @if(session()->has('fail'))
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="alert alert-danger alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                  {{ session()->get('fail') }}
              </div>
            </div>
            @endif

  	  	  	<div class="col-md-12 col-sm-12 col-xs-12 rg_r_1">
  	  	  	  <h3>Register</h3>
  	  	  	</div>

            <form action="register" method="POST">
            {{ csrf_field() }}

  	  	  	<div class="col-md-12 col-sm-12 col-xs-12 rg_r_2">
  	  	  	  <div class="row">
                @if(session()->has('message'))
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="alert alert-success alert-dismissable">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        {{ session()->get('message') }}
                    </div>
                  </div>
                @endif

                @if (count($errors) > 0)
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="alert alert-danger alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            
                    <ul>
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                    </div>
                  </div>
                @endif
                
                <div class="col-md-12 col-sm-12 col-xs-12 rg_r_input">
                  <div class="row">
                    <div class="col-lg-5 col-md-4 col-sm-4 col-xs-12 rg_r_label">
                      <label>Nama Sesuai KTP</label>
                      <label>(tanpa singkatan)</label>
                    </div>

                    <div class="col-md-7 col-sm-8 col-xs-12">
                      <input type="text" class="form-control" name="rg_name" />
                    </div>
                  </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 rg_r_input">
                  <div class="row">
                    <div class="col-lg-5 col-md-4 col-sm-4 col-xs-12 rg_r_label">
                      <label>Email</label>
                    </div>

                    <div class="col-md-7 col-sm-8 col-xs-12">
                      <input type="email" class="form-control" name="rg_email" />
                    </div>
                  </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 rg_r_input">
                  <div class="row">
                    <div class="col-lg-5 col-md-4 col-sm-4 col-xs-12 rg_r_label">
                      <label>Phone</label>
                    </div>

                    <div class="col-md-7 col-sm-8 col-xs-12">
                      <input type="text" class="form-control" name="rg_phone" />
                    </div>
                  </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 rg_r_input">
                  <div class="row">
                    <div class="col-lg-5 col-md-4 col-sm-4 col-xs-12 rg_r_label">
                      <label>Password</label>
                    </div>

                    <div class="col-md-7 col-sm-8 col-xs-12">
                      <input type="password" class="form-control" name="rg_password" />
                    </div>
                  </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 rg_r_button_i">
                  <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                      <div class="g-recaptcha rg_cus_recaptcha" data-sitekey="6LfwCR4UAAAAANHaL6ozBfgPZYsG-2eFdJ-BLCyG"></div>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
                      <button type="submit" class="rg_r_button btn btn-default">Sign Up</button>
                    </div>
                  </div>
                </div>

                <!-- <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rg_r_2_link">
                      <p>Verification Your Account ? <a href="{{ url('/verif-code') }}">Verification</a></p>
                    </div>
                  </div>
                </div> -->
              </div>
  	  	  	</div>
            </form>
  	  	  </div>
  	  	</div>
  	  </div>
  	</div>
  </div>
</div>
@endsection

@push('custom_scripts')
  <script src="https://www.google.com/recaptcha/api.js" async defer></script>
@endpush