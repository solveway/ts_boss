@extends($view_path.'.layouts.master')
@section('content')
<div class="loader-custom">
	<div id="floatingCirclesG">
		<div class="f_circleG" id="frotateG_01"></div>
		<div class="f_circleG" id="frotateG_02"></div>
		<div class="f_circleG" id="frotateG_03"></div>
		<div class="f_circleG" id="frotateG_04"></div>
		<div class="f_circleG" id="frotateG_05"></div>
		<div class="f_circleG" id="frotateG_06"></div>
		<div class="f_circleG" id="frotateG_07"></div>
		<div class="f_circleG" id="frotateG_08"></div>
	</div>
</div>

@if(session()->has('th_message'))
<input type="hidden" id="th_modal" />

<div class="modal fade" id="thModal" role="dialog">
	<div class="modal-dialog">
	  <!-- Modal content-->
	  <div class="modal-content verf_mo_con">
	    <div class="modal-header verf_mo_head">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Notification</h4>
        </div>
	    <div class="modal-body verf_mo_bod">
	      <p>Terima Kasih sudah melakukan Registrasi, Silahkan Cek Email atau sms untuk mendapatkan kode verifikasi.</p>
	    </div>
	  </div>
	</div>
</div>
@endif

<div class="row vc_con">
  <div class="row cus_con">
  	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  	  <div class="row">
  	  	<div class="col-lg-offset-4 col-lg-4 col-md-offset-3 col-md-6 col-sm-offset-2 col-sm-8 col-xs-12 vc_cons">
  	  	  <div class="row">
  	  	    <div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-sm-offset-1 col-sm-10 col-xs-12">
  	  	      <div class="row">

				@if(session()->has('fail'))
			    <div class="col-md-12 col-sm-12 col-xs-12">
				    <div class="alert alert-danger alert-dismissable">
				     	<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
				        {{ session()->get('fail') }}
				    </div>
			    </div>
			  	@endif

			  	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <!-- Success Message -->
	                <div class="st_success"></div>
		        </div> <!-- Success Message -->

	     		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 vc_text">
	     		  <p>Silahkan masukan kode verifikasi</p>
	     		</div>

	     		<form action="verification" method="POST">
	     		{{ csrf_field() }}
	     		
	     		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 vc_input">
	     		  <div class="row">
	     		  	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
	     		  	  <input type="text" maxlength=1 id="vc_1" name="code_1" onkeyup="moveOnMax(this,'vc_2')" required />
	     		  	</div>
				  
				  	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
	     		  	  <input type="text" maxlength=1 id="vc_2" name="code_2" onkeyup="moveOnMax(this,'vc_3')" required />
	     		  	</div>	

	     		  	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
	     		  	  <input type="text" maxlength=1 id="vc_3" name="code_3" onkeyup="moveOnMax(this,'vc_4')" required />
	     		  	</div>

	     		  	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
	     		  	  <input type="text" maxlength=1 id="vc_4" name="code_4" required />
	     		  	</div>
				  </div>
	     		</div>
	     		
	     		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	     	  	  <div class="row">
	     	  	  	<div class="col-lg-offset-5 col-lg-7 col-md-offset-4 col-md-8 col-sm-offset-6 col-sm-6 col-xs-offset-2 col-xs-8">
	     	  	  	  <div class="row">
	     	  	  	    <button type="submit" class="btn btn-default vc_submit">Verifikasi</button>
	     	  	  	  </div>
	     	  	  	</div>
	     	  	  </div>
	     	  	</div>
	     		</form>

	     		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	     	  	  <div class="row">
	     	  	  	<div class="col-lg-offset-5 col-lg-7 col-md-offset-4 col-md-8 col-sm-offset-6 col-sm-6 col-xs-offset-2 col-xs-8">
	     	  	  	  <div class="row">
	     	  	  	    <input type="hidden" value="{{ $em_dt }}" id="em_dta" />
	     	  	  	    <button class="btn btn-default vc_submit vrf_sent">Kirim Ulang Verifikasi</button>
	     	  	  	  </div>
	     	  	  	</div>
	     	  	  </div>
	     	  	</div>
	     	  </div>
     		</div>
  	  	  </div>
  	  	</div>
  	  </div>
  	</div>
  </div>
</div>
@endsection

@push('custom_scripts')
  <script>
  	moveOnMax = function (field, nextFieldID) {
	    if (field.value.length == 1) {
	        document.getElementById(nextFieldID).focus();
	    }
	}

	$(document).ready(function(){
		if($('#th_modal').length > 0){
			$("#thModal").modal("show");
		}
	});
  </script>
@endpush