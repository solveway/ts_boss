@extends($view_path.'.layouts.master')
@section('content')
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="row">
			<div class="swiper-container swiper-home">
		        <div class="swiper-wrapper">
					<div class="swiper-slide">
			    		<img src="{{ asset('components/front/images/mockup/slideshow.jpg') }}" class="img-responsive cus_size" />

			    		<div class="slide_back">
				    		<div class="slide_desc">
				    			<div class="slide_desc_tl">
				    				<h1>GROWING BEYOND EXPECTATION</h1>
				    				<!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
				    				<p>sed do eiusmod tempor incididunt 1</p> -->

				    				<!-- <a href="{{ url('/') }}"><button class="btn btn-default">FIND OUT MORE</button></a> -->
				    			</div>
				    		</div>
				    	</div>
			    	</div>

			    	<div class="swiper-slide">
			    		<img src="{{ asset('components/front/images/mockup/slideshow1.jpg') }}" class="img-responsive cus_size" />

			    		<div class="slide_back">
				    		<div class="slide_desc">
				    			<div class="slide_desc_tl">
				    				<h1>GROWING BEYOND EXPECTATION</h1>
				    				<!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
				    				<p>sed do eiusmod tempor incididunt 2</p> -->

				    				<!-- <a href="{{ url('/') }}"><button class="btn btn-default">FIND OUT MORE</button></a> -->
				    			</div>
				    		</div>
				    	</div>
			    	</div>

			    	<div class="swiper-slide">
			    		<img src="{{ asset('components/front/images/mockup/slideshow2.jpg') }}" class="img-responsive cus_size" />

			    		<div class="slide_back">
				    		<div class="slide_desc">
				    			<div class="slide_desc_tl">
				    				<h1>GROWING BEYOND EXPECTATION</h1>
				    				<!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
				    				<p>sed do eiusmod tempor incididunt 3</p> -->

				    				<!-- <a href="{{ url('/') }}"><button class="btn btn-default">FIND OUT MORE</button></a> -->
				    			</div>
				    		</div>
				    	</div>
			    	</div>
			    </div>

			    <div class="cus_slide_con">
			    	<div class="cus_slide_cons">
			    		<div class="cus_slide_con1">
			    			<div class="cus_slide_con1_con">
							    <div class="swiper-pagination cus_pagination"></div>

								<button class="hm_button-prev">
									<img src="{{ asset('components/front/images/mockup/left_control.jpg') }}" class="img-responsive" />
								</button>
								
								<button class="hm_button-next">
									<img src="{{ asset('components/front/images/mockup/right_control.jpg') }}" class="img-responsive" />
								</button>
				    		</div>
			    		</div>
		    		</div>
	    		</div>
			</div>
		</div>
	</div>
</div>

<div class="row hm_cons1">
	<div class="hm_abs cus_container3">
		<div class="row hm_con1">
			<div class="col-md-12 col-sm-12 col-xs-12 hm_con1_con">
				<div class="row flex_table">
					<div class="col-md-6 col-sm-6 col-xs-12 hm_con1_cus_col">
						<div class="hm_con1_img">
							<img src="{{ asset('components/front/images/mockup/hm_con1.jpg') }}" class="img-responsive img_width img_center" />
						</div>
					</div>

					<div class="col-md-6 col-sm-6 col-xs-12 hm_con1_r">
						<div class="row hm_con1_pad">
							<h3>Premium Coal Quality</h3>

							<p>BOSS and PB have high quality coal with high CV (6,400 GAR Kcal/kg) and low levels of sulfur (0.3-0.4%) and ash (less than 5%). The quality of our coal compares favorably to other Indonesian producers. The typical specifications of BOSS :</p>
						</div>

						<div class="row hm_con2_1_pad">
							<div class="hm_con1_2_lm2">
								<a href="{{ url('/project-data/documents') }}">BOSS COA <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 hm_con1_con">
				<div class="row flex_table">
					<div class="col-md-6 col-sm-6 col-xs-12 hm_con1_cus_col">
						<div class="hm_con1_img">
							<img src="{{ asset('components/front/images/mockup/hm_con2.jpg') }}" class="img-responsive img_width img_center" />
						</div>
					</div>

					<div class="col-md-6 col-sm-6 col-xs-12 hm_con1_2_r">
						<div class="row hm_con1_2_pad">
							<h3>Unique positioning in the market</h3>

							<p>BOSS operates and supply coal to premium customers. With its High Grade coal quality (high CV, low Sulphur and low Ash) , the company has become one of the very few Indonesian coal companies that is able to penetrate the Japanese Market. BOSS coal currently commands a premium over Australian Newcastle Coal pricing, as it offers significantly lower ash than its Australian alternative.</p>
						</div>

						<div class="row hm_con2_1_pad">
							<div class="hm_con1_2_lm">
								<a href="{{ url('/project-data/vision-mission') }}">Learn more <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
     
<div class="row hm_con2" style="background-image:url('{{ asset('components/front/images/mockup/hm_con3.jpg') }}');">
	<div class="col-md-12 col-sm-12 col-xs-12 hm_cons2">
		<h3>BOSS is ideally positioned to fulfill the ever growing</h3>
		<h3>energy needs of our customers for the foreseeable future</h3>
		<a href="{{ url('/#') }}"><button class="btn btn-default">WATCH OUR VIDEO</button></a>
	</div>
</div>

@if($list_news != "")
	@php
		$count = count($list_news);
		
		if($count > 0 && $count <= 4){
			$cus_class = "cus_navigation";
		} else{
			$cus_class = "";
		}
	@endphp
<div class="row hm_con3">
	<div class="cus_container2">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 hm_con3_1">
						<p>News Update</p>
					</div>
				</div>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="row flex_table owl-carousel owl-text owl-theme hm_carousel {{ $cus_class }}">
					@foreach($list_news as $q => $l_news)
					<div class="col-md-12 item hm_item">
						<div class="row">
							<div class="col-md-10 col-sm-10 col-xs-offset-1 col-xs-10 csr_cus_pad">
								<form method="POST" action="{{ url('/news-release') }}" id="news_{{ $q }}">
									{{ csrf_field() }}

									<input type="hidden" name="ids" value="{{ $l_news->id }}" />

									<img class="nws_img" src="{{ asset('components/admin/image/news') }}/{{ $l_news->image }}" class="img-responsive" />
									<p class="hm_item_tl" data-id="{{ $q }}">{{ $l_news->title }}</p>
									<div class="hm_item_des">{!! $l_news->description !!}</div>
								</form>
							</div>
						</div>
					</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</div>
@endif

<div class="row">
	
</div>
@endsection

@push('custom_scripts')
<script>
$( document ).ready(function() {
	var owl_cus;

    var swiper = new Swiper('.swiper-home', {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        effect: 'fade',
        autoplay: 2500,
        autoplayDisableOnInteraction: true,
    });

    $(".hm_button-prev").click(function(e){
	  e.preventDefault();
	  swiper.slidePrev(1500, true);
	});

	$(".hm_button-next").click(function(e){
	  e.preventDefault();
	  swiper.slideNext(1500, true);
	});

    owl_cus = $('.owl-text').owlCarousel({
	    margin:1,
	    nav:true,
	    dots:false,
	    loop:false,
	    autoplay:true,
	    autoplayTimeout:3000,
	    autoplayHoverPause:true,
	     navText : ["<img src='{{ url('components/front/images/mockup/arrow-left.png') }}' class='img-responsive img_center' />","<img src='{{ url('components/front/images/mockup/arrow-right.png') }}' class='img-responsive img_center' />"],
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:2
	        },
	        1024:{
	            items:3
	        },
	        1439:{
	        	items:4
	        }
	    }
	});

	callback();

    owl_cus.on('translated.owl.carousel', function(e) {
		callback();
	});

	owl_cus.on('dragged.owl.carousel', function(e) {
		callback();
	});

	function callback(event) {
		$('.owl-item.active > .hm_item').css('border-right', '2px solid #999999');
		$('.owl-item.active:last > .hm_item').css('border', '0');
	}

	$(document).on('click', '.hm_item_tl', function(){
		var id = $(this).data('id');
		$('#news_'+id).submit();
	});
});
</script>
@endpush