@extends($view_path.'.layouts.master')
@section('content')
<div class="row page_head" style="background-image:url('{{ url('components/front/images/mockup/pratama.jpg') }}');">
	<div class="page_head_bg"></div>
	
	<div class="cus_container3">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 page_head_tl">
				<p>MINING ASSETS</p>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 page_head_des">
				<h3>PT PRATAMA BERSAMA</h3>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="cus_container5">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="row flex_table pra_cons1">
					<div class="col-md-6 col-sm-12 col-xs-12">
						<img src="{{ asset('components/front/images/mockup/pt_pratama.jpg') }}" class="img-responsive img_width" />
					</div>

					<div class="col-md-6 col-sm-12 col-xs-12 pra_cons1_mg">
						<div class="row align_center">
						    <div class="col-md-12 col-sm-12 col-xs-12 pra_con1">
						     	<div class="row">
									<div class="col-lg-4 col-md-5 col-sm-4 col-xs-12">
										<p class="pra_con1_tl">Legal Name <span class="dot_r">:</span></p>
									</div>

									<div class="col-lg-8 col-md-7 col-sm-8 col-xs-12">
										<p class="pra_con1_des">PT. Pratama Bersama (PB) established in 2007</p>
									</div>
								</div>
						    </div>

						    <div class="col-md-12 col-sm-12 col-xs-12 pra_con1">
						     	<div class="row">
									<div class="col-lg-4 col-md-5 col-sm-4 col-xs-12">
										<p class="pra_con1_tl">Location <span class="dot_r">:</span></p>
									</div>

									<div class="col-lg-8 col-md-7 col-sm-8 col-xs-12">
										<p class="pra_con1_des">Muara Pahu, Kutai Barat, East Kalimantan</p>
									</div>
								</div>
						    </div>

						    <div class="col-md-12 col-sm-12 col-xs-12 pra_con1">
						     	<div class="row">
									<div class="col-lg-4 col-md-5 col-sm-4 col-xs-12">
										<p class="pra_con1_tl">Minning Area <span class="dot_r">:</span></p>
									</div>

									<div class="col-lg-8 col-md-7 col-sm-8 col-xs-12">
										<p class="pra_con1_des">4,210 Hectares</p>
									</div>
								</div>
						    </div>

						    <div class="col-md-12 col-sm-12 col-xs-12 pra_con1">
						     	<div class="row">
									<div class="col-lg-4 col-md-5 col-sm-4 col-xs-12">
										<p class="pra_con1_tl">Licensin <span class="dot_r">:</span></p>
									</div>

									<div class="col-lg-8 col-md-7 col-sm-8 col-xs-12">
										<p class="pra_con1_des">IUP Production</p>
									</div>
								</div>
						    </div>

						    <div class="col-md-12 col-sm-12 col-xs-12 pra_con1">
						     	<div class="row">
									<div class="col-lg-4 col-md-5 col-sm-4 col-xs-12">
										<p class="pra_con1_tl">Forestry Status <span class="dot_r">:</span></p>
									</div>

									<div class="col-lg-8 col-md-7 col-sm-8 col-xs-12">
										<p class="pra_con1_des">Non Forestry Area (APL)</p>
									</div>
								</div>
						    </div>

						    <div class="col-md-12 col-sm-12 col-xs-12 pra_con1">
						     	<div class="row">
									<div class="col-lg-4 col-md-5 col-sm-4 col-xs-12">
										<p class="pra_con1_tl">Expolration Activites <span class="dot_r">:</span></p>
									</div>

									<div class="col-lg-8 col-md-7 col-sm-8 col-xs-12">
										<p class="pra_con1_des">On progress exploration drilling activities for PT. Pratama Bersama</p>
									</div>
								</div>
						    </div>

						    <div class="col-md-12 col-sm-12 col-xs-12 pra_con1">
						     	<div class="row">
									<div class="col-lg-4 col-md-5 col-sm-4 col-xs-12">
										<p class="pra_con1_tl">JORC Consultant <span class="dot_r">:</span></p>
									</div>

									<div class="col-lg-8 col-md-7 col-sm-8 col-xs-12">
										<p class="pra_con1_des">PT. Runge Pincock Minarco</p>
									</div>
								</div>
						    </div>

						    <div class="col-md-12 col-sm-12 col-xs-12 pra_con1">
						     	<div class="row">
									<div class="col-lg-4 col-md-5 col-sm-4 col-xs-12">
										<p class="pra_con1_tl">Quality of coal <span class="dot_r">:</span></p>
									</div>

									<div class="col-lg-8 col-md-7 col-sm-8 col-xs-12">
										<p class="pra_con1_des">High CV Coal (GAR 6400 Kcal/Kg) with low sulphur and low ash.</p>
									</div>
								</div>
						    </div>

						    <div class="col-md-12 col-sm-12 col-xs-12 pra_con1">
						     	<div class="row">
									<div class="col-lg-4 col-md-5 col-sm-4 col-xs-12">
										<p class="pra_con1_tl">Production Capacity <span class="dot_r">:</span></p>
									</div>

									<div class="col-lg-8 col-md-7 col-sm-8 col-xs-12">
										<p class="pra_con1_des">Starting from 30,000mt per month, ramping up to 100,000 MT</p>
									</div>
								</div>
						    </div>

						    <div class="col-md-12 col-sm-12 col-xs-12 pra_con1">
						     	<div class="row">
									<div class="col-lg-4 col-md-5 col-sm-4 col-xs-12">
										<p class="pra_con1_tl">Hauling Distance <span class="dot_r">:</span></p>
									</div>

									<div class="col-lg-8 col-md-7 col-sm-8 col-xs-12">
										<p class="pra_con1_des">4 km to stockpile facility and 2.0km from stockpile to the jetty</p>
									</div>
								</div>
						    </div>

						    <div class="col-md-12 col-sm-12 col-xs-12 pra_con1">
						     	<div class="row">
									<div class="col-lg-4 col-md-5 col-sm-4 col-xs-12">
										<p class="pra_con1_tl">Stockpile Capacity <span class="dot_r">:</span></p>
									</div>

									<div class="col-lg-8 col-md-7 col-sm-8 col-xs-12">
										<p class="pra_con1_des">80,000mt (50,000mt ROM / 30,000mt Clean Coal)</p>
									</div>
								</div>
						    </div>

						    <div class="col-md-12 col-sm-12 col-xs-12 pra_con1">
						     	<div class="row">
									<div class="col-lg-4 col-md-5 col-sm-4 col-xs-12">
										<p class="pra_con1_tl">Crushing Capacity <span class="dot_r">:</span></p>
									</div>

									<div class="col-lg-8 col-md-7 col-sm-8 col-xs-12">
										<p class="pra_con1_des">250mt per hour</p>
									</div>
								</div>
						    </div>
						</div>
					</div>
				</div>	

				<div class="row pra_cons2">
					<div class="col-md-12 col-sm-12 col-xs-12 pra_con2_1">
						<p>IUP COORDINATE</p>
					</div>

					<div class="col-md-12 col-sm-12 col-xs-12 pra_con2_2">
						<img src="{{ asset('components/front/images/mockup/table_pratama.jpg') }}" class="img-responsive img_center" />
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection