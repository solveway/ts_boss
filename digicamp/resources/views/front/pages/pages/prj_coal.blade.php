@extends($view_path.'.layouts.master')
@section('content')
<div class="row page_head" style="background-image:url('{{ url('components/front/images/mockup/coal_banner.jpg') }}');">
	<div class="page_head_bg"></div>
	
	<div class="cus_container3">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 page_head_tl">
				<p>PROJECT DATA</p>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 page_head_des">
				<h3>COAL EXPORTS</h3>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="cus_container5">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 prj_coal1">
				<img src="{{ asset('components/front/images/mockup/coal_exports.jpg') }}" class="img-responsive img_center img_width" />
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 prj_coal2">
				<div class="row">
					<div class="col-md-offset-1 col-md-10 col-sm-offset-1 col-sm-10 col-xs-12 prj_coal2_1">
						<p>COAL SALES: Company Sales of Coal are mostly dominated to Asian Market such as Japan, Philippines as well as South Korea. In 2015 and 2016, the coal are mainly sold to Japanese Market. Other markets are Taiwan, USA, South Korea, Thailand, India, Vietnam, Bangladesh and Philippines.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection