@extends($view_path.'.layouts.master')
@section('content')
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="row inve_dis">
			<img src="{{ asset('components/front/images/mockup/slideshow.jpg') }}" class="img-responsive cus_size" />

			<div class="inve_bg"></div>

			<div class="inve_desc">
				<div class="inve_desc_tl">
					<h1>WHY INVEST IN BOSS?</h1>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row inve_cons1">
	<div class="cus_container3 inve_abs">
		<div class="row inve_con1">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="cus_container3">
					<div class="row hm_carousel">
						<div class="col-md-12 invest_tl">
							<p>OUR CAPABILITY</p>
						</div>
					</div>

					<div class="row flex_table owl-carousel owl-invest owl-theme hm_carousel">
						<div class="col-md-12 item invest_item">
							<p class="invest_item_tl">RELIABLE</p>
							<p class="invest_item_des">Molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum.</p>
						</div>

						<div class="col-md-12 item invest_item">
							<p class="invest_item_tl">RELIABLE</p>
							<p class="invest_item_des">Molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum.</p>
						</div>

						<div class="col-md-12 item invest_item">
							<p class="invest_item_tl">RELIABLE</p>
							<p class="invest_item_des">Molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum.</p>
						</div>

						<div class="col-md-12 item invest_item">
							<p class="invest_item_tl">RELIABLE</p>
							<p class="invest_item_des">Molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum.</p>
						</div>

						<div class="col-md-12 item invest_item">
							<p class="invest_item_tl">RELIABLE</p>
							<p class="invest_item_des">Molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum.</p>
						</div>

						<div class="col-md-12 item invest_item">
							<p class="invest_item_tl">RELIABLE</p>
							<p class="invest_item_des">Molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum.</p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row flex_table inve_cons2">
			<div class="col-md-6 col-sm-6 col-xs-12 invest_con1_cus_col">
				<div class="invest_con1_img">
					<img src="{{ asset('components/front/images/mockup/hm_con2.jpg') }}" class="img-responsive img_width img_center" />
				</div>
			</div>

			<div class="col-md-6 col-sm-6 col-xs-12 invest_con1_2_r">
				<div class="row invest_con1_2_pad">
					<h3>Unique positioning in the market</h3>

					<p>PT.BOSS operates and supply coal to premium customers. With its High Grade coal quality (high CV, low Sulphur and low Ash) , the company has become one of the very few Indonesian coal companies that is able to penetrate the Japanese Market. BOSS coal currently commands a premium over Australian Newcastle Coal pricing, as it offers significantly lower ash than its Australian alternative.</p>
				</div>

				<div class="row invest_con2_1_pad">
					<div class="invest_con1_2_lm">
						<a href="{{ url('/about-us/vision-mission') }}">learn more <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row invest_con2" style="background-image:url('{{ asset('components/front/images/mockup/hm_con3.jpg') }}');">
	<div class="col-md-12 col-sm-12 col-xs-12 invest_cons2">
		<h3>Integrated control, operations,</h3>
		<h3>infrastructure and logistics.</h3>
		<a href="{{ url('/') }}"><button class="btn btn-default">OUR OPERATIONAL AREA</button></a>
	</div>
</div>
@endsection

@push('custom_scripts')
<script>
$( document ).ready(function() {
	var owl_cus;

    owl_cus = $('.owl-invest').owlCarousel({
	    margin:5,
	    nav:true,
	    dots:false,
	    loop:false,
	    autoplay:false,
	    autoplayTimeout:2000,
	    autoplayHoverPause:true,
	     navText : ["<img src='{{ url('components/front/images/mockup/arrow-left.png') }}' class='img-responsive img_center' />","<img src='{{ url('components/front/images/mockup/arrow-right.png') }}' class='img-responsive img_center' />"],
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:2
	        },
	        1024:{
	            items:3
	        },
	        1439:{
	        	items:4
	        }

	    }
	});

	callback();

    owl_cus.on('translated.owl.carousel', function(e) {
		callback();
	});

	owl_cus.on('dragged.owl.carousel', function(e) {
		callback();
	});

	function callback(event) {
		$('.owl-item.active > .invest_item').css('border-right', '2px solid #999999');
		$('.owl-item.active:last > .invest_item').css('border', '0');
	}
});
</script>
@endpush