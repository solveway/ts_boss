@extends($view_path.'.layouts.master')
@section('content')
<div class="row page_head" style="background-image:url('{{ url('components/front/images/mockup/health_banner.jpg') }}');">
	<div class="page_head_bg"></div>
	
	<div class="cus_container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 page_head_tl">
				<p>CSR</p>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 page_head_des" id="an_csr">
				<h3>HEALTH & SAFETY</h3>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="cus_container3 csr_con">
		<div class="row flex_table csr_consl">
			<div class="col-md-6 col-sm-12 col-xs-12 csr_consl_img">
				<img src="{{ asset('components/front/images/mockup/local-health.jpg') }}" class="img-responsive img_width" />
			</div>

			<div class="col-md-6 col-sm-12 col-xs-12 csr_conl">
				<p class="csr_conl_tl">Local Health Improvement</p>
				<p class="csr_conl_des">BOSS cares about the local people's also employee's health and safety so BOSS organizes health programs and has own medical Clinics.</p>

				<div class="csr_conl_bt"></div>
			</div>
		</div>

		<div class="row flex_table csr_consl">
			<div class="col-md-6 col-sm-12 col-xs-12 csr_consl_img">
				<img src="{{ asset('components/front/images/mockup/economic.jpg') }}" class="img-responsive img_width" />
			</div>

			<div class="col-md-6 col-sm-12 col-xs-12 csr_conl">
				<p class="csr_conl_tl">Economic Development</p>
				<p class="csr_conl_des">BOSS always supporting the economic growth of local people with empowering local people.</p>

				<div class="csr_conl_bt"></div>
			</div>
		</div>

		<div class="row flex_table csr_consl">
			<div class="col-md-6 col-sm-12 col-xs-12 csr_consl_img">
				<img src="{{ asset('components/front/images/mockup/enviromental.jpg') }}" class="img-responsive img_width" />
			</div>

			<div class="col-md-6 col-sm-12 col-xs-12 csr_conl">
				<p class="csr_conl_tl">Environmental Responbility</p>
				<p class="csr_conl_des">as a coal mining company we are also aware of our responsibility and participate in implementing environmental responbility.</p>

				<div class="csr_conl_bt"></div>
			</div>
		</div>
	</div>
</div>
@endsection