@extends($view_path.'.layouts.master')
@section('content')
<div class="row page_head" style="background-image:url('{{ url('components/front/images/mockup/energi_banner.jpg') }}');">
	<div class="page_head_bg"></div>
	
	<div class="cus_container3">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 page_head_tl">
				<p>MINING ASSETS</p>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 page_head_des">
				<h3>PT ENERGI AMZAL BERSAMA</h3>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="cus_container5">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="row flex_table amz_cons1">
					<div class="col-md-6 col-sm-12 col-xs-12">
						<img src="{{ asset('components/front/images/mockup/pt_energi.jpg') }}" class="img-responsive img_width" />
					</div>

					<div class="col-md-6 col-sm-12 col-xs-12 amz_cons1_mg">
						<div class="row align_center">
						    <div class="col-md-12 col-sm-12 col-xs-12 amz_con1">
						     	<div class="row">
									<div class="col-lg-4 col-md-5 col-sm-4 col-xs-12">
										<p class="amz_con1_tl">Legal Name <span class="dot_r">:</span></p>
									</div>

									<div class="col-lg-8 col-md-7 col-sm-8 col-xs-12">
										<p class="amz_con1_des">PT. Energi Amzal Bersama (EAB) established in 2008</p>
									</div>
								</div>
						    </div>

						    <div class="col-md-12 col-sm-12 col-xs-12 amz_con1">
						     	<div class="row">
									<div class="col-lg-4 col-md-5 col-sm-4 col-xs-12">
										<p class="amz_con1_tl">Location <span class="dot_r">:</span></p>
									</div>

									<div class="col-lg-8 col-md-7 col-sm-8 col-xs-12">
										<p class="amz_con1_des">Muara Pahu, Kutai Barat, East Kalimantan</p>
									</div>
								</div>
						    </div>

						    <div class="col-md-12 col-sm-12 col-xs-12 amz_con1">
						     	<div class="row">
									<div class="col-lg-4 col-md-5 col-sm-4 col-xs-12">
										<p class="amz_con1_tl">Minning Area <span class="dot_r">:</span></p>
									</div>

									<div class="col-lg-8 col-md-7 col-sm-8 col-xs-12">
										<p class="amz_con1_des">3,750 Hectares</p>
									</div>
								</div>
						    </div>

						    <div class="col-md-12 col-sm-12 col-xs-12 amz_con1">
						     	<div class="row">
									<div class="col-lg-4 col-md-5 col-sm-4 col-xs-12">
										<p class="amz_con1_tl">Licensin <span class="dot_r">:</span></p>
									</div>

									<div class="col-lg-8 col-md-7 col-sm-8 col-xs-12">
										<p class="amz_con1_des">IUP Production</p>
									</div>
								</div>
						    </div>

						    <div class="col-md-12 col-sm-12 col-xs-12 amz_con1">
						     	<div class="row">
									<div class="col-lg-4 col-md-5 col-sm-4 col-xs-12">
										<p class="amz_con1_tl">Forestry Status <span class="dot_r">:</span></p>
									</div>

									<div class="col-lg-8 col-md-7 col-sm-8 col-xs-12">
										<p class="amz_con1_des">Non Forestry Area (APL)</p>
									</div>
								</div>
						    </div>

						    <div class="col-md-12 col-sm-12 col-xs-12 amz_con1">
						     	<div class="row">
									<div class="col-lg-4 col-md-5 col-sm-4 col-xs-12">
										<p class="amz_con1_tl">Expolration Activites <span class="dot_r">:</span></p>
									</div>

									<div class="col-lg-8 col-md-7 col-sm-8 col-xs-12">
										<p class="amz_con1_des">Exploration drilling activities has been completed.</p>
									</div>
								</div>
						    </div>

						    <div class="col-md-12 col-sm-12 col-xs-12 amz_con1">
						     	<div class="row">
									<div class="col-lg-4 col-md-5 col-sm-4 col-xs-12">
										<p class="amz_con1_tl">JORC Consultant <span class="dot_r">:</span></p>
									</div>

									<div class="col-lg-8 col-md-7 col-sm-8 col-xs-12">
										<p class="amz_con1_des">PT. Runge Pincock Minarco</p>
									</div>
								</div>
						    </div>

						    <div class="col-md-12 col-sm-12 col-xs-12 amz_con1">
						     	<div class="row">
									<div class="col-lg-4 col-md-5 col-sm-4 col-xs-12">
										<p class="amz_con1_tl">Quality of coal<span class="dot_r">:</span></p>
									</div>

									<div class="col-lg-8 col-md-7 col-sm-8 col-xs-12">
										<p class="amz_con1_des">5,300 – 6,000 Kcal/Kg (Adb) with low sulphur and low ash.</p>
									</div>
								</div>
						    </div>

						    <div class="col-md-12 col-sm-12 col-xs-12 amz_con1">
						     	<div class="row">
									<div class="col-lg-4 col-md-5 col-sm-4 col-xs-12">
										<p class="amz_con1_tl">Location<span class="dot_r">:</span></p>
									</div>

									<div class="col-lg-8 col-md-7 col-sm-8 col-xs-12">
										<p class="amz_con1_des">The Concession is located near to  PT. BOS and PT. Trubaindo Coal Mining (BANPU Group)</p>
									</div>
								</div>
						    </div>
						</div>
					</div>
				</div>	

				<div class="row amz_cons2">
					<div class="col-md-12 col-sm-12 col-xs-12 amz_con2_1">
						<p>IUP COORDINATE</p>
					</div>

					<div class="col-md-12 col-sm-12 col-xs-12 amz_con2_2">
						<img src="{{ asset('components/front/images/mockup/table_amzal.jpg') }}" class="img-responsive img_center" />
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection