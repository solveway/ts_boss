<link rel ="stylesheet" href="{{asset('components/plugins/magnific-popup/css/magnific-popup.css')}}">

@extends($view_path.'.layouts.master')
@section('content')
<div class="row page_head" style="background-image:url('{{ url('components/front/images/mockup/production_banner.jpg') }}');">
	<div class="page_head_bg"></div>
	
	<div class="cus_container3">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 page_head_tl">
				<p>PROJECT DATA</p>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 page_head_des">
				<h3>PRODUCTION & HAULING</h3>
			</div>
		</div>
	</div>
</div>

<div class="row prj_pro_cons1">
	<div class="cus_container5">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="row flex_table">
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="row">
							<img src="{{ asset('components/front/images/mockup/pt_buana.jpg') }}" class="img-responsive img_width" />
						</div>
					</div>

					<div class="col-md-6 col-sm-12 col-xs-12 prj_pror1">
						<div class="row margin_cus">
							<div class="col-md-12 col-sm-12 col-xs-12 prj_pror_con1">
						     	<div class="row">
									<div class="col-lg-4 col-md-5 col-sm-4 col-xs-12">
										<p class="prj_pror_tl">Production <span class="dot_r">:</span></p>
									</div>

									<div class="col-lg-8 col-md-7 col-sm-8 col-xs-12 prj_pror_des">
										<p>35,000-40,000 tpm(ramp up to 70K tpm)</p>
									</div>
								</div>
						    </div>

						    <div class="col-md-12 col-sm-12 col-xs-12 prj_pror_con1">
						     	<div class="row">
									<div class="col-lg-4 col-md-5 col-sm-4 col-xs-12">
										<p class="prj_pror_tl">Mining Method <span class="dot_r">:</span></p>
									</div>

									<div class="col-lg-8 col-md-7 col-sm-8 col-xs-12 prj_pror_des">
										<p>Open Pit</p>
									</div>
								</div>
						    </div>

						    <div class="col-md-12 col-sm-12 col-xs-12 prj_pror_con1">
						     	<div class="row">
									<div class="col-lg-4 col-md-5 col-sm-4 col-xs-12">
										<p class="prj_pror_tl">Neighboring Assets <span class="dot_r">:</span></p>
									</div>

									<div class="col-lg-8 col-md-7 col-sm-8 col-xs-12 prj_pror_des">
										<p>PT Pratama Bersama (“PB”)</p>
										<p>More than 4,000 ha and exploration drilling ongoing. Target production in 1st quarter 2018.</p>
										<p>Same strike with same quality as BOS (High CV Coal) and will utilize the existing infrastructure</p>
									</div>
								</div>
						    </div>

						    <div class="col-md-12 col-sm-12 col-xs-12 prj_pror_con1">
						     	<div class="row">
									<div class="col-lg-4 col-md-5 col-sm-4 col-xs-12">
										<p class="prj_pror_tl">Hauling Distance <span class="dot_r">:</span></p>
									</div>

									<div class="col-lg-8 col-md-7 col-sm-8 col-xs-12 prj_pror_des">
										<p>1.3km to stockpile facility and 2.0 km from stockpile to the jetty</p>
									</div>
								</div>
						    </div>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>

<div class="row prj_pro_cons2">
	<div class="cus_container4">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="row flex_table owl-carousel owl-project owl-theme hm_carousel popup-gallery">
					<div class="col-md-12 item">
						<a href="{{ asset('components/front/images/mockup/prj_production1.jpg') }}">
							<img src="{{ asset('components/front/images/mockup/prj_production1.jpg') }}" class="img-responsive" />
						</a>
					</div>

					<div class="col-md-12 item">
						<a href="{{ asset('components/front/images/mockup/prj_production2.jpg') }}">
							<img src="{{ asset('components/front/images/mockup/prj_production2.jpg') }}" class="img-responsive" />
						</a>
					</div>

					<div class="col-md-12 item">
						<a href="{{ asset('components/front/images/mockup/prj_production3.jpg') }}">
							<img src="{{ asset('components/front/images/mockup/prj_production3.jpg') }}" class="img-responsive" />
						</a>
					</div>

					<div class="col-md-12 item">
						<a href="{{ asset('components/front/images/mockup/prj_production4.jpg') }}">
							<img src="{{ asset('components/front/images/mockup/prj_production4.jpg') }}" class="img-responsive" />
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@push('custom_scripts')
<script type="text/javascript" src="{{ asset('components/plugins/magnific-popup/js/jquery.magnific-popup.min.js') }}"></script>

<script>
$( document ).ready(function() {
    $('.owl-project').owlCarousel({
	    margin:0,
	    nav:true,
	    dots:false,
	    loop:false,
	    autoplay:true,
	    autoplayTimeout:2000,
	    autoplayHoverPause:true,
	     navText : ["<img src='{{ url('components/front/images/mockup/arrow-left.png') }}' class='img-responsive img_center' />","<img src='{{ url('components/front/images/mockup/arrow-right.png') }}' class='img-responsive img_center' />"],
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:2
	        },
	        1024:{
	            items:3
	        },
	        1439:{
	        	items:3
	        }

	    }
	});

	$('.popup-gallery').magnificPopup({
		delegate: 'a',
		type: 'image',
		tLoading: 'Loading image #%curr%...',
		mainClass: 'mfp-img-mobile',
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1] // Will preload 0 - before current, and 1 after the current image
		},
		image: {
			tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
		}
	});
});
</script>
@endpush