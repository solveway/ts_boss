@extends($view_path.'.layouts.master')
@section('content')
<div class="row page_head" style="background-image:url('{{ url('components/front/images/mockup/guaranted_banner.jpg') }}');">
	<div class="page_head_bg"></div>
	
	<div class="cus_container3">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 page_head_tl">
				<p>PROJECT DATA</p>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 page_head_des">
				<h3>GUARANTEED SPECIFICATIONS</h3>
			</div>
		</div>
	</div>
</div>

<div class="row prj_guar_cons1">
	<div class="cus_container5">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="row flex_table">
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="row">
							<img src="{{ asset('components/front/images/mockup/pt_buana.jpg') }}" class="img-responsive img_width" />
						</div>
					</div>

					<div class="col-md-6 col-sm-12 col-xs-12 prj_guar_conr_img">
						<div class="row align_center">
							<img src="{{ asset('components/front/images/mockup/prj_guaranted.jpg') }}" class="img-responsive" />
						</div>
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>

<!-- <div class="row prj_guar_cons2">
	<div class="cus_container4">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="row flex_table owl-carousel owl-guaranted owl-theme hm_carousel">
					<div class="col-md-12 item">
						<img src="{{ asset('components/front/images/mockup/prj_production1.jpg') }}" class="img-responsive" />
					</div>

					<div class="col-md-12 item">
						<img src="{{ asset('components/front/images/mockup/prj_production2.jpg') }}" class="img-responsive" />
					</div>

					<div class="col-md-12 item">
						<img src="{{ asset('components/front/images/mockup/prj_production3.jpg') }}" class="img-responsive" />
					</div>

					<div class="col-md-12 item">
						<img src="{{ asset('components/front/images/mockup/prj_production2.jpg') }}" class="img-responsive" />
					</div>

					<div class="col-md-12 item">
						<img src="{{ asset('components/front/images/mockup/prj_production3.jpg') }}" class="img-responsive" />
					</div>

					<div class="col-md-12 item">
						<img src="{{ asset('components/front/images/mockup/prj_production1.jpg') }}" class="img-responsive" />
					</div>
				</div>
			</div>
		</div>
	</div>
</div> -->
@endsection

<!-- @push('custom_scripts')
<script>
$( document ).ready(function() {
    $('.owl-guaranted').owlCarousel({
	    margin:0,
	    nav:true,
	    dots:false,
	    loop:true,
	    autoplay:true,
	    autoplayTimeout:2000,
	    autoplayHoverPause:true,
	     navText : ["<img src='{{ url('components/front/images/mockup/arrow-left.png') }}' class='img-responsive img_center' />","<img src='{{ url('components/front/images/mockup/arrow-right.png') }}' class='img-responsive img_center' />"],
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:2
	        },
	        1024:{
	            items:3
	        },
	        1439:{
	        	items:3
	        }

	    }
	})
});
</script>
@endpush -->