@extends($view_path.'.layouts.master')
@section('content')
<div class="row page_head" style="background-image:url('{{ url('components/front/images/mockup/vission_mission_banner.jpg') }}');">
	<div class="page_head_bg"></div>
	
	<div class="cus_container3">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 page_head_tl">
				<p>ABOUT US</p>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 page_head_des">
				<h3>VISION & MISSION</h3>
			</div>
		</div>
	</div>
</div>

<div class="row vm_con1">
	<div class="cus_container3">
		<div class="row flex_table vm_con1_1">
			<div class="col-md-6 col-sm-12 col-xs-12">
				<!-- <iframe width="100%" height="100%" src="https://www.youtube.com/embed/AhgtoQIfuQ4" frameborder="0" gesture="media" allowfullscreen></iframe> -->
				<img src="{{ asset('components/front/images/mockup/under-construction.jpg') }}" class="img-responsive img_width" />
			</div>

			<div class="col-md-6 col-sm-12 col-xs-12">
				<div class="row align_center">
					<div class="col-md-12 col-sm-12 col-xs-12 vm_con1_r">
						<p class="vm_con1_r1">VISION STATEMENT</p>
						<p class="vm_con1_r2">To be an established and prominent producer in the energy industry, with the focus on meeting the strict and demanding needs of worldwide customers.</p>
					</div>

					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="col-md-12 col-sm-12 col-xs-12 vm_con1_hr"></div>
					</div>

					<div class="col-md-12 col-sm-12 col-xs-12 vm_con1_r">
						<p class="vm_con1_r1">MISSION STATEMENT</p>
						<p class="vm_con1_r2">To produce high grade premium coal in our never ending pursuit to maximize efficient and environmentally friendly source of energy for our premium customers.</p>
					</div>
				</div>
			</div>
		</div>

		<div class="row flex_table">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-12 col-sm-12 col-xs-12 vm_con2_hr"></div>
			</div>

			<div class="col-md-6 col-md-push-6 col-sm-12 col-xs-12">
				<img src="{{ asset('components/front/images/mockup/about_vission_mission.jpg') }}" class="img-responsive img_center img_width" />
			</div>

			<div class="col-md-6 col-md-pull-6 col-sm-12 col-xs-12 vm_con2_r">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<p class="vm_con2_r1">UNIQUE POSITIONING IN THE MARKET</p>

						<ul class="vm_con2_r2">
							<li>
								<p>PT.BOSS operates and supply coal to premium customers. With its High Grade coal quality (high CV, low Sulphur and low Ash) , the company has become one of the very few Indonesian coal companies that is able to penetrate the Japanese Market. BOSS coal currently commands a premium over Australian Newcastle Coal pricing, as it offers significantly lower ash than its Australian alternative.</p>
							</li>

							<li>
								<p>With the company’s presence and acceptance  in the Japanese Market, the company will ramp up the production to expand and diversify new customer base in Japan and new markets for incremental coal volume as well as develop the cooperation with leading coal player such as Banpu Group, Itochu and Glencore etc.</p>
							</li>

							<li>
								<p>Being the first and the only coal company in Indonesia to secure exploration funding from the Japanese Government through its agency Japan Oil Gas Metal Corporation (JOGMEC). </p>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection