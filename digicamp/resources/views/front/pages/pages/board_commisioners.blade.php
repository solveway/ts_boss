@extends($view_path.'.layouts.master')
@section('content')
<div class="row page_head" style="background-image:url('{{ url('components/front/images/mockup/the-boards.jpg') }}');">
	<div class="page_head_bg"></div>
	
	<div class="cus_container3">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 page_head_tl">
				<p>CORPORATE GOVERNANCE</p>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 page_head_des">
				<h3>The Boards</h3>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="cus_container3">
		<div class="row flex_table bdc_con">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="row">
					<div class="col-lg-4 col-md-5 col-sm-6 col-xs-12 bdc_title">
						<h3>Board Of Commissioners</h3>

						<div class="bdc_title_hr"></div>
					</div>
				</div>
			</div>

			<div class="col-md-6 col-sm-12 col-xs-12 bdc_acc1">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
					    <div class="col-md-12 col-sm-12 col-xs-12 bdc_acc1_tl">
					    	<div class="row">
								<div class="col-md-5 col-sm-5 col-xs-12">
									<div class="row">
										<img src="{{ asset('components/front/images/mockup/freddy-setiawan.jpg') }}" class="img-responsive" />
									</div>
								</div>

								<div class="col-md-5 col-sm-5 col-xs-12">
									<p class="bdc_acc1_name">Freddy Setiawan</p>
									<p class="bdc_acc1_job">President Commissioner</p>
								</div>

								<button class="bdc_acc1_button bdc_but_4" type="button" class="btn btn-info" data-toggle="collapse" data-target="#bdc_4"><i class="fa fa-cus fa-sort-desc" aria-hidden="true"></i></button>
							</div>
						</div>

						<div class="row collapse in bdc_cus_col" id="bdc_4" data-id="4">
							<div class="col-md-12 col-sm-12 col-xs-12 bdc_acc1_desc">
								<p>Freddy Setiawan has over 15 years experience in various forestry related industry. He has previously been the Commisioner of Operations at PT Eka Suara Manufacturing and Managing Director at PT Surya Sentosa Buana. He is currently a Commissioner at PT Inhil Hutan Persada, PT. Bangun Hutan Bersama and PT Yawila Bio Energy Perkasa.</p>
								<p>He has an extensive experience in the CPO plantation sector, the coal mining industry and government networking. Besides managing the coal concessions, his management had also built the palm oil plantation close to the area of 100,000 hectares in Indonesia. He is a Commissioner for PT. Buana Mega Sentosa , PT. Bio Diesel Jambi, PT. Tunas Bersusun Abadi, PT. Pratama Mega Sentosa, PT. Tasnida Agro Lestari.</p> 
								<p>Freddy Setiawan is President Commissioner for BOSS Group with 4 coal concessions with the total area more than 16,000 hectares. Mr Setiawan holds a Bachelor of Business Administration from St John’s Institute of Management Science.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-md-6 col-sm-12 col-xs-12 bdc_acc1">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
					    <div class="col-md-12 col-sm-12 col-xs-12 bdc_acc1_tl">
					    	<div class="row">
								<div class="col-md-5 col-sm-5 col-xs-12">
									<div class="row">
										<img src="{{ asset('components/front/images/mockup/johannes-halim.jpg') }}" class="img-responsive" />
									</div>
								</div>

								<div class="col-md-5 col-sm-5 col-xs-12">
									<p class="bdc_acc1_name">Johannes Halim</p>
									<p class="bdc_acc1_job">Commisioner</p>
								</div>

								<button class="bdc_acc1_button bdc_but_2" type="button" class="btn btn-info" data-toggle="collapse" data-target="#bdc_2"><i class="fa fa-cus fa-sort-desc" aria-hidden="true"></i></button>
							</div>
						</div>

						<div class="row collapse bdc_cus_col" id="bdc_2" data-id="2">
							<div class="col-md-12 col-sm-12 col-xs-12 bdc_acc1_desc">
								<p>Johannes Halim has extensive corporate experience having developed numerous property and manufacturing businesses. He was previously an executive director of PT Manunggal Wiratama  which developed the Sun Plaza shopping mall in Medan.</p>
								<p>He is currently a commissioner of BOSS Group, PT Pesona Aura Biru, PT Pilotindo Prima. Johannes Halim holds a Master of Business Administration from the State University of New York.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-md-6 col-sm-12 col-xs-12 bdc_acc1">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
					    <div class="col-md-12 col-sm-12 col-xs-12 bdc_acc1_tl">
					    	<div class="row">
								<div class="col-md-5 col-sm-5 col-xs-12">
									<div class="row">
										<img src="{{ asset('components/front/images/mockup/supandi-ws.jpg') }}" class="img-responsive" />
									</div>
								</div>

								<div class="col-md-5 col-sm-5 col-xs-12">
									<p class="bdc_acc1_name">Supandi WS</p>
									<p class="bdc_acc1_job">Independent Commisioner</p>
								</div>

								<button class="bdc_acc1_button bdc_but_5" type="button" class="btn btn-info" data-toggle="collapse" data-target="#bdc_5"><i class="fa fa-cus fa-sort-desc" aria-hidden="true"></i></button>
							</div>
						</div>

						<div class="row collapse bdc_cus_col" id="bdc_5" data-id="5">
							<div class="col-md-12 col-sm-12 col-xs-12 bdc_acc1_desc">
								<p>Supandi WS has over 15 years experience in roles in the finance industry he has appointed as Independent Commissioner of the BOSS Group since August 2016. He has previously worked at Manager of Listing Division PT. Indonesia Stock Exchange (1989 - 1992), as Director of Finance and Human Resources Indonesia Stock Exchange (2009-2012), Commissioner of PT Sitara Propertindo Tbk (2013 - June 2016), and an Independent Commissioner of PT. SMR Utama Tbk and President Commissioner of PT Pan Brothers Tex Tbk, PT Andira Agro, PT Garuda Investindo, and PT Intensive Medicare 177 from the year 2013 until now.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>

<div class="row">
	<div class="cus_container3">
		<div class="row flex_table bdc_con">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="row">
					<div class="col-lg-4 col-md-5 col-sm-6 col-xs-12 bdc_title">
						<h3>Board Of Directors</h3>

						<div class="bdc_title_hr"></div>
					</div>
				</div>
			</div>
			
			<div class="col-md-6 col-sm-12 col-xs-12 bdc_acc1">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
					    <div class="col-md-12 col-sm-12 col-xs-12 bdc_acc1_tl">
					    	<div class="row">
								<div class="col-md-5 col-sm-5 col-xs-12">
									<div class="row">
										<img src="{{ asset('components/front/images/mockup/freddy-tedjasasmita.jpg') }}" class="img-responsive" />
									</div>
								</div>

								<div class="col-md-5 col-sm-5 col-xs-12">
									<p class="bdc_acc1_name">Freddy Tedjasasmita</p>
									<p class="bdc_acc1_job">President Director</p>
								</div>

								<button class="bdc_acc1_button bdc_but_3" type="button" class="btn btn-info" data-toggle="collapse" data-target="#bdc_3"><i class="fa fa-cus fa-sort-desc" aria-hidden="true"></i></button>
							</div>
						</div>

						<div class="row collapse bdc_cus_col" id="bdc_3" data-id="3">
							<div class="col-md-12 col-sm-12 col-xs-12 bdc_acc1_desc">
								<p>Freddy Tedjasasmita has extensive experience in commodities, particularly energy related commodities, having held finance, sales, trading and management roles in his over 15 years of experience. He has previously developed PT Diva Kencana Borneo from Greenfield to production in under 2 years. He has also previously been president director of PT Indo Prima Foods, Director of PT Swadaya Agri Sentosa and Operational Director of PT Belfoods Indonesia.</p>
								<p>Freddy Tedjasasmita is President Director for BOSS Group with 4 coal concessions with the total area more than 16,000 hectares</p>
								<p>Freddy Tedjasasmita holds a Bachelor of Commerce majoring in Economics and Finance from Curtin University of Technology in Western Australia.</p>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-6 col-sm-12 col-xs-12 bdc_acc1">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
					    <div class="col-md-12 col-sm-12 col-xs-12 bdc_acc1_tl">
					    	<div class="row">
								<div class="col-md-5 col-sm-5 col-xs-12">
									<div class="row">
										<img src="{{ asset('components/front/images/mockup/widodo-nurly-sumady.jpg') }}" class="img-responsive" />
									</div>
								</div>

								<div class="col-md-5 col-sm-5 col-xs-12">
									<p class="bdc_acc1_name">Widodo Nurly Sumady</p>
									<p class="bdc_acc1_job">Executive Director</p>
								</div>

								<button class="bdc_acc1_button bdc_but_1" type="button" class="btn btn-info" data-toggle="collapse" data-target="#bdc_1"><i class="fa fa-cus fa-sort-asc bdc_acc1_asc" aria-hidden="true"></i></button>
							</div>
						</div>

						<div class="row collapse bdc_cus_col" id="bdc_1" data-id="1">
							<div class="col-md-12 col-sm-12 col-xs-12 bdc_acc1_desc">
								<p>Widodo Nurly Sumady has over 15 years experience in roles in the finance industry and in commodity businesses. He has previously worked at Tamara Bank in its investment banking division, Pinnacle Resource Consultants where he was the Managing Director, and as an executive director at several palm oil plantations including PT. Buana Mega Sentosa , PT. Bio Diesel Jambi, PT. Tunas Bersusun Abadi, PT. Pratama Mega Sentosa, PT. Tasnida Agro Lestari.</p>
								<p>Widodo Nurly Sumady was Commissioner PT Diva Kencana Borneo (coal mining). He is also an Executive Director for BOSS Group with 4 coal concessions including PT. Bangun Olah Sarana Sukses</p>
								<p>Widodo Nurly Sumady holds a Bachelor of Finance and Marketing from Curtin University of Technology in Western Australia.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-md-6 col-sm-12 col-xs-12 bdc_acc1">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
					    <div class="col-md-12 col-sm-12 col-xs-12 bdc_acc1_tl">
					    	<div class="row">
								<div class="col-md-5 col-sm-5 col-xs-12">
									<div class="row">
										<img src="{{ asset('components/front/images/mockup/reza-pranata.jpg') }}" class="img-responsive" />
									</div>
								</div>

								<div class="col-md-5 col-sm-5 col-xs-12">
									<p class="bdc_acc1_name">Reza Pranata</p>
									<p class="bdc_acc1_job">Independent Director</p>
								</div>

								<button class="bdc_acc1_button bdc_but_6" type="button" class="btn btn-info" data-toggle="collapse" data-target="#bdc_6"><i class="fa fa-cus fa-sort-desc" aria-hidden="true"></i></button>
							</div>
						</div>

						<div class="row collapse bdc_cus_col" id="bdc_6" data-id="6">
							<div class="col-md-12 col-sm-12 col-xs-12 bdc_acc1_desc">
								<p>Reza Pranata has extensive experience over 10 years in the mining industry and in commodity businesses.  He has previously worked at PT KTC Coal Mining & Energy ( Januari 2006 – April 2007 ), PT. Pipit Mutiara Jaya ( April 2007 – Maret 2008 ), PT. Globalindo Alam Lestari ( May 2008 – November 2010 ), PT. Asta Minindo ( November 2011 – November 2012 ), and PT Bayan Resources,tbk. ( December 2012 – May 2017 ). He is currently appointed as Independent Director BOSS Group.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>
@endsection
@push('custom_scripts')
<script>
  $(document).ready(function() {
  	$(".bdc_cus_col").on("hide.bs.collapse", function(){
      var a = $(this).data('id');
      $('.bdc_but_'+a+' i').removeClass('fa-sort-asc');
      $('.bdc_but_'+a+' i').addClass('fa-sort-desc');
    });
    $(".bdc_cus_col").on("show.bs.collapse", function(){
      var a = $(this).data('id');
      var c = $('.bdc_cus_col.collapse.in');
      $('.bdc_but_'+a+' i').removeClass('fa-sort-desc');
      $('.bdc_but_'+a+' i').addClass('fa-sort-asc');

      c.each(function(i,v){
      	var ch = $(this).data('id');

      	if(ch != a){
      		$(this).collapse('hide');;
      	}
      });
    });
  });
</script>
@endpush