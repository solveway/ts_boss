@extends($view_path.'.layouts.master')
@section('content')
<div class="row page_head" style="background-image:url('{{ url('components/front/images/mockup/news_banner.jpg') }}');">
	<div class="page_head_bg"></div>
	
	<div class="cus_container3">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 page_head_tl">
				<p>NEWS</p>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 page_head_des" id="an_news">
				<h3>NEWS RELEASE</h3>
			</div>
		</div>
	</div>
</div>

@if($news != "")
<div class="row nws_pro_cons1">
	<div class="cus_container3">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="row flex_table">
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12 cus_padding_minus">
								<img id="nw-ch-img" src="{{ asset('components/admin/image/news') }}/{{ $news->image }}" class="img-responsive img_width" />
							</div>
						</div>
					</div>

					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="row nws_pro_con1">
							<div class="col-md-12 col-sm-12 col-xs-12 nws_pro_h3" id="nw-ch-title">
								<h3>{{ $news->title }}</h3>
							</div>

							<div class="col-md-12 col-sm-12 col-xs-12" id="nw-ch-desc">
								{!! $news->description !!}
							</div>
						</div>
					</div>
				</div>	

				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 nws_pro_con2">
						<p>ALSO ON NEWS RELEASE</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endif

@if($list_news != "")

	@php
		$count = count($list_news);
		
		if($count > 0 && $count <= 4){
			$cus_class = "cus_navigation";
		} else{
			$cus_class = "";
		}
	@endphp
<div class="row">
	<div class="cus_container2">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 nws_pro_con3">
				<div class="row flex_table owl-carousel owl-news owl-theme hm_carousel {{ $cus_class }}">
					@foreach($list_news as $k => $l_news)
					<div class="col-md-12 item nws_item">
						<div class="row">
							<div class="col-md-10 col-sm-10 col-xs-offset-1 col-xs-10 csr_cus_pad">
								<img id="nws-ch-{{ $k }}" src="{{ asset('components/admin/image/news') }}/{{ $l_news->image }}" class="img-responsive nws_img" />

								@php
									$tl = "<h3>".$l_news->title."</h3>";
								@endphp

								<p class="csr_item_tl csr_click" data-value="{{ $tl }}" data-id="{{ $k }}">{{ $l_news->title }}</p>
								<div class="csr_item_des" id="nws-ch-desc-{{ $k }}" data-value="{{ $l_news->description }}">{!! $l_news->description !!}</div>
							</div>
						</div>
					</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</div>
@endif
@endsection

@push('custom_scripts')
<script>
$( document ).ready(function() {
	var owl_cus;

    owl_cus = $('.owl-news').owlCarousel({
	    margin:1,
	    nav:true,
	    dots:false,
	    loop:false,
	    autoplay:false,
	    autoplayTimeout:2000,
	    autoplayHoverPause:true,
	     navText : ["<img src='{{ url('components/front/images/mockup/arrow-left.png') }}' class='img-responsive img_center' />","<img src='{{ url('components/front/images/mockup/arrow-right.png') }}' class='img-responsive img_center' />"],
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:2
	        },
	        1024:{
	            items:3
	        },
	        1439:{
	        	items:4
	        }

	    }
	});

	callback();

    owl_cus.on('translated.owl.carousel', function(e) {
		callback();
	});

	owl_cus.on('dragged.owl.carousel', function(e) {
		callback();
	});

	function callback(event) {
		$('.owl-item.active > .nws_item').css('border-right', '2px solid #999999');
		$('.owl-item.active:last > .nws_item').css('border', '0');
	}

    $(document).on('click', '.csr_click', function(){
    	var old_tl = $('#nw-ch-title').text();
    	var old_desc = $('#nw-ch-desc').html();
    	var old_image =  $('#nw-ch-img').attr('src');

    	var id = $(this).data('id');
    	var title = $(this).data('value');
    	var image = $('#nws-ch-'+id).attr('src');
    	var content = $('#nws-ch-desc-'+id).data('value'); 

    	$('#nw-ch-title').html(title);
    	$('#nw-ch-desc').html(content);
    	$('#nw-ch-img').attr("src", image);

    	$('html, body').animate({
	        scrollTop: $("#an_news").offset().top
	    }, 1000);

    	$(this).text(old_tl);
    	$('#nws-ch-'+id).attr("src", old_image);
    	$('#nws-ch-desc-'+id).html($old_desc); 
	});	
});
</script>
@endpush