@extends($view_path.'.layouts.master')
@section('content')
<div class="row page_head" style="background-image:url('{{ url('components/front/images/mockup/search_banner.jpeg') }}');">
	<div class="page_head_bg"></div>
	
	<div class="cus_container3">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 page_head_tl">
				<p>&nbsp;</p>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 page_head_des">
				<h3>Search</h3>
			</div>
		</div>
	</div>
</div>

<div class="row scr_con">
	<div class="cus_container3">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<h1>Search Result</h1>
			</div>
			@if($news != "")
				@foreach($news as $q => $nw)
				<div class="col-md-12 col-sm-12 col-xs-12">
					<form method="POST" action="{{ url('/news-release') }}" id="news_{{ $q }}">
						{{ csrf_field() }}

						<input type="hidden" name="ids" value="{{ $nw->id }}" />
						
						<h3 class="scr_tl" data-id="{{ $q }}">{{ $nw->title }}</h3>
						<div class="scr_desc">{!! $nw->description !!}</div>
					</form>
				</div>
				@endforeach
			@else
				<div class="col-md-12 col-sm-12 col-xs-12 scr_desc">
					<h3>Sorry, Search Result Not Found.</h3>
				</div>
			@endif
		</div>
	</div>
</div>
@endsection

@push('custom_scripts')
<script>
$(document).ready(function() {
	$(document).on('click', '.scr_tl', function(){
		var id = $(this).data('id');
		$('#news_'+id).submit();
	});
});
</script>
@endpush