<link rel ="stylesheet" href="{{asset('components/plugins/magnific-popup/css/magnific-popup.css')}}">

@extends($view_path.'.layouts.master')
@section('content')
<div class="row page_head" style="background-image:url('{{ url('components/front/images/mockup/coal_banner.jpg') }}');">
	<div class="page_head_bg"></div>
	
	<div class="cus_container3">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 page_head_tl">
				<p>PROJECT DATA</p>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 page_head_des">
				<h3>DOCUMENTS</h3>
			</div>
		</div>
	</div>
</div>

<div class="row doc_con">
	<div class="cus_container3">
		<div class="row doc_cons">
			<div class="col-md-12 col-sm-12 col-xs-12 doc_tl">
				<h3>Gallery</h3>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 popup-gallery">
				<div class="row">
					<a class="col-md-3 col-sm-6 col-xs-12 doc_img" href="{{ asset('components/front/images/mockup/cs1.jpg') }}">
						<img src="{{ asset('components/front/images/mockup/cs1.jpg') }}" class="img-responsive">
					</a>

					<a class="col-md-3 col-sm-6 col-xs-12 doc_img" href="{{ asset('components/front/images/mockup/cs2.jpg') }}">
						<img src="{{ asset('components/front/images/mockup/cs2.jpg') }}" class="img-responsive">
					</a>

					<a class="col-md-3 col-sm-6 col-xs-12 doc_img" href="{{ asset('components/front/images/mockup/cs4.jpg') }}">
						<img src="{{ asset('components/front/images/mockup/cs4.jpg') }}" class="img-responsive">
					</a>

					<a class="col-md-3 col-sm-6 col-xs-12 doc_img" href="{{ asset('components/front/images/mockup/cs5.jpg') }}">
						<img src="{{ asset('components/front/images/mockup/cs5.jpg') }}" class="img-responsive">
					</a>
				</div>
			</div>
		</div>

		<div class="row doc_cons2">
			<div class="col-md-12 col-sm-12 col-xs-12 doc_tl">
				<h3>Document</h3>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
				<table class="table table-bordered doc_table">
					<thead>
				      	<tr>
				        	<th class="center">No</th>
				        	<th>Link</th>
				        </tr>
				    </thead>

				    <tbody>
				      	<tr>
				        	<td>
				        		<p class="center">1</p>	
				        	</td>

				        	<td>
				        		<a href="{{ url('components/front/images/mockup/pdf-1.pdf') }}" target="_blank">Certificate Of Sampling And Analysis</a>
				        	</td>
				        </tr>

				        <tr>
				        	<td>
				        		<p class="center">2</p>	
				        	</td>

				        	<td>
				        		<a href="{{ url('components/front/images/mockup/cnc.pdf') }}" target="_blank">Clear And Clean</a>
				        	</td>
				        </tr>

				        <tr>
				        	<td>
				        		<p class="center">3</p>	
				        	</td>

				        	<td>
				        		<a href="{{ url('components/front/images/mockup/et.pdf') }}" target="_blank">Eksportir Terdaftar</a>
				        	</td>
				        </tr>
				    </tbody>
				</table>
			</div>

			<!-- <div class="col-md-3 col-sm-6 col-xs-12 doc_pdf">
				<a href="{{ url('components/front/images/mockup/pdf-1.pdf') }}" target="_blank">
					<img src="{{ asset('components/front/images/mockup/pdf-image.png') }}" title="Certificate Of Sampling And Analysis" class="img-responsive" />
				</a>
			</div>
			
			<div class="col-md-3 col-sm-6 col-xs-12 doc_pdf">
				<a href="{{ url('components/front/images/mockup/cnc.pdf') }}" target="_blank">
					<img src="{{ asset('components/front/images/mockup/pdf-image.png') }}" title="PDF 1" class="img-responsive" />
				</a>
			</div>
			
			<div class="col-md-3 col-sm-6 col-xs-12 doc_pdf">
				<a href="{{ url('components/front/images/mockup/et.pdf') }}" target="_blank">
					<img src="{{ asset('components/front/images/mockup/pdf-image.png') }}" title="PDF 1" class="img-responsive" />
				</a>
			</div> -->
		</div>
	</div>
</div>
@endsection

@push('custom_scripts')
<script type="text/javascript" src="{{ asset('components/plugins/magnific-popup/js/jquery.magnific-popup.min.js') }}"></script>

<script>
$(document).ready(function() {
	$('.popup-gallery').magnificPopup({
		delegate: 'a',
		type: 'image',
		tLoading: 'Loading image #%curr%...',
		mainClass: 'mfp-img-mobile',
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1] // Will preload 0 - before current, and 1 after the current image
		},
		image: {
			tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
		}
	});
});
</script>
@endpush