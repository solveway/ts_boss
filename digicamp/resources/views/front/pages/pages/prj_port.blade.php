@extends($view_path.'.layouts.master')
@section('content')
<div class="row page_head" style="background-image:url('{{ url('components/front/images/mockup/port_banner.jpg') }}');">
	<div class="page_head_bg"></div>
	
	<div class="cus_container3">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 page_head_tl">
				<p>PROJECT DATA</p>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 page_head_des">
				<h3>PORT FACILITY</h3>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="cus_container5 prj_con">
		<div class="row prj_port_cons1">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="row flex_table">
					<div class="col-md-6 col-sm-12 col-xs-12 prj_port_img">
						<div class="row align_center_nolr">
							<img src="{{ asset('components/front/images/mockup/pj_port.jpg') }}" class="img-responsive img_width" />
						</div>
					</div>

					<div class="col-md-6 col-sm-12 col-xs-12 prj_portr1">
						<div class="row margin_cus">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="row">
									<div class="col-md-12 col-sm-12 col-xs-12 prj_portr_tl">
										<p>Two Jetty’s facilities owned by the Company</p>
									</div>

									<div class="col-md-12 col-sm-12 col-xs-12 prj_portr_tls2">
										<p>Once crushed, BOS coal is hauled to either Jetty 1 or Jetty 2</p>
									</div>
								</div>
							</div>

							<div class="col-md-12 col-sm-12 col-xs-12 prj_portr_con1">
						     	<div class="row">
									<div class="col-lg-4 col-md-5 col-sm-4 col-xs-12">
										<p class="prj_portr_tl">Option 1 <span class="dot_r">:</span></p>
									</div>

									<div class="col-lg-8 col-md-7 col-sm-8 col-xs-12 prj_portr_des">
										<p>It is then loaded onto 180 ft / 230ft barges (up to 3,000mt) and barged 180 nautical miles along the Kedang Pahu and Mahakam rivers directly to Muara Jawa anchorage.</p>
									</div>
								</div>
						    </div>

						    <div class="col-md-12 col-sm-12 col-xs-12 prj_portr_con1">
						     	<div class="row">
									<div class="col-lg-4 col-md-5 col-sm-4 col-xs-12">
										<p class="prj_portr_tl">Option 2 <span class="dot_r">:</span></p>
									</div>

									<div class="col-lg-8 col-md-7 col-sm-8 col-xs-12 prj_portr_des">
										<p>It is then loaded onto Self Discharging Barges and then transshipped to either 270ft / 300ft barges (up to 7,500 mt) and barged to Muara Jawa anchorage.</p>
									</div>
								</div>
						    </div>
						</div>
					</div>
				</div>	
			</div>
		</div>

		<div class="row prj_port_cons2">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="row flex_table">
					<div class="col-md-6 col-sm-12 col-xs-12 port_acc1">
						<div class="row">
						    <div class="col-md-12 col-sm-12 col-xs-12 port_acc1_tl">
						    	<div class="row">
									<div class="col-md-5 col-sm-5 col-xs-12">
										<div class="row">
											<img src="{{ asset('components/front/images/mockup/prj_fort1.jpg') }}" class="img-responsive" />
										</div>
									</div>

									<div class="col-md-5 col-sm-5 col-xs-12">
										<p class="port_acc1_name">JETTY 1</p>
									</div>

									<button class="port_acc1_button port_but_1" type="button" class="btn btn-info" data-toggle="collapse" data-target="#port_1"><i class="fa fa-cus fa-sort-asc port_acc1_asc" aria-hidden="true"></i></button>
								</div>
							</div>

							<div class="row collapse in port_cus_col" id="port_1" data-id="1">
								<div class="col-md-12 col-sm-12 col-xs-12 port_acc1_desc">
									<div class="row">
										<div class="col-md-12 col-sm-12 col-xs-12 prj_portr_con1">
									     	<div class="row">
												<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
													<p class="prj_portr_tl">Coordinate <span class="dot_r">:</span></p>
												</div>

												<div class="col-lg-9 col-md-8 col-sm-8 col-xs-12 port_acc1_content">
													<p>0° 24' 12.2" S/ 115° 54' 48.8" E </p>
												</div>
											</div>
									    </div>

									    <div class="col-md-12 col-sm-12 col-xs-12 prj_portr_con1">
									     	<div class="row">
												<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
													<p class="prj_portr_tl">Loading Method <span class="dot_r">:</span></p>
												</div>

												<div class="col-lg-9 col-md-8 col-sm-8 col-xs-12 port_acc1_content">
													<p>Conveyor Loading</p>
												</div>
											</div>
									    </div>

									    <div class="col-md-12 col-sm-12 col-xs-12 prj_portr_con1">
									     	<div class="row">
												<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
													<p class="prj_portr_tl">Capacity <span class="dot_r">:</span></p>
												</div>

												<div class="col-lg-9 col-md-8 col-sm-8 col-xs-12 port_acc1_content">
													<p>500 tph</p>
												</div>
											</div>
									    </div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-6 col-sm-12 col-xs-12 port_acc1">
						<div class="row">
						    <div class="col-md-12 col-sm-12 col-xs-12 port_acc1_tl">
						    	<div class="row">
									<div class="col-md-5 col-sm-5 col-xs-12">
										<div class="row">
											<img src="{{ asset('components/front/images/mockup/prj_fort2.jpg') }}" class="img-responsive" />
										</div>
									</div>

									<div class="col-md-5 col-sm-5 col-xs-12">
										<p class="port_acc1_name">JETTY 2</p>
									</div>

									<button class="port_acc1_button port_but_2" type="button" class="btn btn-info" data-toggle="collapse" data-target="#port_2"><i class="fa fa-cus fa-sort-asc port_acc1_asc" aria-hidden="true"></i></button>
								</div>
							</div>

							<div class="row collapse in port_cus_col" id="port_2" data-id="2">
								<div class="col-md-12 col-sm-12 col-xs-12 port_acc1_desc">
									<div class="row">
										<div class="col-md-12 col-sm-12 col-xs-12 prj_portr_con1">
									     	<div class="row">
												<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
													<p class="prj_portr_tl">Coordinate <span class="dot_r">:</span></p>
												</div>

												<div class="col-lg-9 col-md-8 col-sm-8 col-xs-12 port_acc1_content">
													<p>0° 24' 12.2" S/ 115° 54' 48.8" E </p>
												</div>
											</div>
									    </div>

									    <div class="col-md-12 col-sm-12 col-xs-12 prj_portr_con1">
									     	<div class="row">
												<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
													<p class="prj_portr_tl">Loading Method <span class="dot_r">:</span></p>
												</div>

												<div class="col-lg-9 col-md-8 col-sm-8 col-xs-12 port_acc1_content">
													<p>Conveyor Loading</p>
												</div>
											</div>
									    </div>

									    <div class="col-md-12 col-sm-12 col-xs-12 prj_portr_con1">
									     	<div class="row">
												<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
													<p class="prj_portr_tl">Capacity <span class="dot_r">:</span></p>
												</div>

												<div class="col-lg-9 col-md-8 col-sm-8 col-xs-12 port_acc1_content">
													<p>500 tph</p>
												</div>
											</div>
									    </div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@push('custom_scripts')
<script>
  $(document).ready(function() {
  	$(".port_cus_col").on("hide.bs.collapse", function(){
      var a = $(this).data('id');
      $('.port_but_'+a+' i').removeClass('fa-sort-asc');
      $('.port_but_'+a+' i').addClass('fa-sort-desc');
    });
    $(".port_cus_col").on("show.bs.collapse", function(){
      var a = $(this).data('id');
      var c = $('.port_cus_col.collapse.in');
      $('.port_but_'+a+' i').removeClass('fa-sort-desc');
      $('.port_but_'+a+' i').addClass('fa-sort-asc');

      c.each(function(i,v){
      	var ch = $(this).data('id');

      	if(ch != a){
      		$(this).collapse('hide');;
      	}
      });
    });
  });
</script>
@endpush