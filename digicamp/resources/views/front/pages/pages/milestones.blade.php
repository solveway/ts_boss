@extends($view_path.'.layouts.master')
@section('content')
<div class="row page_head" style="background-image:url('{{ url('components/front/images/mockup/milestones.jpg') }}');">
	<div class="page_head_bg"></div>
	
	<div class="cus_container3">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 page_head_tl">
				<p>ABOUT US</p>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 page_head_des">
				<h3>MILESTONES</h3>
			</div>
		</div>
	</div>
</div>

<div class="row flex_table ml_con1_1">
	<div class="col-md-3 col-sm-3 col-xs-12 ml_con1l">
		<div class="ml_cons1l">
			<ul class="ml_list">
			    <li class="slider first li_active">
			    	<p class="ml_slide" data-id="1" data-image="{{ asset('components/front/images/mockup/mining-industry.jpg') }}">2013</p>
			    </li>

			    <li class="slider">
			    	<p class="ml_slide" data-id="2" data-image="{{ asset('components/front/images/mockup/port_banner.jpg') }}">2014</p>
			    </li>

			    <li class="slider">
			    	<p class="ml_slide" data-id="3" data-image="{{ asset('components/front/images/mockup/milestones.jpg') }}">2015</p>
			    </li>

			    <li class="slider">
			    	<p class="ml_slide" data-id="4" data-image="{{ asset('components/front/images/mockup/ml3.jpg') }}">2016</p>
			    </li>

			    <li class="slider">
			    	<p class="ml_slide" data-id="5" data-image="{{ asset('components/front/images/mockup/pt_bangun.jpg') }}">2017</p>
			    </li>

			    <li class="slider">
			    	<p class="ml_slide" data-id="6" data-image="{{ asset('components/front/images/mockup/ml3.jpg') }}">2018</p>
			    </li>

			    <li class="slider">
			    	<p class="ml_slide" data-id="7" data-image="{{ asset('components/front/images/mockup/production_banner.jpg') }}">2019</p>
			    </li>

			    <li class="slider last">
			    	<p class="ml_slide" data-id="8" data-image="{{ asset('components/front/images/mockup/mining-industry.jpg') }}">2020</p>
			    </li>
			</ul>
			
			<button class="ml_button ml_btn_prevs">
				<i class="fa fa-chevron-up" aria-hidden="true"></i>
			</button>

			<button class="ml_button ml_btn_nexts">
				<i class="fa fa-chevron-down" aria-hidden="true"></i>
			</button>
		</div>
	</div>

	<div class="col-md-9 col-sm-9 col-xs-12 ml_con1r" id="back_image" style="background-image:url('{{ asset('components/front/images/mockup/mining-industry.jpg') }}');">
		<div class="row ml_con1r_bg ml_con1_active" id="ml_con1r_slide1">
			<div class="col-md-12 col-sm-12 col-xs-12 ml_con1r_con1">
				<p>The management has brought one of their concessions namely PT Bangun Olahsarana Sukses (</p>
				<p>BOS) into production</p>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 ml_con1r_con2">
				<p>2013</p>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 ml_con1r_desc">
				<p>BOS has sold the coal to major clients as Glencore, Banpu Group, Avra, Trafigura etc. and become</p>
				<p>one of the coal producer able to penetrate the Japan Market.</p>
			</div>
		</div>

		<div class="row ml_con1r_bg" id="ml_con1r_slide2">
			<div class="col-md-12 col-sm-12 col-xs-12 ml_con1r_con1">
				<p>The management has brought one of their concessions namely PT Bangun Olahsarana Sukses (</p>
				<p>BOS) into production</p>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 ml_con1r_con2">
				<p>2014</p>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 ml_con1r_desc">
				<p>BOS has sold the coal to major clients as Glencore, Banpu Group, Avra, Trafigura etc. and become</p>
				<p>one of the coal producer able to penetrate the Japan Market.</p>
			</div>
		</div>

		<div class="row ml_con1r_bg" id="ml_con1r_slide3">
			<div class="col-md-12 col-sm-12 col-xs-12 ml_con1r_con1">
				<p>The management has brought one of their concessions namely PT Bangun Olahsarana Sukses (</p>
				<p>BOS) into production</p>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 ml_con1r_con2">
				<p>2015</p>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 ml_con1r_desc">
				<p>BOS has sold the coal to major clients as Glencore, Banpu Group, Avra, Trafigura etc. and become</p>
				<p>one of the coal producer able to penetrate the Japan Market.</p>
			</div>
		</div>

		<div class="row ml_con1r_bg" id="ml_con1r_slide4">
			<div class="col-md-12 col-sm-12 col-xs-12 ml_con1r_con1">
				<p>The management has brought one of their concessions namely PT Bangun Olahsarana Sukses (</p>
				<p>BOS) into production</p>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 ml_con1r_con2">
				<p>2016</p>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 ml_con1r_desc">
				<p>BOS has sold the coal to major clients as Glencore, Banpu Group, Avra, Trafigura etc. and become</p>
				<p>one of the coal producer able to penetrate the Japan Market.</p>
			</div>
		</div>

		<div class="row ml_con1r_bg" id="ml_con1r_slide5">
			<div class="col-md-12 col-sm-12 col-xs-12 ml_con1r_con1">
				<p>The management has brought one of their concessions namely PT Bangun Olahsarana Sukses (</p>
				<p>BOS) into production</p>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 ml_con1r_con2">
				<p>2017</p>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 ml_con1r_desc">
				<p>BOS has sold the coal to major clients as Glencore, Banpu Group, Avra, Trafigura etc. and become</p>
				<p>one of the coal producer able to penetrate the Japan Market.</p>
			</div>
		</div>

		<div class="row ml_con1r_bg" id="ml_con1r_slide6">
			<div class="col-md-12 col-sm-12 col-xs-12 ml_con1r_con1">
				<p>The management has brought one of their concessions namely PT Bangun Olahsarana Sukses (</p>
				<p>BOS) into production</p>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 ml_con1r_con2">
				<p>2018</p>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 ml_con1r_desc">
				<p>BOS has sold the coal to major clients as Glencore, Banpu Group, Avra, Trafigura etc. and become</p>
				<p>one of the coal producer able to penetrate the Japan Market.</p>
			</div>
		</div>

		<div class="row ml_con1r_bg" id="ml_con1r_slide7">
			<div class="col-md-12 col-sm-12 col-xs-12 ml_con1r_con1">
				<p>The management has brought one of their concessions namely PT Bangun Olahsarana Sukses (</p>
				<p>BOS) into production</p>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 ml_con1r_con2">
				<p>2019</p>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 ml_con1r_desc">
				<p>BOS has sold the coal to major clients as Glencore, Banpu Group, Avra, Trafigura etc. and become</p>
				<p>one of the coal producer able to penetrate the Japan Market.</p>
			</div>
		</div>

		<div class="row ml_con1r_bg" id="ml_con1r_slide8">
			<div class="col-md-12 col-sm-12 col-xs-12 ml_con1r_con1">
				<p>The management has brought one of their concessions namely PT Bangun Olahsarana Sukses (</p>
				<p>BOS) into production</p>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 ml_con1r_con2">
				<p>2020</p>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 ml_con1r_desc">
				<p>BOS has sold the coal to major clients as Glencore, Banpu Group, Avra, Trafigura etc. and become</p>
				<p>one of the coal producer able to penetrate the Japan Market.</p>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="cus_container">
		<div class="row ml_con1_2">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="row">
					<div class="col-md-6 col-sm-12 col-xs-12 ml_con1_2_1">
						<img src="{{ asset('components/front/images/mockup/ml1.jpg') }}" class="img-responsive img_width" />

						<p>In order to ramp production, BOSS will arrange more heavy equipment to be mobilized to the mine site.</p>
					</div>

					<div class="col-md-6 col-sm-12 col-xs-12 ml_con1_2_1">
						<img src="{{ asset('components/front/images/mockup/ml2.jpg') }}" class="img-responsive img_width" />

						<p>BOSS is currently expanding the operating by expanding our Barge Loading Conveyor (BLC), constructing new crusher, fuel storage, workshop, camp and new facilities, in addition to the existing facilities. The constructions had started in June 2017 and expected to be completed in November 2017.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@push('custom_scripts')
<script>
$( document ).ready(function() {
	$('.ml_list li:gt(5)').css('display', 'none');

	$(".ml_btn_nexts").click(function() {  
		if(!$('.ml_list > .li_active').hasClass('last')){
			$('.ml_list > .li_active').next().addClass('li_active');
			$('.ml_list > .li_active').prev().removeClass('li_active');

			var id = $('.ml_list > .li_active > .ml_slide').data('id');
			var image = $('.ml_list > .li_active > .ml_slide').data('image');

			$('.ml_con1_active').removeClass('ml_con1_active');
			$('#ml_con1r_slide' + id).addClass('ml_con1_active');
			$('#back_image').css('background-image', 'url('+ image +')');
		}

		if(!$('.ml_list li:eq(5)').hasClass("last")){
			$('.btn_prevs').removeAttr('disabled');
		    $('.ml_list li:first').css('display', 'none').insertAfter('.ml_list li:last');
		    $('.ml_list li:eq(5)').css('display', 'block');
		} 
	});

	$(".ml_btn_prevs").click(function(){
		if(!$('.ml_list > .li_active').hasClass('first')){
			$('.ml_list > .li_active').prev().addClass('li_active');
			$('.ml_list > .li_active').next().removeClass('li_active');

			var id = $('.ml_list > .li_active > .ml_slide').data('id');
			var image = $('.ml_list > .li_active > .ml_slide').data('image');

			$('.ml_con1_active').removeClass('ml_con1_active');
			$('#ml_con1r_slide' + id).addClass('ml_con1_active');
			$('#back_image').css('background-image', 'url('+ image +')');
		}

		if(!$('.ml_list li:first').hasClass("first")){
			$('.ml_list li:last-child').css('display', 'block').insertBefore('.ml_list li:first');
	    	$('.ml_list li:gt(5)').css('display', 'none');
		} 
	});

	$('.ml_slide').click(function(){
		var id = $(this).data('id');
		var image = $(this).data('image');

		$('.ml_con1_active').removeClass('ml_con1_active');
		$('#ml_con1r_slide' + id).addClass('ml_con1_active');
		$('#back_image').css('background-image', 'url('+ image +')');
	});
});
</script>
@endpush


