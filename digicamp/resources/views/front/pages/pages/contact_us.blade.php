@extends($view_path.'.layouts.master')
@section('content')
<div class="row page_head" style="background-image:url('{{ url('components/front/images/mockup/contact_banner.jpg') }}');">
	<div class="page_head_bg"></div>
	
	<div class="cus_container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 page_head_tl">
				<p>&nbsp;</p>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 page_head_des">
				<h3>CONTACT US</h3>
			</div>
		</div>
	</div>
</div>


<div class="row">
	<div class="cus_container3 con_con">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12 con_con1_pt">
								<p>PT BANGUN OLAH SARANA SUKSES</p>
							</div>

							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="row">
									<div class="col-md-6 col-sm-6 col-xs-12 con_con1_addr">
										<p>Wisma 77 Building, Tower 1, 8th Floor,</p>
										<p>Letjen S Parman#77</p>
										<p>West Jakarta 11410, Indonesia</p>
									</div>

									<div class="col-md-6 col-sm-6 col-xs-12 con_con1_addr">
										<p>p. +6221 535 9777</p>
										<p>f. +6221 536 1227</p>
										<!-- <p>e. info@boss.com</p> -->
									</div>
								</div>
							</div>	

							<div class="col-md-12 col-sm-12 col-xs-12">
								<p class="con_con1_send">Send us a message</p>
							</div>
						</div>
					</div>

					<div class="col-md-12 col-sm-12 col-xs-12">
						<form action="{{ url('/contact-us') }}" method="POST">
							{{csrf_field()}}

							@if(session()->has('message'))
						    <div class="col-md-12 col-sm-12 col-xs-12">
						    	<div class="row">
								    <div class="alert alert-success alert-dismissable">
								     	<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
								        {{ session()->get('message') }}
								    </div>
								</div>
						    </div>
						  	@endif

				  	  	  	@if (count($errors) > 0)
						  	<div class="col-md-12 col-sm-12 col-xs-12">
						  		<div class="row">
								    <div class="alert alert-danger alert-dismissable">
										<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
										        
										<ul>
										  @foreach ($errors->all() as $error)
										    <li>{{ $error }}</li>
										  @endforeach
										</ul>
							  	    </div>
								</div>
						 	</div>
						  	@endif

							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="row">
									<div class="form-group cont_input_1 cont_input_4">
									    <label for="title">Title:</label>
									    <input type="text" class="form-control cus_input" id="title" name="title">
									</div>
								</div>
							</div>

							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="row">
									<div class="form-group cont_input_2 cont_input_5">
									    <label for="first_name">First Name:</label>
									    <input type="text" class="form-control cus_input" id="first_name" name="first_name">
									</div>
								</div>
							</div>

							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="row">
									<div class="form-group cont_input_3 cont_input_4">
									    <label for="last_name">Last Name:</label>
									    <input type="text" class="form-control cus_input" id="last_name" name="last_name">
									</div>
								</div>
							</div>

							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="row">
									<div class="form-group cont_input_1 cont_input_5">
									    <label for="email">Email:</label>
									    <input type="email" class="form-control cus_input" id="email" name="email">
									</div>
								</div>
							</div>

							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="row">
									<div class="form-group cont_input_2 cont_input_4">
									    <label for="phone">Phone:</label>
									    <input type="text" class="form-control cus_input" id="phone" name="phone">
									</div>
								</div>
							</div>

							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="row">
									<div class="form-group">
									    <label for="message">Message:</label>
									    <textarea rows="4" class="form-control cus_input" id="message" name="message"></textarea>
									</div>
								</div>
							</div>

							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="row">
									<div class="col-md-4 col-sm-4 col-xs-4">
										<div class="row">
											<button type="submit" class="btn btn-default cont_button">submit</button>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row foot_con flex_table">
	<div class="col-md-7 col-sm-7 col-xs-12">
		<div class="row">
		    <div class="google-map">
		        <div id="gmap-style-1"></div>
		    </div>
		</div>
	</div>

	<div class="col-md-5 col-sm-5 col-xs-12 foot_r">
		<div class="row align_center">
			<div class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12">
				<div class="row foot_cons">
					<div class="col-md-12 col-sm-12 col-xs-12 foot_r_tl">
						<p>Contact Info</p>
					</div>

					<div class="col-md-12 col-sm-12 col-xs-12 foot_r_ph">
						<p>p. +6221 535 9777</p>
						<p>f. +6221 536 1227</p>
					</div>

					<div class="col-md-12 col-sm-12 col-xs-12 foot_r_tl2">
						<p>Address</p>
					</div>

					<div class="col-md-12 col-sm-12 col-xs-12 foot_r_ad">
						<p>Wisma 77 Building, Tower 1, 8th Floor,</p>
						<p>Letjen S Parman#77</p>
						<p>West Jakarta 11410, Indonesia</p>
					</div>

					<div class="col-md-12 col-sm-12 col-xs-12 foot_r_sc">
						<a href="https://www.linkedin.com/uas/login"><img src="{{ asset('components/front/images/mockup/linkedin.png') }}" class="img-responsive img_center" /></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@push('custom_scripts')
<script>
	jQuery(document).ready(function () {
	   	/* ==========================================================================
	    Map
	    ========================================================================== */
	    if (jQuery('.google-map').length) {

	        map_Lat_lng = new google.maps.LatLng(-6.190501, 106.797545);
	        map_style = [
	            {
	                "featureType": "landscape",
	                "stylers": [
	                    {"saturation": -100},
	                    {"lightness": 65},
	                    {"visibility": "on"}
	                ]
	            }, {
	                "featureType": "poi",
	                "stylers": [
	                    {"saturation": -100},
	                    {"lightness": 51},
	                    {"visibility": "simplified"}
	                ]
	            }, {
	                "featureType": "road.highway",
	                "stylers": [
	                    {"saturation": -100},
	                    {"visibility": "simplified"}
	                ]
	            }, {
	                "featureType": "road.arterial",
	                "stylers": [
	                    {"saturation": -100},
	                    {"lightness": 30},
	                    {"visibility": "on"}
	                ]
	            }, {
	                "featureType": "road.local",
	                "stylers": [
	                    {"saturation": -100},
	                    {"lightness": 40},
	                    {"visibility": "on"}
	                ]
	            }, {
	                "featureType": "transit",
	                "stylers": [
	                    {"saturation": -100},
	                    {"visibility": "simplified"}
	                ]
	            }, {
	                "featureType": "administrative.province",
	                "stylers": [
	                    {"visibility": "off"}
	                ]
	            }, {
	                "featureType": "water",
	                "elementType": "labels",
	                "stylers": [
	                    {"visibility": "on"},
	                    {"lightness": -25},
	                    {"saturation": -100}
	                ]
	            }, {
	                "featureType": "water",
	                "elementType": "geometry",
	                "stylers": [
	                    {"hue": "#ffff00"},
	                    {"lightness": -25},
	                    {"saturation": -97}
	                ]
	            }
	        ];

	        if (jQuery('#gmap-style-1').length) {
	            map_options = {
	                zoom: 16,
	                panControl: false,
	                scrollwheel: false,
	                mapTypeControl: true,
	                center: map_Lat_lng
	            };
	            map_element = document.getElementById('gmap-style-1');
	            google_map = new google.maps.Map(map_element, map_options);
	            map_marker_image = '{{url("components/front/images/icon/marker.png")}}';
	            map_marker = new google.maps.Marker({
	                position: map_Lat_lng,
	                map: google_map,
	                icon: map_marker_image
	            });
	            google_map.panBy(0, 0);
	            google.maps.event.trigger(google_map, 'resize');

	        } 
	    }
	});
</script>
@endpush
