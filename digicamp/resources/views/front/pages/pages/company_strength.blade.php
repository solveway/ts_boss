@extends($view_path.'.layouts.master')
@section('content')
<div class="row page_head" style="background-image:url('{{ url('components/front/images/mockup/company-strength.jpg') }}');">
	<div class="page_head_bg"></div>
	
	<div class="cus_container3">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 page_head_tl">
				<p>ABOUT US</p>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 page_head_des">
				<h3>COMPANY STRENGTH</h3>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="cus_container3 cs_con">
		<div class="row flex_table cs_consl">
			<div class="col-md-6 col-md-push-6 col-sm-12 col-xs-12 cs_consl_img">
				<img src="{{ asset('components/front/images/mockup/cs7.jpg') }}" class="img-responsive img_width" />
			</div>

			<div class="col-md-6 col-md-pull-6 col-sm-12 col-xs-12 cs_conl">
				<p class="cs_conl_tl">JOGMEC</p>
				<p class="cs_conl_des">Being the first and the only coal company in Indonesia to secure exploration funding from the Japanese Government through its agency Japan Oil Gas Metal Corporation (JOGMEC).</p>

				<div class="cs_conl_bt"></div>
			</div>
		</div>

		<div class="row flex_table cs_consl">
			<div class="col-md-6 col-md-push-6 col-sm-12 col-xs-12 cs_consl_img">
				<img src="{{ asset('components/front/images/mockup/cs1.jpg') }}" class="img-responsive img_width" />
			</div>

			<div class="col-md-6 col-md-pull-6 col-sm-12 col-xs-12 cs_conl">
				<p class="cs_conl_tl">ATTRACTIVE GOAL CHARACTERISTIC</p>
				<p class="cs_conl_des">BOS and PB have high quality coal  (premium quality) with high CV and relatively low levels of sulfur and ash and compares favorably to other Indonesian producers.</p>

				<div class="cs_conl_bt"></div>
			</div>
		</div>

		<div class="row flex_table cs_consl">
			<div class="col-md-6 col-md-push-6 col-sm-12 col-xs-12 cs_consl_img">
				<img src="{{ asset('components/front/images/mockup/cs2.jpg') }}" class="img-responsive img_width" />
			</div>

			<div class="col-md-6 col-md-pull-6 col-sm-12 col-xs-12 cs_conl">
				<p class="cs_conl_tl">COMPETITIVE COST STRUCTURE</p>
				<p class="cs_conl_des">The Company only operates in West Kutai regency, where the Company maintains strong relations only with ONE authorities. The concessions are located in an established coal mining region in Kalimantan and are located adjacent to producing assets owned by established Indonesian coal miners (ie. Gunung Bayan Group and Banpu Group) and which use similar haul and barge transportation.  This should provide comfort that further exploration work will likely yield additional resources whilst showing the viability of the proposed transport plan.</p>

				<div class="cs_conl_bt"></div>
			</div>
		</div>

		<div class="row flex_table cs_consl">
			<div class="col-md-6 col-md-push-6 col-sm-12 col-xs-12 cs_consl_img">
				<img src="{{ asset('components/front/images/mockup/cs4.jpg') }}" class="img-responsive img_width" />
			</div>

			<div class="col-md-6 col-md-pull-6 col-sm-12 col-xs-12 cs_conl">
				<p class="cs_conl_tl">DEVELOPMENT & GROWTH PLAN</p>
				<p class="cs_conl_des">Management has survived during low coal price in 2015 and 2016. With current price range of US$ 90 - US$ 100, it has a clear plan to ramp up production of BOS and bring PB into production by the 1st quarter of 2018. By ramping up production, the business can be cash generative within a short period of time. This is expected to be achieved without significant capex requirements.  Furthermore, by using common infrastructure, management is also seeking to ensure the most efficient use of capital expenditure. The company has also begun in the process of securing land and mapping out a 20km road from the mine directly to the Mahakam river in order to accommodate larger barges and shipments.</p>

				<div class="cs_conl_bt"></div>
			</div>
		</div>

		<div class="row flex_table cs_consl">
			<div class="col-md-6 col-md-push-6 col-sm-12 col-xs-12 cs_consl_img">
				<img src="{{ asset('components/front/images/mockup/cs5.jpg') }}" class="img-responsive img_width" />
			</div>

			<div class="col-md-6 col-md-pull-6 col-sm-12 col-xs-12 cs_conl">
				<p class="cs_conl_tl">EXPAND COAL PRODUCTION AND DIVERSIFY CUSTOMER BASE</p>
				<p class="cs_conl_des">Expand production and productivity from existing mine sites to 120,000 tons per month (tpm) consist:</p>
				<ul>
					<li>
						<p>Expand BOS coal production capacity from 35,000 tpm to 70,000 tpm at Main Pit and Extended Pit.</p>
					</li>

					<li>
						<p>Ramp up PB coal production to 30,000 tpm to 50,000 tpm.</p>
					</li>
				</ul>

				<p class="cs_conl_des">Expand and diversify new customer base and new markets for incremental coal volume i.e. Japan, South Korea, USA, Philippines, Thailand, Vietnam, Taiwan market as well as with develop the cooperation with leading coal player such as Banpu Group, Itochu and Glencore.</p>

				<div class="cs_conl_bt"></div>
			</div>
		</div>
	</div>
</div>
@endsection