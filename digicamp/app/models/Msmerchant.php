<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Msmerchant extends Model{
	protected $table = 'msmerchant';
	protected $merchant_category = 'digipos\models\merchant_category';
	protected $merchant_log 	 = 'digipos\models\merchant_log';

	public function merchant_category(){
		return $this->belongsTo($this->merchant_category,'id_merchant_category');
	}

	public function merchant_log(){
		return $this->hasMany($this->merchant_log,'merchant_id');
	}

	public function city(){
		return $this->belongsTo($this->merchant_log,'city_id');
	}
}