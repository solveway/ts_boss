<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Merchant_category extends Model{
	protected $table = 'merchant_category';
	protected $city 	 = 'digipos\models\city';

	public function city(){
		return $this->hasMany($this->city,'city_id');
	}
}
