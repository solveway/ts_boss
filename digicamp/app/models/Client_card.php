<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Client_card extends Model{
	protected $table 		 	= 'client_card';
	protected $card_category   	= 'digipos\models\Card_category';

    public function card_category(){
        return $this->belongsTo($this->card_category,'card_category_id');
    }
}