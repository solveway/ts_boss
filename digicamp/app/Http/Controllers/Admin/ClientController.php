<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Msmerchant;
use digipos\models\Merchant_category;
use digipos\models\Authmenu;
use digipos\models\Useraccess;
use digipos\models\Card_category;
use digipos\models\User;
use digipos\models\City;
use digipos\models\Config;
use digipos\models\Client_card;
use digipos\models\Client;

// use Request;
use Validator;
use Auth;
use Hash;
use DB;
use digipos\Libraries\Alert;
use Illuminate\Http\Request;
use digipos\Libraries\Email;
use Carbon\Carbon;
use File;


class ClientController extends KyubiController{
	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard);
		$this->middleware($this->role_guard);
		$this->primary_field	= 'id';
		$this->title			= 'Our Client';
		$this->root_link		= 'manage-our-client';
		$this->model			= new Client;
		$this->category_card	= new Card_category;
		$this->client_card			= new Client_card;
		$this->delete_relation	= ['store'];
		$this->bulk_action		= true;
		$this->bulk_action_data = [2];
		$this->image_path 		= 'components/admin/image/client/';
		$this->image_path3		= 'components/admin/image/client_card/';
		$this->data['image_path'] 	= $this->image_path;
		$this->data['image_path2'] 	= 'components/both/images/web/';
		$this->data['image_path3'] 	= $this->image_path3;

		$this->meta_title = Config::where('name', 'web_title')->first();
        $this->meta_description = Config::where('name', 'web_description')->first();
        $this->meta_keyword = Config::where('name', 'web_keywords')->first();
	}

	public function index(){
		$this->field = [
			[
				'name' => 'logo',
				'label' => 'Logo',
				'type' => 'image',
				'file_opt' => ['path' => $this->image_path]
			],
			[
				'name' => 'client_name',
				'label' => 'Name',
				'sorting' 	=> 'y',
				'search' 	=> 'text'
			],
			[
				'name' 		=> 'status',
				'label' 	=> 'Status',
				'sorting' 	=> 'y',
				'search' => 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			]
		];
		$this->model = $this->model;
		// dd('test');
		return $this->build('index');
	}

	public function field_create(){
		$field = [
			[
				'name' => 'merchant_name',
				'label' => 'Name',
				'type' => 'text',
				'attribute' => 'required',
				'validation' => 'required',
				'tab' => 'general',
			],
			[
				'name' => 'address',
				'label' => 'Address',
				'type' => 'text',
				'attribute' => 'required',
				'validation' => 'required',
				'tab' => 'general'
			],
			[
				'name' => 'email',
				'label' => 'Email',
				'type' => 'email',
				'attribute' => 'required',
				'validation' => 'required',
				'tab' => 'general'
			],
			[
				'name' => 'phone',
				'label' => 'Phone',
				'type' => 'text',
				'attribute' => 'required',
				'validation' => 'required',
				'tab' => 'general'
			],
		];
		return $field;
	}

	public function field_edit(){
		$field = [
			[
				'name' => 'merchant_name',
				'label' => 'Name',
				'type' => 'text',
				'attribute' => 'required',
				'validation' => 'required',
				'tab' => 'general'
			],
			[
				'name' => 'owner',
				'label' => 'Owner',
				'type' => 'text',
				'attribute' => 'required',
				'validation' => 'required',
				'tab' => 'general'
			],
			[
				'name' => 'email',
				'label' => 'Email',
				'type' => 'email',
				'attribute' => 'required',
				'validation' => 'required',
				'tab' => 'general'
			],
			[
				'name' => 'user_access_id',
				'label' => 'User Access',
				'type' => 'select',
				'attribute' => 'required',
				'validation' => 'required',
				// 'data' => $this->get_user_access(),
				'class' => 'select2'
			]
		];
		return $field;
	}

	public function create(){
		// $this->field = $this->field_create();
		// return $this->build('create');
		$this->data['title'] = "Create Client";
		$this->data['data1'] = Card_category::pluck('card_category_name', 'id')->toArray();
		return $this->render_view('pages.client.create');
	}

	public function store(Request $request){
		$this->validate($request,[
				'client_name' => 'required|min:5|unique:client,client_name',
				'logo' 			=> 'mimes:jpeg,png,jpg,gif',
		]);
		$this->model->client_name			= $request->client_name;
		$this->model->status 				= 'y';
		$this->model->meta_title 			= $request->meta_title != NULL ? $request->meta_title : $this->meta_title->value;
		$this->model->meta_description 		= $request->meta_title != NULL ? $request->meta_title : $this->meta_description->value;

		$this->model->meta_keyword 			= $request->meta_title != NULL ? $request->meta_title : $this->meta_keyword->value;

		$this->model->updated_by 			= auth()->guard($this->guard)->user()->id;
		
		if ($request->hasFile('logo')){
        	// File::delete($path.$user->images);
			$data = [
						'name' => 'logo',
						'file_opt' => ['path' => $this->image_path, 'width' => '334', 'height' => '273']
					];
			$image = $this->build_image($data);
			$this->model->logo = $image;
		}

		$this->model->save();

		Alert::success('Successfully add new Client');
		return redirect()->to($this->data['path']);
	}

	public function show($id){
		$this->data['merchant'] = 	DB::table('msmerchant')->find($id);
		// dd($this->data['merchant']);
		$this->data['data1'] = Merchant_category::pluck('category_name', 'id')->toArray();
		$this->data['data2'] = $this->city->pluck('name', 'id')->toArray();
		$this->data['data3'] = $this->outlet->get();
		$this->data["title"] = "View Store ".$this->data['merchant']->merchant_name;
		return $this->render_view('pages.merchant.view');
	}

	public function edit($id){
		$this->data['client'] = 	DB::table('client')->find($id);
		// dd($this->data['merchant']);
		$this->data['data1'] = Card_category::where('status', 'y')->get();
		$this->data['data3'] = $this->client_card->where('id_client', $this->data['client']->id)->get();
		// dd($this->data['data3']);
		$this->data["title"] = "View Client ".$this->data['client']->client_name;
		return $this->render_view('pages.client.edit');
	}

	public function update(Request $request, $id){
		// $this->user 				= $this->user->where('email', $request->email_user)->first();
		
		$this->validate($request,[
				'client_name' 	=> 'required|min:5|unique:client,client_name,'.$id,
				'logo' 			=> 'mimes:jpeg,png,jpg,gif',
		]);

		$this->model				= $this->model->find($id);
		
		$this->model->client_name			= $request->client_name;
		// $this->model->status 				= 'y';
		$this->model->meta_title 			= $request->meta_title != NULL ? $request->meta_title : $this->meta_title->value;
		$this->model->meta_description 		= $request->meta_title != NULL ? $request->meta_title : $this->meta_description->value;

		$this->model->meta_keyword 			= $request->meta_title != NULL ? $request->meta_title : $this->meta_keyword->value;

		$this->model->updated_by 			= auth()->guard($this->guard)->user()->id;
		if($request->input('remove-single-image-logo') == 'y'){
			if($this->model->logo != NULL){
				File::delete($this->image_path.$this->model->logo);
				$this->model->logo = '';
			}
		}

		if ($request->hasFile('logo')){
        	// File::delete($path.$user->images);
			$data = [
						'name' => 'logo',
						'file_opt' => ['path' => $this->image_path, 'width' => '334', 'height' => '273']
					];
			$image = $this->build_image($data);
			$this->model->logo = $image;
		}

		$this->model->save();

		//insert data card_client
		$card_client 			= $this->client_card->where('id_client', $this->model->id);
		$id_table_card_client	= $request->id_table_promo;
		// var_dump($id_table_promo);
		if(count($card_client->get()) > 0){
			foreach ($card_client->get() as $a) {
				if($a->image != NULL ){
					if($id_table_card_client){
							if((in_array($a->id, $id_table_card_client))){

							}else{
								// var_dump($this->image_path3.$a->image);
								File::delete($this->image_path3.$a->image);
							}
					}
					
				}
			}
			$card_client->delete();
		}
		// dd($promo->get());
		$temp_card_client			= [];
		$client_card_card_category 	= $request->client_card_card_category;
		$client_card_name 			= $request->client_card_name;
		$client_card_description 	= $request->client_card_description;
		$client_card_no_card 		= $request->client_card_no_card;
		$client_card_code_card		= $request->client_card_code_card;
		$client_card_end_date		= $request->client_card_end_date;
		$client_card_image_index	= $request->client_card_image_index;
		$img_client_card_image		= $request->img_client_card_image;
		// var_dump($promo_image_index);
		var_dump($img_client_card_image);
		if($client_card_name){
			foreach ($client_card_name as $key => $pn) {
				$clientCardName 			= $pn;
				$imgClientCardImage 		= "";
				if(isset($img_client_card_image[$key])){
					$imgClientCardImage 	 = $img_client_card_image[$key];
				}

				if($clientCardName != ""){
					$clientCardDescription	= "";
					if(!empty($client_card_description[$key])){
						$clientCardDescription	= $client_card_description[$key];
					}
				
					$clientCardImageIndex = $client_card_image_index[$key];
					$image = "";
					// var_dump($key);
					// var_dump('$img_promo_image['.$key.']: ');

					// var_dump('promo_image_'.$promoImageIndex);
					
					if ($request->hasFile('client_card_image_'.$clientCardImageIndex)){
						$data = [
									'name' => 'client_card_image_'.$clientCardImageIndex,
									'file_opt' => ['path' => $this->image_path3, 'width' => '382', 'height' => '243']
								];
						$image = $this->build_image($data);
					}else if($imgClientCardImage != ""){
						$image = $imgClientCardImage;
					}

					$endDate = Carbon::parse($client_card_end_date[$key])->format('Y-m-d');
					
					$clientCardNoCard = "";
					if(!empty($client_card_no_card[$key])){
						$clientCardNoCard	= $client_card_no_card[$key];
					}

					$clientCardCodeCard = "";
					if(!empty($client_card_code_card[$key])){
						$clientCardCodeCard	= $client_card_code_card[$key];
					}

					$clientCardCardCategory = "";
					if(!empty($client_card_card_category[$key])){
						$clientCardCardCategory	= $client_card_card_category[$key];
					}

					$temp_card_client[]			= [
						'id_client'			=> $this->model->id,
						'id_card_category'	=> $clientCardCardCategory,
						'client_card_name'		=> $clientCardName,
						'description'		=> $clientCardDescription,
						'no_card'			=> $clientCardNoCard,
						'code_card'			=> $clientCardCodeCard,
						'updated_by'		=> auth()->guard($this->guard)->user()->id,
						'image'				=> $image,
						'end_date'			=> $endDate,
						'created_at'		=> $endDate,
					];

				}else{
					if($imgClientCardImage != ""){
						File::delete($this->image_path3.$imgClientCardImage);
					}
				}
			}
			// dd($temp_card_client);
			if(count($temp_card_client) > 0){
				Client_card::insert($temp_card_client);
			}
		}
		Alert::success('Successfully update client');
		return redirect()->to($this->data['path']);
	}							

	public function destroy(Request $request){
		// return $this->build('delete');

		$id = $request->id;
		$uc = $this->model->find($id);
		$uc->delete();
		Alert::success('Merchant has been deleted');
		return redirect()->back();
	}

	public function ext($action){
		return $this->$action();
	}

	public function updateflag(){
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}

	public function get_user_access(){
		$q = $this->build_array(Useraccess::where('id','>',1)->get(),'id','access_name');
		return $q;
	}

	public function export(){
		return $this->build_export();
	}

	public function sorting(){
		$this->field = [
			[
				'name' 		=> 'merchant_name',
				'label' 	=> 'Name',
				'sorting' 	=> 'y',
				'search' 	=> 'text',
				'type' 		=> 'text'
			],
			[
				'name' 		=> 'status',
				'label' 	=> 'Status',
				'sorting' 	=> 'y',
				'search' => 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			]
		];
		$this->model = $this->model->where('id','>','1');
		return $this->build('sorting');
	}

	public function dosorting(){
		return $this->dosorting();
	}
}
?>