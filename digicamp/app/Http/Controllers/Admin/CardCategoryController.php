<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Invoicelog;
use digipos\models\Msmerchant;
use digipos\models\Member;
use digipos\models\Memberuser;
use digipos\models\Membertype;
use digipos\models\Member_user;
use digipos\models\Store;
use digipos\models\Useraccess;
use digipos\models\Card_category;

// use Request;
use Validator;
use Auth;
use Hash;
use DB;
use digipos\Libraries\Alert;
use digipos\Libraries\Email;
use Illuminate\Http\Request;

class CardCategoryController extends KyubiController{
	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard);
		$this->middleware($this->role_guard);
		$this->primary_field	= 'id';
		$this->title			= 'Card Category';
		$this->root_link		= 'card-category';
		$this->bulk_action_data = [1];
		$this->model			= new Card_category;
		$this->bulk_action		= true;
		// $this->hide_edit_button	= true;
	}

	public function index(){
		$this->field = [
			[
				'name' 		=> 'card_category_name',
				'label' 	=> 'Name',
				'sorting' 	=> 'y',
				'search' 	=> 'text'
			],
			[
				'name' 		=> 'status',
				'label' 	=> 'Status',
				'sorting' 	=> 'y',
				'search' => 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			]
		];
		$this->model = $this->model;
		return $this->build('index');
	}

	public function field_create(){
		$field = [
			[
				'name' => 'card_category_name',
				'label' => 'Card Category Name',
				'type' => 'text',
				'attribute' => 'required',
				'validation' => 'required',
				'tab' => 'general',
				'form_class' => 'col-md-6',
			]
		];
		return $field;
	}

	public function field_edit(){
		$field = [
			[
				'name' => 'card_category_name',
				'label' => 'Card Category Name',
				'type' => 'text',
				'attribute' => 'required',
				'validation' => 'required',
				'tab' => 'general',
			]
		];
		return $field;
	}

	public function create(){
		$this->field = $this->field_create();
		return $this->build('create');
	}

	public function store(Request $request){
		$this->validate($request,[
				'card_category_name' => 'required|unique:card_category,card_category_name',
			]);
		
		$this->model->card_category_name	= $request->card_category_name;
		
		$this->model->status 		= 'y';
		$this->model->updated_by 		= auth()->guard($this->guard)->user()->id;
		$this->model->save();

		Alert::success('Successfully create new card category');
		return redirect()->to($this->data['path']);
	}

	public function show($id){
		$this->model = $this->model->find($id);
		$this->field = $this->field_create();
		return $this->build('view');
	}

	public function edit($id){
		$this->model = $this->model->find($id);
		$this->field = $this->field_edit();
		return $this->build('edit');
	}

	public function update(Request $request, $id){
		$this->validate($request,[
				'card_category_name' => 'required|unique:card_category,card_category_name',
			]);
		
		
		$this->model = $this->model->find($id);
		$this->model->card_category_name	= $request->card_category_name;
		
		$this->model->status 		= 'y';
		$this->model->updated_by 	= auth()->guard($this->guard)->user()->id;
		$this->model->save();

		Alert::success('Successfully edit card category');
		return redirect()->to($this->data['path']);
	}									

	public function destroy(Request $request){
		// return $this->build('delete');

		$id = $request->id;
		$uc = $this->model->find($id);
		$uc->delete();
		Alert::success('Card Category has been deleted');
		return redirect()->back();
	}

	public function ext($action){
		return $this->$action();
	}

	public function updateflag(){
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}

	public function get_user_access(){
		$q = $this->build_array(Useraccess::where('id','>',1)->get(),'id','access_name');
		return $q;
	}

	public function export(){
		$store = $this->myStore();

		$list_member_id = Member::whereIn('store_id', $store)->groupBy('user_id')->pluck('user_id')->toArray();
		return $this->build_export($list_member_id);
	}
}
?>