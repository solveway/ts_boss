<?php namespace digipos\Http\Controllers\Admin;

use DB;
use Session;
use Hash;
use File;

use digipos\models\User;
use digipos\models\Msmenu;
use digipos\models\Useraccess;
use digipos\models\Mslanguage;

use digipos\Libraries\Alert;
use Illuminate\Http\Request;

class UserController extends KyubiController {

	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title 			= "User";
		$this->root_url			= "users/user";
		$this->primary_field 	= "name";
		$this->root_link 		= "user";
		$this->model 			= new User;
		$this->restrict_id 		= [1];
		$this->bulk_action 		= true;
		$this->bulk_action_data = [4];
		$this->image_path 		= 'components/admin/image/user/';
		$this->data['image_path'] 	= $this->image_path;
		$this->merchant_id		= '';

		$this->data['root_url']		= $this->root_url;
		// $this->data['title']	= $this->title;

		// $this->data['authmenux'] = Session('authmenux'); 
		// $this->data['msmenu'] = Session('msmenu');
	}

	/**source.
	 *
	 * @return Response
	 * Display a listing of the response
	 */
	public function index(){
		$this->field = [
			[
				'name' => 'picture',
				'label' => 'Photo',
				'type' => 'image',
				'file_opt' => ['path' => $this->image_path]
			],[
				'name' => 'email',
				'label' => 'Email',
				'sorting' => 'y',
				'search' => 'text'
			],
			[
				'name' => 'username',
				'label' => 'Username',
				'sorting' => 'y',
				'search' => 'text'
			],
			[
				'name' => 'name',
				'label' => 'Name',
				'sorting' => 'y',
				'search' => 'text'
			],
			// [
			// 	'name' => 'user_access_id',
			// 	'label' => 'User Access',
			// 	'sorting' => 'y',
			// 	'search' => 'select',
			// 	'search_data' => $this->get_user_access(),
			// 	'belongto' => ['method' => 'useraccess','field' => 'access_name']
			// ],
			[
				'name' => 'status',
				'label' => 'Status',
				'type' => 'check',
				'data' => ['y' => 'Active','n' => 'Not Active'],
				'tab' => 'general'
			]
		];
		return $this->build('index');

		// global
		// $this->data['user'] = $this->get_user();
		// return $this->render_view('pages.user.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */

	public function field_create(){
		$field = [
					[
						'name' => 'username',
						'label' => 'Username',
						'type' => 'text',
						'attribute' => 'required autofocus',
						'validation' => 'required',
						'not_same' => 'y',
					],
					[
						'name' => 'email',
						'label' => 'Email',
						'type' => 'email',
						'attribute' => 'required autofocus',
						'validation' => 'email|required',
						'not_same' => 'y',
					],[
						'name' => 'password',
						'label' => 'Password',
						'type' => 'password',
						'attribute' => 'required',
						'validation' => 'required',
						'hash' => 'y'
					],[
						'name' => 'name',
						'label' => 'Name',
						'type' => 'text',
						'attribute' => 'required',
						'validation' => 'required'
					],
					[
						'name' => 'phone',
						'label' => 'Phone',
						'type' => 'text',
						'attribute' => 'required',
						'validation' => 'required'
					],
					[
						'name' => 'user_access_id',
						'label' => 'User Access',
						'type' => 'select',
						'data' => $this->get_user_access(),
						'class'	=> 'select2',
						'attribute' => 'required',
						'validation' => 'required'
					],
					[
						'name' => 'store_id',
						'label' => 'User Access',
						'type' => 'select',
						'data' => [],
						'class'	=> 'select2',
						'id'	=> 'store',
						'attribute' => 'required',
						'validation' => 'required',
						'disable'	=>	'true' 
					],
					[
						'name' => 'picture',
						'label' => 'Your Picture',
						'type' => 'file',
						'file_opt' => ['path' => $this->image_path],
						'upload_type' => 'single-image',
						'form_class' => 'col-md-6 pad-left',
						'validation' => 'mimes:jpeg,png,jpg,gif|max:2000',
						'note' => 'Note: File Must jpeg,png,jpg,gif | Max file size: 2Mb'
					],
					// [
					// 	'name' => 'status',
					// 	'label' => 'Status User',
					// 	'type' => 'radio',
					// 	'data' => ['y' => 'Active','n' => 'Not Active'],
					// 	'attribute' => 'required',
					// 	'validation' => 'required'
					// ]
				];
		return $field;
	}

	public function field_edit(){
		$field = [
					[
						'name' => 'username',
						'label' => 'Username',
						'type' => 'text',
						'attribute' => 'required autofocus',
						'validation' => 'required',
						'not_same' => 'y',
					],
					[
						'name' => 'email',
						'label' => 'Email',
						'type' => 'email',
						'attribute' => 'required autofocus',
						'validation' => 'email|required',
						'not_same' => 'y',
					],[
						'name' => 'password',
						'label' => 'Password',
						'type' => 'password',
						'attribute' => 'required',
						'validation' => 'required',
						'hash' => 'y'
					],[
						'name' => 'name',
						'label' => 'Name',
						'type' => 'text',
						'attribute' => 'required',
						'validation' => 'required'
					],
					[
						'name' => 'phone',
						'label' => 'Phone',
						'type' => 'text',
						'attribute' => 'required',
						'validation' => 'required'
					],
					[
						'name' => 'user_access_id',
						'label' => 'User Access',
						'type' => 'select',
						'data' => $this->get_user_access(),
						'class'	=> 'select2',
						'attribute' => 'required',
						'validation' => 'required'
					],
					[
						'name' => 'store_id',
						'label' => 'Store',
						'type' => 'select',
						'data' => $this->get_store(''),
						'class'	=> 'select2',
						'id'	=> 'store',
						'attribute' => 'required',
						'validation' => 'required',
						'disable'	=>	'true' 
					],
					[
						'name' => 'status',
						'label' => 'Status User',
						'type' => 'radio',
						'data' => ['y' => 'Active','n' => 'Not Active'],
						'attribute' => 'required',
						'validation' => 'required'
					],
					[
						'name' => 'picture',
						'label' => 'Your Picture',
						'type' => 'file',
						'file_opt' => ['path' => $this->image_path],
						'upload_type' => 'single-image',
						'form_class' => 'col-md-6 pad-left',
						'validation' => 'mimes:jpeg,png,jpg,gif|max:2000',
						'note' => 'Note: File Must jpeg,png,jpg,gif | Max file size: 2Mb'
					],
				];
		return $field;
	}

	public function create(){
		$this->data['title'] 	= 'Create New '.$this->title;
		// $this->field = $this->field_create();
		// return $this->build('create');
		// $this->data['merchant'] = $this->get_merchant();
		$this->data['user_access'] = $this->get_user_access();

		return $this->render_view('pages.user.create');

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request){
		$this->validate($request,[
			'username'				=> 'required|unique:user',
			'email'					=> 'required|unique:user',
			'password'				=> 'required',
			'name'					=> 'required',
			'phone'					=> 'numeric',
			'picture' 				=> 'mimes:jpeg,png,jpg,gif'
		]);
		
		$this->model->username 			= $request->username;
		$this->model->password 			= Hash::make($request->password);
		$this->model->email 			= $request->email;
		$this->model->name 				= $request->name;
		$this->model->phone 			= $request->phone;
		$this->model->user_access_id 	= $request->user_access_id;
		// $this->model->merchant_id 		= auth()->guard($this->guard)->user()->merchant_id;
		// $this->model->store_id 			= $request->store_id;
		$this->model->status 			= 'y';
		// ($request->login_web == 'y' ? $this->model->login_web = 'y' : $this->model->login_web = 'n');
		// ($request->login_app == 'y' ? $this->model->login_app = 'y' : $this->model->login_app = 'n');

		// dd($request->hasFile('logo'));
		if ($request->hasFile('picture')){
        	// File::delete($this->image_path.$this->model->picture);
			$data = [
						'name' => 'picture',
						'file_opt' => ['path' => $this->image_path]
					];
			$image = $this->build_image($data);
			$this->model->picture = $image;
		}

		if($request->input('remove-single-image-images') == 'y'){
			File::delete($this->image_path.$this->model->picture);
			$this->model->picture = '';
		}
		// $this->model->logo 				= $request->logo;
		$this->model->status 			= 'y';
		$this->model->updated_by			= auth()->guard($this->guard)->user()->id;
		// dd($this->model);
		$this->model->save();

		// $locations = New Jukir_location;
		// $locations->jukir_id = $this->model->id;
		// $locations->location_id = $request->location_id;
		// $locations->save();

		Alert::success('Successfully create user');
		return redirect()->to($this->data['path']);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
		// $this->model = $this->model->where('id',$id);
		// $this->field = $this->field_edit();
		// return $this->build('view');

		$this->data['user'] = $this->model->find($id);
		$this->data['title'] 	= 'View '.$this->title.' '.$this->data['user']->username;	
		$this->data['user_access'] = $this->get_user_access();

		return $this->render_view('pages.user.view');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){
		// $this->model = $this->model->where('id',$id);
		// $this->field = $this->field_edit();
		// return $this->build('edit');

		$this->data['user'] = $this->model->find($id);
		$this->data['title'] 	= 'Edit '.$this->title.' '.$this->data['user']->username;	
		$this->data['user_access'] = $this->get_user_access();
		
		return $this->render_view('pages.user.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id){
		$this->validate($request,[
			'username'				=> 'required|unique:user,username,'.$id,
			'email'					=> 'required|unique:user,email,'.$id,
			'password'				=> 'required',
			'name'					=> 'required',
			'phone'					=> 'numeric',
			'picture' 				=> 'mimes:jpeg,png,jpg,gif'
		]);
		
		$this->model 					= $this->model->find($id);
		$this->model->username 			= $request->username;
		$this->model->password 			= Hash::make($request->password);
		$this->model->email 			= $request->email;
		$this->model->name 				= $request->name;
		$this->model->phone 			= $request->phone;
		$this->model->user_access_id 	= $request->user_access_id;

		//$this->model->merchant_id 		= auth()->guard($this->guard)->user()->merchant_id;

		// dd($request->hasFile('logo'));
		// dd($request->input('remove-single-image-picture'));
		if($request->input('remove-single-image-picture') == 'y'){
			if($this->model->picture != null){
				File::delete($this->image_path.$this->model->picture);
				$this->model->picture = '';
			}
		}
		if ($request->hasFile('picture')){
        	// File::delete($this->image_path.$this->model->picture);
			$data = [
						'name' => 'picture',
						'file_opt' => ['path' => $this->image_path]
					];
			$image = $this->build_image($data);
			$this->model->picture = $image;
		}
		
		// $this->model->logo 				= $request->logo;
		$this->model->status 			= 'y';
		$this->model->updated_by			= auth()->guard($this->guard)->user()->id;
		// dd($this->model);
		$this->model->save();

		// $locations = New Jukir_location;
		// $locations->jukir_id = $this->model->id;
		// $locations->location_id = $request->location_id;
		// $locations->save();

		Alert::success('Successfully update user');
		return redirect()->to($this->data['path']);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(){
		$this->field = $this->field_edit();
		return $this->build('delete');
	}

	public function ext($action){
		return $this->$action();
	}

	public function updateflag(){
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}

	public function get_user_access(){
		$q = $this->build_array(Useraccess::where('id','>',1)->get(),'id','access_name');
		return $q;
	}

	public function get_user(){
		return 1;
		$q = User::where('id', '!=',null)->get();
		return $q;
	}

	public function get_language(){
		$q = Mslanguage::where('status','y')->orderBy('order','asc')->pluck('language_name','id')->toArray();
		return $q;
	}

	// public function get_merchant(){
	// 	$q = $this->build_array(Msmerchant::where('status', 'y')->where('id','>',1)->orderBy('merchant_name','asc')->get(),'id','merchant_name');
	// 	return $q;
	// }

	public function get_store($id){
		return 1;
		// if($id == ''){
		// 	$q = Store::orderBy('store_name','asc')->pluck('store_name','id')->toArray();
		// }else{
		// 	$q = Store::where('merchant_id', $id)->orderBy('store_name','asc')->pluck('store_name','id')->toArray();
		// }
		// return $q;
	}

	public function get_userId_byStore(){
		$my_user					= $this->myStore(); 
		$query						= User::where('id','>','1')->get();
		$users 						= [];	
		// var_dump('$my_user: ');
		// var_dump($my_user);
		// var_dump('<br>');
		// dd($my_user);
		if(is_array($my_user)){
			// $query						= User::get();
			// $users 						= [];
			foreach ($query as $row) {
				$my_user2 = json_decode($row->store_id);				
				// var_dump('my_user2: ');
				// var_dump($my_user2);
				// var_dump('<br>');
				if(is_array($my_user2)){
					foreach ($my_user2 as $key => $value) {
						if(in_array($value,$my_user) && !in_array($row->id,$users)){
							array_push($users, $row->id);
						}
					}
				}else if($my_user2 != null && !in_array($row->id,$users) && $row->store_id == 2 && auth()->guard($this->guard)->user()->store_id == 2 && auth()->guard($this->guard)->user()->merchant_id == $row->merchant_id){
					array_push($users, $row->id);
				}
			}
		}else if($my_user != null){
			// $query						= User::get();
			// $users 						= [];
			foreach ($query as $row) {
				$my_user2	= json_decode($row->store_id);
				// var_dump('my_user2: ');
				// var_dump($my_user2);
				// var_dump('<br>');
				if(is_array($my_user2)){
					foreach ($my_user2 as $key => $value) {
						if($value == $my_user && !in_array($row->id,$users)){
							array_push($users, $row->id);
						}
					}
				}else if($my_user2 != null){
					if($my_user2 == $my_user && !in_array($row->id,$users)){
						array_push($users, $row->id);
					}
				}
			}

		}

		return $users;
	}

	public function get_user_storeId($store_id){
		if($store_id){
			$my_store = json_decode($store_id);
		}else{
			$my_store = json_decode(auth()->guard($this->guard)->user()->store_id);
		}
		$store = [];
		if(is_array($my_store)){
			$store 						= $my_store;	
		}else if($my_store != null){
			if($my_store == 2){
				$store = $this->myStore();
			}else{
				array_push($store, $my_store);
			}

		}
		return $store;
	}

	public function export(){
		if(in_array(auth()->guard($this->guard)->user()->store_id,["0","1"])){
			$users = '';
		}else{
			$users = $this->get_userId_byStore();
		}
		return $this->build_export($users);
	}
}
