<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Msmerchant;
use digipos\models\Merchant_category;
use digipos\models\Authmenu;
use digipos\models\Useraccess;
use digipos\models\Outlet;
use digipos\models\User;
use digipos\models\City;
use digipos\models\Config;
use digipos\models\Promo;
use digipos\models\Merchant_log;
// use Request;
use Validator;
use Auth;
use Hash;
use DB;
use digipos\Libraries\Alert;
use Illuminate\Http\Request;
use digipos\Libraries\Email;
use Carbon\Carbon;
use File;


class MerchantController extends KyubiController{
	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard);
		$this->middleware($this->role_guard);
		$this->primary_field	= 'id';
		$this->title			= 'Merchant';
		$this->root_link		= 'manage-merchant';
		$this->model			= new MsMerchant;
		$this->promo			= new Promo;
		$this->user				= new User;
		$this->city				= new City;
		$this->outlet			= new Outlet;
		$this->merchant_category= new Merchant_category;
		$this->delete_relation	= ['store'];
		$this->bulk_action		= true;
		$this->bulk_action_data = [1];
		$this->image_path 		= 'components/admin/image/merchant/';
		$this->image_path3		= 'components/admin/image/promo/';
		$this->data['image_path'] 	= $this->image_path;
		$this->data['image_path2'] 	= 'components/both/images/web/';
		$this->data['image_path3'] 	= $this->image_path3;

		$this->meta_title = Config::where('name', 'web_title')->first();
        $this->meta_description = Config::where('name', 'web_description')->first();
        $this->meta_keyword = Config::where('name', 'web_keywords')->first();
	}

	public function index(){
		$this->field = [
			[
				'name' => 'logo',
				'label' => 'Logo',
				'type' => 'image',
				'file_opt' => ['path' => $this->image_path]
			],
			[
				'name' 		=> 'merchant_name',
				'label' 	=> 'Name',
				'sorting' 	=> 'y',
				'search' 	=> 'text'
			],
			[
				'name' 		=> 'status',
				'label' 	=> 'Status',
				'sorting' 	=> 'y',
				'search' => 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			]
		];
		$this->model = $this->model;
		return $this->build('index');
	}

	public function field_create(){
		$field = [
			[
				'name' => 'merchant_name',
				'label' => 'Name',
				'type' => 'text',
				'attribute' => 'required',
				'validation' => 'required',
				'tab' => 'general',
			],
			[
				'name' => 'address',
				'label' => 'Address',
				'type' => 'text',
				'attribute' => 'required',
				'validation' => 'required',
				'tab' => 'general'
			],
			[
				'name' => 'email',
				'label' => 'Email',
				'type' => 'email',
				'attribute' => 'required',
				'validation' => 'required',
				'tab' => 'general'
			],
			[
				'name' => 'phone',
				'label' => 'Phone',
				'type' => 'text',
				'attribute' => 'required',
				'validation' => 'required',
				'tab' => 'general'
			],
		];
		return $field;
	}

	public function field_edit(){
		$field = [
			[
				'name' => 'merchant_name',
				'label' => 'Name',
				'type' => 'text',
				'attribute' => 'required',
				'validation' => 'required',
				'tab' => 'general'
			],
			[
				'name' => 'owner',
				'label' => 'Owner',
				'type' => 'text',
				'attribute' => 'required',
				'validation' => 'required',
				'tab' => 'general'
			],
			[
				'name' => 'email',
				'label' => 'Email',
				'type' => 'email',
				'attribute' => 'required',
				'validation' => 'required',
				'tab' => 'general'
			],
			[
				'name' => 'user_access_id',
				'label' => 'User Access',
				'type' => 'select',
				'attribute' => 'required',
				'validation' => 'required',
				// 'data' => $this->get_user_access(),
				'class' => 'select2'
			]
		];
		return $field;
	}

	public function create(){
		// $this->field = $this->field_create();
		// return $this->build('create');
		$this->data['title'] = "Create Merchant";
		$this->data['data1'] = Merchant_category::pluck('category_name', 'id')->toArray();
		$this->data['data2'] = $this->city->pluck('name', 'id')->toArray();
		return $this->render_view('pages.merchant.create');
	}

	public function store(Request $request){

		$this->validate($request,[
				'merchant_name' => 'required|unique:msmerchant,merchant_name',
				'slug' 			=> 'required|unique:msmerchant,slug',
				'address' 		=> 'required',
				'email' 		=> 'required|unique:msmerchant,email',
				'phone'			=> 'required',
				'city'			=> 'required',
				'logo' 			=> 'mimes:jpeg,png,jpg,gif',
		]);
		$this->model->merchant_name			= $request->merchant_name;
		$this->model->id_merchant_category	= $request->merchant_category;
		$this->model->address				= $request->address;
		$this->model->email					= $request->email;
		$this->model->phone					= $request->phone;
		$this->model->id_city				= $request->city;
		$this->model->status 				= 'y';
		$this->model->end_date				= Carbon::parse($request->merchant_end_date)->format('Y-m-d');
		$this->model->meta_title 			= $request->meta_title != NULL ? $request->meta_title : $this->meta_title->value;
		$this->model->meta_description 		= $request->meta_title != NULL ? $request->meta_title : $this->meta_description->value;

		$this->model->meta_keyword 			= $request->meta_title != NULL ? $request->meta_title : $this->meta_keyword->value;

		$this->model->updated_by 			= auth()->guard($this->guard)->user()->id;

		if ($request->hasFile('headline_image')){
        	// File::delete($path.$user->images);
			$data = [
						'name' => 'headline_image',
						'file_opt' => ['path' => $this->image_path, 'width' => '744', 'height' => '465']
					];
			$image = $this->build_image($data);
			$this->model->headline_image = $image;
		}

		($request->sticky == 'y' ? $this->model->sticky = 'y' : $this->model->sticky = 'n');
		// dd($this->model);
		$this->model->save();

		Alert::success('Successfully add new Merchant');
		return redirect()->to($this->data['path']);
	}

	public function show($id){
		$this->data['merchant'] = 	DB::table('msmerchant')->find($id);
		// dd($this->data['merchant']);
		$this->data['data1'] = Merchant_category::pluck('category_name', 'id')->toArray();
		$get_city = $this->city->select('id','name')->get();

		foreach($get_city as $ctc) {
			$gt_city = array(
       	  				'KAB.', 'KOTA', 'ADM.'
       	  			   ); 

		 	$city = str_replace($gt_city, ' ', $ctc->name);

		 	$result_city[$ctc->id] = trim($city, ' ');
		}

		$this->data['data2'] = $result_city;
		$this->data['data3'] = $this->outlet->where('merchant_id', $id)->get();
		$this->data["title"] = "View Store ".$this->data['merchant']->merchant_name;
		return $this->render_view('pages.merchant.view');
	}

	public function edit($id){
		$this->data['merchant'] = 	DB::table('msmerchant')->find($id);
		// dd($this->data['merchant']);
		$this->data['data1'] = Merchant_category::pluck('category_name', 'id')->toArray();
		// $this->data['data2'] = $this->city->pluck('name', 'id')->toArray();

		$get_city = $this->city->select('id','name')->get();

		foreach($get_city as $ctc) {
			$gt_city = array(
       	  				'KAB.', 'KOTA', 'ADM.'
       	  			   ); 

		 	$city = str_replace($gt_city, ' ', $ctc->name);

		 	$result_city[$ctc->id] = trim($city, ' ');
		}

		$this->data['data2'] = $result_city;
		$this->data['data3'] = $this->outlet->where('merchant_id', $id)->get();
		$this->data['data4'] = $this->promo->where('merchant_id',$id)->get();
		$this->data["title"] = "View merchant ".$this->data['merchant']->merchant_name;
		return $this->render_view('pages.merchant.edit');
	}

	public function update(Request $request, $id){
		$this->user 				= $this->user->where('email', $request->email_user)->first();
		
		$this->validate($request,[
				'merchant_name' 			=> 'required|min:5|unique:msmerchant,merchant_name,'.$id,
				'address' 					=> 'required',
				'email' 					=> 'required|unique:msmerchant,email,'.$id,
				'phone'						=> 'required',
				'city'						=> 'required',
				'logo' 						=> 'mimes:jpeg,png,jpg,gif',
		]);

		$this->model				= $this->model->find($id);
		
		$this->model->merchant_name			= $request->merchant_name;
		$this->model->slug 					= $request->slug;
		$this->model->id_merchant_category	= $request->merchant_category;
		$this->model->address				= $request->address;
		$this->model->email					= $request->email;
		$this->model->phone					= $request->phone;
		$this->model->end_date				= Carbon::parse($request->merchant_end_date)->format('Y-m-d');
		$this->model->id_city				= $request->city;
		// $this->model->status 				= 'y';
		$this->model->meta_title 			= $request->meta_title != NULL ? $request->meta_title : $this->meta_title->value;
		$this->model->meta_description 		= $request->meta_title != NULL ? $request->meta_title : $this->meta_description->value;

		$this->model->meta_keyword 			= $request->meta_title != NULL ? $request->meta_title : $this->meta_keyword->value;

		$this->model->updated_by 			= auth()->guard($this->guard)->user()->id;
		
		if($request->input('remove-single-image-promo_image') == 'y'){
			if($this->model->logo != NULL){
				File::delete($this->image_path.$this->model->logo);
				$this->model->logo = '';
			}
		}

		if ($request->hasFile('logo')){
        	// File::delete($path.$user->images);
			$data = [
						'name' => 'logo',
						'file_opt' => ['path' => $this->image_path, 'width' => '202', 'height' => '144']
					];
			$image = $this->build_image($data);
			$this->model->logo = $image;
		}

		if($request->input('remove-single-image-headline_image') == 'y'){
			if($this->model->logo != NULL){
				File::delete($this->image_path.$this->model->logo);
				$this->model->logo = '';
			}
		}

		if ($request->hasFile('headline_image')){
        	// File::delete($path.$user->images);
			$data = [
						'name' => 'headline_image',
						'file_opt' => ['path' => $this->image_path, 'width' => '744', 'height' => '465']
					];
			$image = $this->build_image($data);
			$this->model->headline_image = $image;
		}

		($request->sticky == 'y' ? $this->model->sticky = 'y' : $this->model->sticky = 'n');
		// dd($this->model);
		$this->model->save();

		//delete data promo_merchant and data promo
		// $promo_merchant 	= Promo_merchant::where('id_merchant', $this->model->id);
		// if(count($promo_merchant->get()) > 1){
		// 	foreach ($promo_merchant->get() as $pm) {
		// 		$this->promo->where('id', $pm->promo_id)->delete();
		// 	}
		// 	$promo_merchant->delete();
		// }

		// //insert data promo
		// $this->promo->promo_name 			= $request->promo_name;
		// $this->promo->promo_description 	= $request->promo_description;
		// $this->promo->term_conditon 		= $request->promo_description;
		// $this->promo->status 				= 'y';
		// $this->promo->promo_name 			= $request->promo_description;

		$outlet 	= $this->outlet->where('merchant_id', $this->model->id);
		if(count($outlet->get()) > 1){
			$outlet->delete();
		}

		//insert data outlet
		$temp_outlet					= [];
		$outlet_name2 			= $request->outlet_name2;
		$address2 				= $request->outlet_address2;
		$phone2 				= $request->outlet_phone2;
		$email2 				= $request->outlet_email2;
		
		if($outlet_name2){
			foreach ($outlet_name2 as $key => $om) {
				$outletName 		= $om;
				$outletAddress		= $address2[$key];
				$outletPhone		= "";
				if(!empty($phone2[$key])){
					$outletPhone	= $phone2[$key];
				}

				$outletEmail		= "";
				if(!empty($email2[$key])){
					$outletEmail		= $email2[$key];
				}

				$temp_outlet[]			= [
					'merchant_id'	=> $this->model->id,
					'outlet_name'	=> $outletName,
					'address'		=> $outletAddress,
					'phone'			=> $outletPhone,
					'email'			=> $outletEmail,
					'updated_by'	=> auth()->guard($this->guard)->user()->id
				];
			}

			if(count($temp_outlet) > 0){
				// dd($temp_outlet);
				Outlet::insert($temp_outlet);
			}
		}

		//insert data promo
		$promo 	= $this->promo->where('merchant_id', $this->model->id);
		$id_table_promo	= $request->id_table_promo;

		if(count($promo->get()) > 0){
			foreach ($promo->get() as $a) {
				if($a->image != NULL ){
					if($id_table_promo){
							if((in_array($a->id, $id_table_promo))){

							}else{
								File::delete($this->image_path3.$a->image);
							}
					}
					
				}
			}
			$promo->delete();
		}

		$temp_promo					= [];
		$promo_name 				= $request->promo_name;
		$promo_slug					= $request->promo_slug;
		$promo_description 			= $request->promo_description;
		$term_condition 			= $request->term_condition;
		$promo_image_description	= $request->promo_image_description;
		$start_date					= $request->start_date;
		$end_date					= $request->end_date;
		$promo_image_index			= $request->promo_image_index;
		$img_promo_image			= $request->img_promo_image;

		if($promo_name){
			foreach ($promo_name as $key => $pn) {
				$promoName 			= $pn;
				$imgPromoImage = "";
				if(isset($img_promo_image[$key])){
					$imgPromoImage 	 = $img_promo_image[$key];
				}

				if($promoName != ""){
					$termCondition		= $term_condition[$key];
					$promoSlug 			= $promo_slug[$key];
					$promoDescription	= "";
					if(!empty($promo_description[$key])){
						$promoDescription	= $promo_description[$key];
					}

					$promoImageIndex = $promo_image_index[$key];
					$image = "";
				
					if ($request->hasFile('promo_image_'.$promoImageIndex)){
						$data = [
									'name' => 'promo_image_'.$promoImageIndex,
									'file_opt' => ['path' => $this->image_path3, 'width' => '776', 'height' => '222']
								];
						$image = $this->build_image($data);
					}else if($imgPromoImage != ""){
						$image = $imgPromoImage;
					}

					$startDate = Carbon::parse($start_date[$key])->format('Y-m-d');
					$endDate = Carbon::parse($end_date[$key])->format('Y-m-d');


					$temp_promo[]			= [
						'merchant_id'		=> $this->model->id,
						'promo_name'		=> $promoName,
						'promo_slug'        => $promoSlug,
						'description'		=> $promoDescription,
						'term_condition'	=> $termCondition,
						'updated_by'		=> auth()->guard($this->guard)->user()->id,
						'image'				=> $image,
						'start_date'		=> $startDate,
						'end_date'			=> $endDate,
					];

				}else{
					if($imgPromoImage != ""){
						File::delete($this->image_path3.$imgPromoImage);
					}
				}
			}
			if(count($temp_promo) > 0 ){
				// dd($temp_promo);
				Promo::insert($temp_promo);
			}
		}
		Alert::success('Successfully add create new merchant');
		return redirect()->to($this->data['path']);
	}							

	public function destroy(Request $request){
		// return $this->build('delete');

		$id = $request->id;
		$uc = $this->model->find($id);
		$store = Store::where('merchant_id', $uc->id);
		$user = $this->user->where('merchant_id', $uc->id);
		$uc->delete();
		$user->delete();
		$store->delete();
		Alert::success('Merchant has been deleted');
		return redirect()->back();
	}

	public function ext($action){
		return $this->$action();
	}

	public function updateflag(){
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}

	public function get_user_access(){
		$q = $this->build_array(Useraccess::where('id','>',1)->get(),'id','access_name');
		return $q;
	}

	public function get_kecamatan($text = ''){
		$kecamatan = $this->kecamatan->where('name', 'like', '%'.$text.'%')->pluck('name', 'id')->toArray();
		return $kecamatan;
	}

	public function export(){
		return $this->build_export();
	}

	public function sorting(){
		$this->field = [
			[
				'name' 		=> 'merchant_name',
				'label' 	=> 'Name',
				'sorting' 	=> 'y',
				'search' 	=> 'text',
				'type' 		=> 'text'
			],
			[
				'name' 		=> 'status',
				'label' 	=> 'Status',
				'sorting' 	=> 'y',
				'search' => 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			]
		];
		$this->model = $this->model->where('id','>','1');
		return $this->build('sorting');
	}

	public function dosorting(){
		return $this->dosorting();
	}

	public function get_merchant_category($par){
		$data = $this->merchant_category
			->join('msmerchant','msmerchant.id_merchant_category','=','merchant_category.id')
			->join('city','city.id','=','msmerchant.id_city')
			->join('merchant_log','merchant_log.merchant_id','=','msmerchant.id')
			->where('merchant_log.created_at','>=',DB::raw('DATE_SUB(CURDATE(), INTERVAL '.$par.' DAY)'))
			->where('merchant_log.created_at','<=',DB::raw('NOW()')); 
		$merchant_category 	= $data
							->select('merchant_category.*', 'msmerchant.merchant_name', 'msmerchant.id_merchant_category','city.name as city_name','merchant_log.ip_address', 'merchant_log.created_at', DB::raw('COUNT(*) as visits'))
							->groupBy('msmerchant.id_merchant_category')
							->get();
		return json_encode($merchant_category);
	}

	public function get_merchant_location($par){
		$merchant_location 	= $this->merchant_category
							->select('msmerchant.merchant_name', 'msmerchant.id_merchant_category', 'msmerchant.id_city','city.name as city_name','merchant_log.ip_address', 'merchant_log.created_at', DB::raw('COUNT(*) as visits'))
							->join('msmerchant','msmerchant.id_merchant_category','=','merchant_category.id')
							->join('city','city.id','=','msmerchant.id_city')
							->join('merchant_log','merchant_log.merchant_id','=','msmerchant.id')
							->where('merchant_log.created_at','>=',DB::raw('DATE_SUB(CURDATE(), INTERVAL '.$par.' DAY)'))
							->where('merchant_log.created_at','<=',DB::raw('NOW()'))
							->orderBy('msmerchant.id_city')
							->groupBy('city.id', 'msmerchant.id_city')
							->get();
		return json_encode($merchant_location);
	}
}
?>