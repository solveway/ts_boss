<?php namespace digipos\Http\Controllers\Admin;
use digipos\Libraries\Email;

use digipos\models\Province;
use digipos\models\City;
use digipos\models\Sub_district;
use digipos\models\Msmerchant;
use digipos\models\Merchant_category;
use digipos\models\Merchant_log;
use digipos\models\News;
use digipos\models\Customer;
use digipos\models\Client;

use DB;

class IndexController extends Controller {

	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard); 
		$this->data['title'] 	= "Dashboard";
		$this->merchant 		= new Msmerchant;
		$this->news 			= new News;
		$this->customer 		= new Customer;
		$this->client 			= new Client;
		$this->merchant_category = new Merchant_category;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(){
		// $curl = curl_init();

		// curl_setopt_array($curl, array(
		//   CURLOPT_URL => "http://pro.rajaongkir.com/api/cost",
		//   CURLOPT_RETURNTRANSFER => true,
		//   CURLOPT_ENCODING => "",
		//   CURLOPT_MAXREDIRS => 10,
		//   CURLOPT_TIMEOUT => 30,
		//   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		//   CURLOPT_CUSTOMREQUEST => "POST",
		//   CURLOPT_POSTFIELDS => "origin=151&originType=city&destination=2087&destinationType=subdistrict&weight=1000&courier=jne",
		//   CURLOPT_HTTPHEADER => array(
		//     "content-type: application/x-www-form-urlencoded",
		//     "key: c231455ad5320cbb2d670a39aef8fc8f"
		//   ),
		// ));

		// $response = curl_exec($curl);
		// $err = curl_error($curl);

		// curl_close($curl);

		// if ($err) {
		//   echo "cURL Error #:" . $err;
		// } else {
		//   dd(json_decode($response)->rajaongkir->results);
		// }
		// dd('x');
		/*Email::to('sinceritymaiden@hotmail.com');
		Email::subject('test');
		Email::view($this->view_path.'.emails.body-info');
		Email::email_data($this->data);
		Email::send();*/
		// $this->data['merchant_category'] = $this->merchant->with('merchant_category')->with('merchant_log')->get();
		$data = $this->merchant_category
			->join('msmerchant','msmerchant.id_merchant_category','=','merchant_category.id')
			->join('city','city.id','=','msmerchant.id_city')
			->join('merchant_log','merchant_log.merchant_id','=','msmerchant.id')
			->where('merchant_log.created_at','>=',DB::raw('DATE_SUB(CURDATE(), INTERVAL 7 DAY)'))
			->where('merchant_log.created_at','<=',DB::raw('NOW()')); 
		$merchant_category 	= $data
							->select('merchant_category.*', 'msmerchant.merchant_name', 'msmerchant.id_merchant_category','city.name as city_name','merchant_log.ip_address', 'merchant_log.created_at', DB::raw('COUNT(*) as visits'))
							->groupBy('msmerchant.id_merchant_category')
							->get();
		$merchant_category2 = $data
							->select('merchant_category.*', 'msmerchant.merchant_name', 'msmerchant.id_merchant_category','city.name as city_name','merchant_log.ip_address', 'merchant_log.created_at', DB::raw('COUNT(*) as visits'))
							->groupBy('msmerchant.merchant_name')
							->get();
		$merchant_location 	= $this->merchant_category
							->select('msmerchant.merchant_name', 'msmerchant.id_merchant_category', 'msmerchant.id_city','city.name as city_name','merchant_log.ip_address', 'merchant_log.created_at', DB::raw('COUNT(*) as visits'))
							->join('msmerchant','msmerchant.id_merchant_category','=','merchant_category.id')
							->join('city','city.id','=','msmerchant.id_city')
							->join('merchant_log','merchant_log.merchant_id','=','msmerchant.id')
							->where('merchant_log.created_at','>=',DB::raw('DATE_SUB(CURDATE(), INTERVAL 7 DAY)'))
							->where('merchant_log.created_at','<=',DB::raw('NOW()'))
							->orderBy('msmerchant.id_city')
							->groupBy('city.id', 'msmerchant.id_city')
							->get();
		// var_dump(count($merchant_category2));					
		// foreach ($merchant_category as $key => $value) {
		// 	foreach ($merchant_category2 as $key2 => $value2) {
		// 		if($value['id_merchant_category'] == $value2['id_merchant_category']){
		// 			if(!isset($value['subdata'])){
		// 				$value['subdata'] = array();
		// 				$value['subdata'] = $value;
		// 			}else{
		// 				$value['subdata'] = $value2;
		// 			}
		// 		}

		// 	}
		// }	
		// dd($merchant_location);
		$this->data['merchant_category'] = 	json_encode($merchant_category);
		$this->data['merchant_category2'] = json_encode($merchant_category2);
		$this->data['merchant_location'] = json_encode($merchant_location);
		$this->data['merchant'] = $this->merchant->count();
		$this->data['news'] = $this->news->count();
		$this->data['customer'] = $this->customer->count();
		$this->data['client'] = $this->client->count();
		$this->data['merchant_log'] = Msmerchant::select('msmerchant.merchant_name', DB::raw('COUNT(*) as counts'))->join('merchant_log','merchant_log.merchant_id', 'msmerchant.id')->groupBy('msmerchant.id')->orderBy('counts', 'desc')->limit(5)->get();
		// dd($this->data['merchant_category']);
		// dd($this->data['merchant_category']);
		return $this->render_view('pages.index');
	}

}
