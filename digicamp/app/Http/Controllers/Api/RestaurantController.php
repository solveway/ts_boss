<?php namespace digipos\Http\Controllers\Api;

use Illuminate\Http\Request;
use digipos\models\Restaurant;
use digipos\models\Restaurant_feature;
use digipos\models\Restaurant_food;
use digipos\models\Restaurant_tag;
use App;

class RestaurantController extends Controller {

	public function __construct(){
		$this->middleware($this->auth_guard);
		parent::__construct();
	}

	public function dashboard(request $request){
		$cust 	= session()->pull('cust');
		if(count($cust->restaurant) == 0){
			$res 	= ['status' => 'no_data'];
		}else{

		}
		return response()->json($res);
	}

	public function create(){
		$res['feature'] = Restaurant_feature::where('status','y')->orderBy('feature_name','asc')->select('id','feature_name')->get(); 
		$res['food'] 	= Restaurant_food::where('status','y')->orderBy('food_name','asc')->select('id','food_name')->get(); 
		$res['tag'] 	= Restaurant_tag::where('status','y')->orderBy('tag_name','asc')->select('id','tag_name')->get();
		$res['listDay']	= ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
		$start 			= '00:00';
		$end 			= '24:00';
		$listHours[] 	= $start; 
		while($start < '23:45'){
			$start  	= $this->set_dif_time('+','15','minutes',$start);
			$listHours[] 	= $start;
		}
		$listHours[]		= $end;
		$res['listHours']	= $listHours;
		return response()->json($res);
	}

	public function store(request $request){
		dd($request->all());
	}
}
