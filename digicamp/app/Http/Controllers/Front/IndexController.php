<?php namespace digipos\Http\Controllers\Front;

use DB;
use digipos\models\News;

class IndexController extends ShukakuController {

	public function index(){
		$this->data['list_news'] = News::select('id', 'title', 'description', 'image')->where('status', 'y')->orderBy('id', 'desc')->get();

		return $this->render_view('pages.index');
	}
}
