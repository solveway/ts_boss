<?php namespace digipos\Http\Controllers\Front;
use Illuminate\Http\request;
use digipos\models\Contact_message;
use digipos\models\News;


class PagesController extends ShukakuController {
	public function sc_live(request $request){
		$search = $request->search;
		$merchant = Merchant_category::join('msmerchant', 'merchant_category.id', 'msmerchant.id_merchant_category')->where([['merchant_category.status', 'y'],['merchant_category.category_name', 'LIKE', '%'.$search.'%']])->select('merchant_category.category_name')->distinct()->get();

		if($merchant != NULL){
			$result = $merchant; 
		} else{
			$result['status'] = "No Result";
		}

		return response()->json(['result' => $result]);
	}

	public function aboutvm(){
		return $this->render_view('pages.pages.visi_misi');
	}

	public function aboutcs(){
		return $this->render_view('pages.pages.company_strength');
	}

	public function aboutml(){
		return $this->render_view('pages.pages.milestones');
	}

	public function boardcm(){
		return $this->render_view('pages.pages.board_commisioners');
	}

	public function boarddc(){
		return $this->render_view('pages.pages.board_directors');
	}

	public function olah_sarana(){
		return $this->render_view('pages.pages.olah_sarana');
	}

	public function pratama_bersama(){
		return $this->render_view('pages.pages.pratama_bersama');
	}

	public function energi_amzal(){
		return $this->render_view('pages.pages.amzal_bersama');
	}

	public function buana_sentosa(){
		return $this->render_view('pages.pages.pratama_buana');
	}

	public function prj_pro(){
		return $this->render_view('pages.pages.prj_production');
	}

	public function prj_port(){
		return $this->render_view('pages.pages.prj_port');
	}

	public function prj_guaran(){
		return $this->render_view('pages.pages.prj_guaranted');
	}

	public function prj_coal(){
		return $this->render_view('pages.pages.prj_coal');
	}

	public function prj_documents(){
		return $this->render_view('pages.pages.documents');
	}

	public function health_safe(){
		return $this->render_view('pages.pages.health_safe');
	}

	public function news(request $request){
		$ids = $request->ids;

		$query = News::select('id','title', 'description', 'image')->where('status', 'y');

		if($ids != NULL){
			$news = $query->where('id', $ids)->first();
		} else{
			$news = $query->orderBy('id', 'desc')->first();
		}

		if(count($news) > 0){
			$list_news = News::select('title', 'description', 'image')->where([['status', 'y'], ['id', '!=', $news->id]])->orderBy('id', 'desc')->get();
		} else{
			$news = "";
			$list_news = "";
		}

		$this->data['news'] = $news;
		$this->data['list_news'] = $list_news;
		return $this->render_view('pages.pages.news_release');
	}

	public function contact_us(){
		return $this->render_view('pages.pages.contact_us');
	}

	public function relation(){
		return $this->render_view('pages.pages.investor_relation');
	}

	public function search(Request $request){
		$news = News::select('id', 'title', 'description')->where('title', 'like', '%'. $request->search .'%')->orWhere('description', 'like', '%'.$request->search.'%')->get();

		$search = $request->search;
		$description = '';
		$data = [];

		if(count($news) > 0){
			foreach($news as $n => $nw){
				if($nw->description != ""){
					$array = explode("\n", $nw->description);

					$matches = array();
					foreach($array as $k=>$v) {
					    if(preg_match("/\b$search\b/i", $v)) {
					        $matches[$k] = $v;
					        break;
					    }
					}

					if(count($matches) > 0){
						$description = $matches[0];
						$description = preg_replace("/\p{L}*?".preg_quote($search)."\p{L}*/ui", "<b>$0</b>", $description);
					}
				}

				$temp = [
							'id' 			=> $nw->id,
							'title' 		=> $nw->title,
							'description'	=> $description,
						];

				$data[$n] = (object)$temp; 
			}

			$this->data['news'] = $data;
		} else{
			$this->data['news'] = '';			
		}

		return $this->render_view('pages.pages.search');
	}

	public function contact_message(Request $request){
		$get_email = Config::select('value')->where('name', 'web_email')->first();

		$this->validate($request,[
			'title'					=> 'required',
			'first_name'			=> 'required',
			'email'					=> 'required',
			'phone'					=> 'required|numeric',
			'message'				=> 'required',
		],
		[
			'title.required'		=> 'Title is Required',
			'first_name.required'	=> 'First Name is Required',
			'phone.required'		=> 'Phone is Required',
			'phone.numeric'			=> 'Phone must be number',
			'email'					=> 'Email is Required',
			'message.required'		=> 'Message is Required',
		]);

		$contact =  new Contact_message;
		$contact->title 		= $request->title;
		$contact->first_name 	= $request->first_name;
		$contact->last_name 	= $request->last_name;
		$contact->email 	 	= $request->email;
		$contact->phone 	 	= $request->phone;
		$contact->message 	 	= $request->message;
		$contact->save();

		$url      = "front.mail.content";
		$title    = "Thanks For Contacting Us";
		$subject  = "Thanks For Contacting Us"; 
		$status   = "cust";
		$to 	  = $request->email;
		$content  = "<p>Hi ".$request->first_name." ".$request->last_name.",</p>
					 <p>Thanks for sending us a Message, We will reply soon.</p>
					";

		$request->request->add([
								'e_url'     => $url,
								'e_title'   => $title,
								'e_subject' => $subject,
								'e_email'	=> $to,
								'e_status'  => $status,
								'e_content' => $content
								]);

		$this->email($request);

		if(count($get_email) > 0){
			$url2      = "front.mail.content";
			$title2    = "Contact Us";
			$subject2  = "Contact Us"; 
			$status2   = "admin";
			$to2	   = $get_email->value;
			$content2  = "  <p>Hi Admin,&nbsp;</p>
							<p>There a message for you from Customer, with detail :</p>
							<p>Title &nbsp; &nbsp; : ".$request->title."</p>
							<p>Name &nbsp; &nbsp; : ".$request->first_name." ".$request->last_name.""."</p>
							<p>Email &nbsp; &nbsp; : ".$request->email."</p>
							<p>Phone &nbsp;: ".$request->phone."</p>
							<p>Message &nbsp; : ".$request->message."</p>
						";

			$request->request->add([
									'e_url'     => $url2,
									'e_title'   => $title2,
									'e_subject' => $subject2,
									'e_email'	=> $to2,
									'e_status'  => $status2,
									'e_content' => $content2
									]);

			$this->email($request);
		}
		
       return redirect()->back()->with('message', 'Success, Thanks For Apply');
	}
}
